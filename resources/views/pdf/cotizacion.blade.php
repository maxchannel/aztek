<html>
<head>
<title>Folio: #{{ $order->id }}</title>
{!! Html::style('assets/css/pdf.css') !!}

</head>
<body>
    <div id="page-wrap">
        <table width="100%">
            <tbody>
                <tr>
                    <td width="30%">
                        <img src="{{ asset('assets/images/logos/azteca.jpg') }}" class="img-logo"> <!-- your logo here -->
                    </td>
                    <td width="70%">
                        <h3 class="pdf-title">Cotización</h3>
                        <strong>Folio:</strong> #{{ $order->id }}<br> 
                        <strong>Fecha:</strong> {{$order->start_at}}<br>
                        <strong>Expedido por:</strong> {{$order->user->name}}<br>
                        @if($modo == 1)
                        <strong>Cliente:</strong> {{$order->client->name}}<br>
                        @elseif($modo == 2)
                        <strong>Obra:</strong> {{$order->work->name}}<br>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </tbody>
        </table>

        @if($order->products->count() > 0)
            <p>&nbsp;</p>
            <table width="100%" class="outline-table table-prop">
                <tbody>
                    <tr class="border-bottom border-right grey">
                        <td colspan="5"><strong>Cristales</strong></td>
                    </tr>
                    <tr class="border-bottom border-right center">
                        <td width="10%"><strong>Piezas</strong></td>
                        <td width="40%"><strong>Descripción</strong></td>
                        <td width="15%"><strong>Ancho</strong></td>
                        <td width="15%"><strong>Largo</strong></td>
                        <td width="20%"><strong>Importe (MXN)</strong></td>
                    </tr>

                    @if($order->products->count() > 0)
                            @if($conteo > 0)
                                @foreach($products as $key => $p_s)
                                    <tr class="border-right">
                                        <td class="pad-left">{{ $p_s->cantidad }}</td>
                                        <td class="center">
                                            {{ $p_s->product->name }} ({{ $p_s->product->unity }})
                                            @if(count($p_s->processes) > 0)
                                                @foreach($p_s->processes as $process)
                                                    @if($process->pivot->process_id != 1)
                                                    <br>-{{ $process->name }} (x{{$process->pivot->cantidad}})
                                                    @endif
                                                @endforeach
                                            @endif
                                         </td>
                                        <td class="right-center">{{ $p_s->ancho }}</td>
                                        <td class="right-center">{{ $p_s->largo }}</td>
                                        <td>
                                            @if($order->price == 0)
                                                $ {{ round($importe[$key]) }}
                                            @else
                                                $ {{ $x[$key][$p_s->p] }}
                                            @endif
                                        </td>
                                   </tr>
                                @endforeach
                            @endif
                    @else
                        <div class="alert alert-warning alert-dismissable">
                            Este Cliente aún no tiene pedidos.
                        </div>
                    @endif

                </tbody>
            </table>

        @else
            <div class="alert alert-warning alert-dismissable">
                Este Cliente aún no tiene pedidos.
            </div>
        @endif

        <table width="100%">
            <tbody>
                <tr>
                    <td width="80%">
                    </td>
                    <td width="20%">
                        <strong>Precio</strong><br>
                        Subtotal: ${{$sub}}<br>
                        IVA: ${{$iva}}<br>
                        Total: $ {{ round($precio) }}<br>
                    </td>
                </tr>
            </tbody>
        </table>

        <br><br><br>
        <table>
            <tbody>
                <tr>
                    <td>
                        Modo de Pago: 50% de Anticipo y 50% antes de entregar.<br>
                        La empresa no se reponsabiliza del pedido pasados 3 días de terminado.<br>
                    </td>
                </tr>
                <tr>
                    <td id="info-emp">
                        VidrioTempladoyAluminiosAzteca.com, Av. Aurelio Ortega 1198, Colinas de Atemajac, 45170 Zapopan, Jal. (33-3366-1884)
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>