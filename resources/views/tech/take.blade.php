@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-3">
                </br><a href="{{ route('tech_list_order_all') }}" class="btn btn-info">Ver Pedidos</a></br>
            </div>
            <div class="col-md-6">
                <h3>#Folio ({{ $order->id }}) - Cliente: {{ $order->client->name }}</h3>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($order->status == "C")
                    {!! Form::open(['route'=>['tech_take_update', $order->id], 'method'=>'POST', 'role'=>'form']) !!}
                    @if($conteo > 0)
                        @foreach($products as $key => $product)
                            <h1 class="take-title">
                                {{ $cristales[$key] }} 
                                (x{{$product->cantidad}}) ({{$product->ancho}}x{{$product->largo}})
                            </h1>
                                @foreach($product->processesMaquilables as $llave => $process)
                                <h3 class="take-process"> 
                                    @if($process->pivot->confirmed == 1)
                                        {!! Form::hidden('proceso_hecho['.$key.']['.$llave.']', false) !!}
                                        {!! Form::checkbox('proceso_hecho['.$key.']['.$llave.']','yes',true) !!}
                                    @else
                                        {!! Form::hidden('proceso_hecho['.$key.']['.$llave.']', true) !!}
                                        {!! Form::checkbox('proceso_hecho['.$key.']['.$llave.']','yes',false) !!}
                                    @endif
                                    {{ $process->name }} <div id="veces">({{$process->pivot->cantidad}} Vez)</div>

                                    @if($process->pivot->confirmed == 1)
                                        @if($process->pivot->user_id != NULL)
                                            - Hecho: {{ $stereo[$key][$llave] }}
                                        @endif
                                    @endif
                                </h3>
                                <input type="hidden" name="proceso_id_value[{{$key}}][{{$llave}}]" value="{{$process->pivot->id}}" />
                                @endforeach
                        @endforeach
                    @else
                        <div class="alert alert-warning alert-dismissable">
                            Este Pedido aún no tiene procesos.
                        </div>
                    @endif
                    </br></br></br>
                    <button class="btn btn-info btn-lg">Actualizar</button></br>
                    {!! Form::close() !!}  
                @else
                    @if($order->m != "")
                    <div class="alert alert-warning" role="alert">{{$order->m }}</div>
                    @else
                    <div class="alert alert-warning" role="alert">Por el momento Este pedido esta detenido.</div>
                    @endif
                @endif

            </div><!-- /fin de col -->

            <div class="col-md-3">
                </br><a href="{{ route('tech_details', [$order->id]) }}" class="btn btn-success">Detalles</a></br>
                @if($order->status == "C")
                    <p>
                        Inicio: 
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $order->start_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                        ({{ $order->start_at }})
                    </p>
                    <p>
                        Entrega:
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $order->limited_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                        ({{ $order->limited_at }})
                    </p>

                    @if($order->techs->count() > 0)
                        @foreach($order->techs as $tech)
                            @if($tech->id == \Auth::user()->id)    
                                <br><a href="{{ route('garbage') }}" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Agregar Desecho</a>
                            @endif
                        @endforeach
                    @endif
                @endif
            </div><!-- /fin de col -->

        </div><!-- /fin de row -->

    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
@endsection

