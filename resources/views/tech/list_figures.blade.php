@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            </br><a href="{{ route('tech_list_order') }}" class="btn btn-info">Ver Pedidos</a></br>
        </div>
        <div class="col-md-8">
            <h1>Figuras en el Pedido</h1>

            @if($order->figures->count() > 0)
                @foreach($order->figures as $figure)
                    <div class="col-sm-4">
                        <!-- Modal -->
                        <div class="modal fade" id="myModal{{$figure->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                             <div class="modal-dialog modal-lg">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                                 </div>
                                 <div class="modal-body">
                                    <img src="{{ asset('images/figuras/'.$figure->route) }}" class="img-responsive" />
                                 </div>
                                 <div class="modal-footer">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                               </div>
                             </div>
                        </div>
                        <!-- Modal -->
                        <a href="" data-toggle="modal" data-target="#myModal{{$figure->id}}"><img src="{{ asset('images/figuras/'.$figure->route) }}" class="order-image-min" /></a>
                        <br><br>
                    </div>
                @endforeach
            @else
                <div class="alert alert-warning alert-dismissable">
                    Este Pedido aún no tiene imágenes.
                </div>
            @endif
        </div>
        <div class="col-md-2">
            </br><a href="{{ route('tech_details', [$order->id]) }}" class="btn btn-success">Detalles</a></br>
        </div>
    </div>
</div>
@endsection
