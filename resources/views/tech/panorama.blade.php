@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-2">
            </br><a href="{{ route('tech_list_order') }}" class="btn btn-info">Ver Pedidos</a></br>
            </div>
            <div class="col-md-8">
                <h3>#Folio ({{ $order->id }}) - Cliente {{ $order->client->name }} - {{ $order->status }}</h3>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($order->products->count() > 0)
                    <table class="table table-hover">
                        <tr>
                            <th>Piezas</th>
                            <th>Cristal</th>
                            <th>Ancho</th>
                            <th>Largo</th>
                            <th></th>
                        </tr>
                        @foreach($order->products as $product)
                            @if($product->pivot->deleted_at == NULL)
                            <tr data-id="{{ $product->pivot->id }}">
                                <td>{{ $product->pivot->cantidad }}</td>
                                <td>
                                    {{ $product->name }}
                                </td>
                                <td>{{ $product->pivot->ancho }}</td>
                                <td>{{ $product->pivot->largo }}</td> 
                            </tr>
                            @endif
                        @endforeach
                    </table>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Cliente aún no tiene pedidos.
                    </div>
                @endif

            </div><!-- /fin de col -->

            <div class="col-md-2">
                @if($order->techs->count() > 0)
                    @foreach($order->techs as $tech)
                        @if($tech->id == \Auth::user()->id)
                        <a href="{{ route('tech_take', [$order->id]) }}" class="btn btn-warning btn-xs">Avanzar Pedido</a> 
                        @else
                        Tomado por: {{ $tech->name }}
                        @endif
                    @endforeach
                @else
                <br><a href="{{ route('tech_take', [$order->id]) }}" class="btn btn-success btn">Tomar pedido</a><br>
                @endif
                <p>
                    Inicio: 
                    <script>
                    moment.locale("es");
                    document.writeln(moment.utc("{{ $order->start_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                    </script>
                    ({{ $order->start_at }})
                </p>
                <p>
                    Entrega:
                    <script>
                    moment.locale("es");
                    document.writeln(moment.utc("{{ $order->limited_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                    </script>
                    ({{ $order->limited_at }})
                </p>
            </div><!-- /fin de col -->

        </div><!-- /fin de row -->

        <br><br>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <h4>Procesos</h4>
                @if($conteo > 0)
                    @foreach($products as $key => $product)
                        <h6>{{ $cristales[$key] }}</h6>
                        @foreach($product->processes as $process)
                            -{{ $process->name }}<br>
                        @endforeach
                    @endforeach
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene procesos.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h4>Imágenes (<a href="" data-toggle="modal" data-target="#myModalImages">Ver</a>)</h4>
                @if($order->images->count() > 0)
                    <!-- Modal -->
                    <div class="modal fade" id="myModalImages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                         <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                             <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                               <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                             </div>
                             <div class="modal-body">
                                <!-- Carousel -->
                                <div class="bs-example" data-example-id="simple-carousel">
                                      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                @foreach($order->images as $key => $image)
                                                    @if($key == 0)
                                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="active"></li>
                                                    @else
                                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"></li>
                                                    @endif
                                                @endforeach  
                                            </ol>    

                                            <div class="carousel-inner" role="listbox">
                                                @foreach($order->images as $i => $image)
                                                    @if($i == 0)
                                                        <div class="item active">
                                                            <img src="{{ $temp[$i] }}" class="tales">
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            <img src="{{ $temp[$i] }}" class="tales">
                                                        </div> 
                                                    @endif
                                                @endforeach  
                                            </div>    

                                            @if(count($order->images) > 1)
                                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            @endif
                                      </div>
                                </div>
                                <!-- Carousel -->
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                             </div>
                           </div>
                         </div>
                    </div>
                    <!-- Modal -->
                    <a href="" data-toggle="modal" data-target="#myModalImages">
                        <img src="{{ $imh }}" class="img-responsive" />
                    </a>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene imágenes.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h4>Figuras (<a href="" data-toggle="modal" data-target="#myModal2">Ver</a>)</h4>
                @if($order->figuresVigent->count() > 0)
                    <!-- Modal -->
                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                         <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                             <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                               <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                             </div>
                             <div class="modal-body">
                                <!-- Carousel -->
                                <div class="bs-example" data-example-id="simple-carousel">
                                      <div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                @foreach($order->figuresVigent as $key => $fig)
                                                    @if($key == 0)
                                                        <li data-target="#carousel-example-generic2" data-slide-to="{{$key}}" class="active"></li>
                                                    @else
                                                        <li data-target="#carousel-example-generic2" data-slide-to="{{$key}}"></li>
                                                    @endif
                                                @endforeach  
                                            </ol>    

                                            <div class="carousel-inner" role="listbox">
                                                @foreach($order->figuresVigent as $i => $fig)
                                                    @if($i == 0)
                                                        <div class="item active">
                                                            <img src="{{ $tem[$i] }}" class="tales">
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            <img src="{{ $tem[$i] }}" class="tales">
                                                        </div> 
                                                    @endif
                                                @endforeach  
                                            </div>    

                                            @if(count($order->figuresVigent) > 1)
                                                <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            @endif
                                      </div>
                                </div>
                                <!-- Carousel -->
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                             </div>
                           </div>
                         </div>
                    </div>
                    <!-- Modal -->
                    <a href="" data-toggle="modal" data-target="#myModal2">
                        <img src="{{ $imf }}" class="img-responsive" />
                    </a>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene figuras.
                    </div>
                @endif
            </div>
            <div class="col-md-2">
                <h4>Operadores</h4>
                @if($order->techs->count() > 0)
                    @foreach($order->techs as $tech)
                        {{ $tech->name }}
                    @endforeach
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene operadores.
                    </div>
                @endif
            </div>
            <div class="col-md-1">
            </div>
        </div>

    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
@endsection

