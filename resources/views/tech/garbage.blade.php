@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-3">
                </br><a href="{{ route('tech_list_order') }}" class="btn btn-info">Ver Pedidos</a></br>
            </div>
            <div class="col-md-6">
                <h3>Agregar Desecho</h3>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'garbage_create', 'method'=>'POST', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cristal*</label>
                        <div class="col-md-6">
                            {!! Form::select('product_id',[''=>'Seleccionar']+$products,null,['class'=>'form-control name_list', 'onchange'=>'price()']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Ancho*</label>
                        <div class="col-md-6">
                            {!! Form::text('a',null,['class'=>'form-control', 'placeholder'=>'Ancho del Desperdicio']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Largo*</label>
                        <div class="col-md-6">
                            {!! Form::text('l',null,['class'=>'form-control', 'placeholder'=>'Largo del Desperdicio']) !!}
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Enviar
                            </button>
                        </div>
                    </div>
                {!! Form::close() !!}

            </div><!-- /fin de col -->

            <div class="col-md-3">
            </div><!-- /fin de col -->

        </div><!-- /fin de row -->

    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
@endsection

