@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('partials.commands')
            @if(Session::has('message'))
                <br>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif

            <h1 class="text-center">Lista Completa de Pedidos</h1>

            <table class="table table-hover">
                <tr>
                    <th>Cliente</th>
                    <th>Status</th>
                    <th>Avance</th>
                    <th>Creada el</th>
                    <th></th>
                </tr>
                @foreach($orders as $h => $order)
                <tr>
                    <td>{{ $order->client->name }}</td>
                    <td>
                        @if($order->status == "C")
                            Confirmado
                        @elseif($order->status == "S")
                            StandBy
                            @if($order->m != "")
                            : {{$order->m}}
                            @endif
                        @endif
                    </td>
                    <td>{{ $avan[$h] }} %</td>
                    <td>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $order->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </td>
                    <td>
                        @if($order->techs->count() > 0)
                            @foreach($order->techs as $tech)
                                @if($tech->id == \Auth::user()->id)
                                <a href="{{ route('tech_details', [$order->id]) }}" class="btn btn-primary btn-xs">Detalles</a> 
                                <a href="{{ route('tech_take', [$order->id]) }}" class="btn btn-warning btn-xs">Avanzar</a> 
                                @else
                                Tomado por: {{ $tech->name }}
                                @endif
                            @endforeach
                        @else
                        <a href="{{ route('tech_details', [$order->id]) }}" class="btn btn-primary btn-xs">Detalles</a>
                        <a href="{{ route('tech_take', [$order->id]) }}" class="btn btn-success btn-xs">Avanzar</a> 
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>

            
            
            <table class="table table-hover">
                <tr>
                    <th>Obra</th>
                    <th>Status</th>
                    <th>Avance</th>
                    <th>Creada el</th>
                    <th></th>
                </tr>
                @foreach($orders2 as $z => $order)
                <tr>
                    <td>{{ $order->work->name }}</td>
                    <td>
                        @if($order->status == "C")
                            Confirmado
                        @elseif($order->status == "S")
                            StandBy
                            @if($order->m != "")
                            : {{$order->m}}
                            @endif
                        @endif
                    </td>
                    <td>{{ $avan2[$z] }} %</td>
                    <td>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $order->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </td>
                    <td>
                        @if($order->techs->count() > 0)
                            @foreach($order->techs as $tech)
                                
                                @if($tech->id == \Auth::user()->id)
                                <a href="{{ route('tech_details_orca', [$order->id]) }}" class="btn btn-primary btn-xs">Detalles</a> 
                                <a href="{{ route('tech_take_orca', [$order->id]) }}" class="btn btn-warning btn-xs">Avanzar</a> 
                                @else
                                Tomado por: {{ $tech->name }}
                                @endif
                            @endforeach
                        @else
                        <a href="{{ route('tech_details_orca', [$order->id]) }}" class="btn btn-primary btn-xs">Detalles</a>
                        <a href="{{ route('tech_take_orca', [$order->id]) }}" class="btn btn-success btn-xs">Avanzar</a> 
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>


        </div>
    </div>
</div>
@endsection
