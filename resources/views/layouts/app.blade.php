<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vidrios Azteca</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <!-- Styles -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/css/style.css') !!}
    @yield('script_head')
</head>
<body id="app-layout">
    @yield('script_body')

    @if(Auth::check())
        @if(Auth::user()->mode() == 'orca')
        <nav class="navbar nav-orca navbar-static-top">
        @elseif(Auth::user()->mode() == 'azteca')
        <nav class="navbar nav-aztek navbar-static-top">
        @else
        <nav class="navbar nav-aztek navbar-static-top">
        @endif
    @else
    <nav class="navbar navbar-default navbar-static-top">
    @endif

        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Vidrios Azteca
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @if(Auth::check())
                        @if(Auth::user()->isTech())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('tech_list_order_all') }}"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Todos los Pedidos</a></li>
                                <!-- <li><a href="{{ route('tech_list_order_orca') }}"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Pedidos para Orca</a></li>
                                <li><a href="{{ route('tech_list_order') }}"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Pedidos para Clientes</a></li> -->
                            </ul>
                        </li>
                        <li>    
                            <a href="{{ route('notifications') }}"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> 
                                Notificaciones 
                                @if(Auth::user()->getNotifications() > 0)
                                    <span class="badge red-n">{{ Auth::user()->getNotifications() }}</span>
                                @endif
                            </a>
                        </li>
                        @endif

                        @if(Auth::user()->isAdmin())
                        <li class="dropdown">
                            @if(Auth::check())
                                @if(Auth::user()->mode() == 'orca')
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Agregar (Orca)<span class="caret"></span></a>
                                @elseif(Auth::user()->mode() == 'azteca')
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Agregar (Azteca)<span class="caret"></span></a>
                                @endif
                            @endif
                            <ul class="dropdown-menu">
                                @if(Auth::check())
                                    @if(Auth::user()->mode() == 'orca')
                                    <li><a href="{{ route('add_work_orca') }}"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> Obra a Orca</a></li>
                                    <li><a href="{{ route('add_order') }}"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> Pedido a Orca</a></li>
                                    @elseif(Auth::user()->mode() == 'azteca')
                                    <li><a href="{{ route('add_work') }}"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> Pedido a Cliente</a></li>
                                    @endif
                                @endif
                                <li><a href="{{ route('add_product') }}"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Producto a Stock</a></li>
                                <li><a href="{{ route('add_client') }}"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Cliente</a></li>
                                <li><a href="{{ route('add_incident') }}"><span class="glyphicon glyphicon-record" aria-hidden="true"></span> Incidente</a></li>
                                <li><a href="{{ route('add_figure') }}"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Figura</a></li>
                                <li><a href="{{ route('add_process') }}"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Proceso</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @if(Auth::check())
                                    @if(Auth::user()->mode() == 'orca')
                                    <li><a href="{{ route('list_work') }}"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> Obras a Orca</a></li>
                                    @elseif(Auth::user()->mode() == 'azteca')
                                    <li><a href="{{ route('list_order') }}"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Pedidos a Clientes </a></li>
                                    @endif
                                @endif
                                <li><a href="{{ route('list_product') }}"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Productos</a></li>
                                <li><a href="{{ route('list_process') }}"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Procesos</a></li>
                                <li><a href="{{ route('list_client') }}"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Clientes</a></li>
                                <li><a href="{{ route('list_incident') }}"><span class="glyphicon glyphicon-record" aria-hidden="true"></span> Incidentes</a></li>
                                <!--<li><a href="{{ route('list_operator') }}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Operadores</a></li>-->
                                <li><a href="{{ route('list_figure') }}"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Figuras</a></li>
                                <li><a href="{{ route('list_budget') }}"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Cotizaciones</a></li>
                                <li><a href="{{ route('list_garbage') }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Desperdicios</a></li>
                                <li><a href="{{ route('tech_list_order_all') }}"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Avances</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('statistic_stock') }}"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Estadísticas</a></li>
                        <li>    
                            <a href="{{ route('notifications') }}"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> 
                                Notificaciones 
                                @if(Auth::user()->getNotifications() > 0)
                                    <span class="badge red-n">{{ Auth::user()->getNotifications() }}</span>
                                @endif
                            </a>
                        </li>
                        {!! Form::open(['route'=>'command_line', 'method'=>'GET', 'role'=>'form', 'class'=>'navbar-form navbar-left']) !!}
                            <div class="form-group">
                                {!! Form::text('query',null,['class'=>'form-control', 'placeholder'=>'Buscar']) !!}
                            </div>
                            {!! Form::hidden('mode', 'search_general') !!}
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        {!! Form::close() !!}
                        @endif
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if(Auth::guest())
                        <li><a href="{{ route('login') }}"><i class="fa fa-btn fa-sign-in"></i>Login</a></li>
                        <li><a href="{{ route('signup') }}"><i class="fa fa-btn fa-user"></i>Registro</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('settings') }}"><span class="glyphicon glyphicon-cog"></span> Configuración</a></li>
                                <li><a href="{{ route('settings') }}"><span class="glyphicon glyphicon-wrench"></span> Optimización</a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @yield('script_footer')
</body>
</html>
