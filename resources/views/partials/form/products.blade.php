                <div class="row" id="cp1">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas1', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[0]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id1' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[0]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho1', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[0]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo1', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <div class="row" id="cp2">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[1]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas2', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[1]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id2' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[1]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho2', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[1]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo2', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <div class="row" id="cp3">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[2]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas3', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[2]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id3' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[2]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho3', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[2]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo3', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <div class="row" id="cp4">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[3]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas4', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[3]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id4' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[3]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho4', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[3]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo4', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <div class="row" id="cp5">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[4]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas5', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[4]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id5' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[4]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho5', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[4]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo5', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <div class="row" id="cp6">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[5]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas6', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[5]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id6' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[5]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho6', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[5]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo6', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <div class="row" id="cp7">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[6]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas7', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[6]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id7' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[6]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho7', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[6]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo7', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <div class="row" id="cp8">
                    <div class="col-xs-2">
                        {!! Form::text('cantidad[7]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'piezas8', 'placeholder'=>'Piezas']) !!}
                    </div>
                    <div class="col-xs-4">
                        {!! Form::select('product[7]',[''=>'Seleccionar']+$products,null,['class'=>'form-control', 'id'=>'product_id8' ,'onchange'=>'price()']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('ancho[7]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'ancho8', 'placeholder'=>'Ancho']) !!}
                    </div>
                    <div class="col-xs-3">
                        {!! Form::text('largo[7]',null,['class'=>'form-control', 'onchange'=>'price()', 'id'=>'largo8', 'placeholder'=>'Largo']) !!}
                    </div>
                </div>
                <br><br>