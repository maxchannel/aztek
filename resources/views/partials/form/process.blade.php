                <!-- procesos -->
                <div id="addAttr3" class="btn btn-info btn-xs">Más +</div><br>
                {!! Form::label('material', 'Procesos') !!}  
                <div class="row">
                    <!-- proceso -->
                    <div class="col-xs-3">
                        <input type="checkbox" name="barreno_n" onclick="showMe('proc1', 'barreno_n')">
                        {!! Form::label('barreno', 'Barreno') !!}
                    </div>
                    <div id="proc1" style="display:none">
                        <div class="col-xs-2" >
                            {!! Form::text('cantidad_p[0]',\Request::input('cantidad1'),['class'=>'form-control', 'id'=>'nm1', 'placeholder'=>'#']) !!}
                        </div>
                        <div class="col-xs-7">
                            {!! Form::select('barreno[0]',$processes,null,['class'=>'form-control', 'id'=>'barreno_id', 'onchange'=>'price()']) !!}
                        </div>
                    </div><br><br><br>
                    @foreach($processes_t as $process_t)
                        <input type="hidden" id="barreno_id_{{$process_t->id}}" value="{{$process_t->price}}">
                    @endforeach
                    <!-- proceso -->
                    <!-- proceso -->
                    <div class="col-xs-3">
                        <input type="checkbox" name="resaque_s" onclick="showMe('proc2', 'resaque_s')">
                        {!! Form::label('resaque', 'Resaque') !!}
                    </div>
                    <div id="proc2" style="display:none">
                        <div class="col-xs-2">
                            {!! Form::text('cantidad_p[1]',\Request::input('cantidad2'),['class'=>'form-control', 'id'=>'nm1', 'placeholder'=>'#']) !!}
                        </div>
                        <div class="col-xs-7">
                            {!! Form::select('process[1][1]',['' => 'Seleccionar', '1' => 'Barreno 4m'],\Request::input('resaque'),['class'=>'form-control']) !!}
                        </div>
                    </div><br><br><br>
                    <!-- proceso -->
                    <!-- proceso -->
                    <div class="col-xs-3">
                        <input type="checkbox" name="canto_s" onclick="showMe('proc3', 'canto_s')">
                        {!! Form::label('canto', 'Canto') !!}
                    </div>
                    <div id="proc3" style="display:none">
                        <div class="col-xs-2">
                            {!! Form::text('cantidad_p[2]',\Request::input('cantidad3'),['class'=>'form-control', 'id'=>'nm1', 'placeholder'=>'#']) !!}
                        </div>
                        <div class="col-xs-7">
                            {!! Form::select('process[2][2]',['' => 'Seleccionar', '1' => 'Barreno 4m'],\Request::input('canto'),['class'=>'form-control']) !!}
                        </div>
                    </div><br><br><br>
                    <!-- proceso -->
                    <!-- proceso -->
                    <div class="col-xs-3">
                        <input type="checkbox" name="bisel_s" onclick="showMe('proc4', 'bisel_s')">
                        {!! Form::label('bisel', 'Bisel') !!}
                    </div>
                    <div id="proc4" style="display:none">
                        <div class="col-xs-2">
                            {!! Form::text('cantidad_p[3]',\Request::input('cantidad4'),['class'=>'form-control', 'id'=>'nm1', 'placeholder'=>'#']) !!}
                        </div>
                        <div class="col-xs-7">
                            {!! Form::select('process[3][3]',['' => 'Seleccionar', '1' => 'Barreno 4m'],\Request::input('bisel'),['class'=>'form-control']) !!}
                        </div>
                    </div><br><br><br>
                    <!-- proceso -->
                    <!-- proceso -->
                    <div class="col-xs-3">
                        <input type="checkbox" name="pelicula_s" onclick="showMe('proc5', 'pelicula_s')">
                        {!! Form::label('pelicula', 'Película') !!}
                    </div>
                    <div id="proc5" style="display:none">
                        <div class="col-xs-2">
                            {!! Form::text('cantidad_p[4]',\Request::input('cantidad5'),['class'=>'form-control', 'id'=>'nm1', 'placeholder'=>'#']) !!}
                        </div>
                        <div class="col-xs-7">
                            {!! Form::select('process[4][4]',['' => 'Seleccionar', '1' => 'Barreno 4m'],\Request::input('pelicula'),['class'=>'form-control']) !!}
                        </div>
                    </div><br><br><br>
                    <!-- proceso -->
                    <!-- proceso -->
                    <div class="col-xs-3">
                        <input type="checkbox" name="esmeril_s" onclick="showMe('proc6', 'esmeril_s')">
                        {!! Form::label('esmeril', 'Esmerilado') !!}
                    </div>
                    <div id="proc6" style="display:none">
                        <div class="col-xs-2">
                            {!! Form::text('cantidad_p[5]',\Request::input('cantidad6'),['class'=>'form-control', 'id'=>'nm1', 'placeholder'=>'#']) !!}
                        </div>
                        <div class="col-xs-7">
                            {!! Form::select('process[5][5]',['' => 'Seleccionar', '1' => 'Barreno 4m'],\Request::input('esmerilado'),['class'=>'form-control']) !!}
                        </div>
                    </div><br><br><br>
                    <!-- proceso -->
                    <!-- otros -->
                    <div class="col-xs-3">
                        <input type="checkbox" name="otros_s" onclick="showMe('proc7', 'otros_s')">
                        {!! Form::label('otros', 'Otros') !!}
                    </div>
                    <div id="proc7" style="display:none">
                        <div class="col-xs-2">
                            {!! Form::text('cantidad_p[6]',\Request::input('cantidad7'),['class'=>'form-control', 'id'=>'nm1', 'placeholder'=>'#']) !!}
                        </div>
                        <div class="col-xs-7">
                            {!! Form::select('process[6][6]',['' => 'Seleccionar', '1' => 'Barreno 4m'],\Request::input('otros'),['class'=>'form-control']) !!}
                        </div>
                    </div><br><br><br>
                    <!-- otros -->
                </div><br>
                <!-- procesos -->

