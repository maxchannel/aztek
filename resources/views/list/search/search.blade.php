@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Busqueda: {{ $query }}</h1>
            
            <table class="table table-hover">
                <tr>
                    <th>Resultado</th>
                    <th>Tipo de Resultado</th>
                    <th>Creado el</th>
                    <th></th>
                </tr>

                <!-- Obras-->
                @if($results_obra->count() > 0)
                    @foreach($results_obra as $result)
                    <tr>
                        <td>{{ $result->name }}</td>
                        <td>Obra a Orca</td> 
                        <td>{{ $result->created_at }}</td> 
                        <td>
                            <a href="{{ route('add_work_panorama_orca', [$result->id]) }}" class="btn btn-success btn-xs">Pedidos</a> 
                            <a href="{{ route('edit_work', [$result->id]) }}" class="btn btn-warning btn-xs">Editar</a> 
                        </td>
                    </tr>
                    @endforeach
                @else
                <tr>
                    <td>Sin resultado en Obras</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @endif

                <!-- Pedidos -->
                @if($results_pedido->count() > 0)
                    @foreach($results_pedido as $order)
                    <tr>
                        <td>{{ $order->name }}</td>
                        <td>Pedido</td> 
                        <td>{{ $order->created_at }}</td> 
                        <td>
                        <a href="{{ route('add_work_panorama', [$order->id]) }}" class="btn btn-success btn-xs">Panorama</a> 
                        <a href="{{ route('pdf_cotizacion', [$order->id]) }}" class="btn btn-info btn-xs" target="_blank">Ver PDF</a>
                        <a href="{{ route('add_work_edit', [$order->id]) }}" class="btn btn-warning btn-xs">Editar</a> 
                    </td>
                    </tr>
                    @endforeach
                @endif

                <!-- Clients -->
                @if($results_cliente->count() > 0)
                    @foreach($results_cliente as $cliente)
                    <tr>
                        <td>{{ $cliente->name }}</td>
                        <td>Cliente</td> 
                        <td>{{ $cliente->created_at }}</td> 
                        <td>
                            <a href="{{ route('edit_client', [$cliente->id]) }}" class="btn btn-warning btn-xs">Editar</a>
                        </td>
                    </tr>
                    @endforeach
                @endif

                <!-- Productos -->
                @if($results_product->count() > 0)
                    @foreach($results_product as $product)
                    <tr>
                        <td>{{ $product->name }}</td>
                        <td>Producto en Stock</td> 
                        <td>{{ $product->created_at }}</td> 
                        <td>
                            <a href="{{ route('edit_product', [$product->id]) }}" class="btn btn-warning btn-xs">Editar</a>
                        </td>
                    </tr>
                    @endforeach
                @endif

                <!-- Process -->
                @if($results_process->count() > 0)
                    @foreach($results_process as $process)
                    <tr>
                        <td>{{ $process->name }}</td>
                        <td>Proceso</td> 
                        <td>{{ $process->created_at }}</td> 
                        <td>
                            <a href="{{ route('edit_process', [$process->id]) }}" class="btn btn-warning btn-xs">Editar</a>
                        </td>
                    </tr>
                    @endforeach
                @endif

                
            </table>
        </div>
    </div>
</div>
@endsection
