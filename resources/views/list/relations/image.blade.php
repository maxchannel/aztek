@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-8">
                <h1>Imágenes del Pedido {{ $order->name }}</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($order->images->count() > 0)
                    @foreach($order->images as $image)
                        <div class="col-sm-4">
                            <img src="{{ asset('images/obras/'.$image->route) }}" class="order-image-min" />
                            {!! Form::open(['route'=>['delete_order_image', $image->id], 'method'=>'DELETE']) !!}
                            {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm btn-block', 'style' => 'margin: 10px 0']) !!}
                            {!! Form::close() !!}
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene imágenes.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h2>Información</h2>
                <a href="{{ route('edit_orderStore', [$order->id]) }}" class="btn btn-success">Volver a editar Información</a>

                <!-- images -->
                {!! Form::open(['route'=>['list_order_images_store', $order->id], 'method'=>'PUT', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                <h3>Subir más Imágenes</h3>
                <p class="text-muted">Puedes subir multiples imágenes al Pedido</p>
                @if(Session::has('image-message'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('image-message') }}
                    </div>
                @endif
                
                {!! Form::file('file[]',['multiple' => 'multiple', 'id' => 'multiple-files', 'accept' => 'image/*']) !!}
                <br>
                <button type="submit" class="btn btn-primary">Enviar</button>

                {!! Form::close() !!}
                <!-- images -->
            </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/img-upl.js') }}"></script>
<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $(".delete-button").click(function(){
        if (!confirm("Confirmas eliminar la imágen?")){
          return false;
        }
    });
</script>
@endsection
