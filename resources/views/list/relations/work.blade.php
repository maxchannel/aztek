@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Pedidos para {{ $work->name }}</h1>
            <a href="">Regresar a Obras</a>
            <table class="table table-hover">
                <tr>
                    <th>Nombre</th>
                    <th>Status</th>
                    <th>Ténico a Cargo</th>
                    <th>Creado el</th>
                    <th></th>
                </tr>
                @foreach($work->orders as $order)
                <tr>
                    <td>{{ $order->name }}</td>
                    <td>90%</td> 
                    <td>Omar</td>
                    <td>{{ $order->created_at }}</td> 
                    <td>
                        <!-- <a href="" class="btn btn-info btn-xs">Tareas (14)</a> -->
                        <a href="{{ route('list_order_images', $order->id) }}" class="btn btn-success btn-xs">Imagenes ({{ $order->images->count() }})</a>
                        <a href="{{ route('list_order_products', $order->id) }}" class="btn btn-success btn-xs">Material ({{ $order->products->count() }})</a>
                        <a href="{{ route('edit_order', $order->id) }}" class="btn btn-warning btn-xs">Editar</a> 
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection
