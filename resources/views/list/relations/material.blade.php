@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-8">
                <h1>Material del Pedido {{ $order->name }}</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($order->products->count() > 0)
                    <table class="table table-hover">
                        <tr>
                            <th>Nombre</th>
                            <th>Utilizado</th>
                            <th>Precio Unitario</th>
                            <th>Unidad</th>
                            <th></th>
                        </tr>
                        @foreach($order->products as $product)
                            <tr data-id="{{ $product->pivot->id }}">
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->pivot->cantidad }}</td>
                                <td>{{ $product->price }} $</td>
                                <td>{{ $product->unity }}</td> 
                                <td>
                                    <a href="#" class="btn-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene productos del stock.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h2>Información</h2>
            </div>
    </div>
</div>

{!! Form::open(['route'=>['delete_order_product', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}

@endsection

@section('script_footer')
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection
