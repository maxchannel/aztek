@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('partials.commands')
            <h1 class="text-center">Lista de Productos en Stock</h1>
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif


            <p class="pull-right">
                Ordenar(
                    @if(\Request::input('sort') == 'name' || \Request::input('sort') == 'name_a')
                    Nombre
                    @else
                    <a href="{{ route('list_product', ['sort'=>'name']) }}">Nombre</a>
                    @endif
                    /
                    @if(\Request::input('sort') == 'fecha' || \Request::input('sort') == 'fecha_a')
                    Fecha
                    @else
                    <a href="{{ route('list_product', ['sort'=>'fecha']) }}">Fecha</a>
                    @endif
                    )    

                    @if(\Request::input('sort') == 'name')
                    <a href="{{ route('list_product', ['sort'=>'name_a']) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    @elseif(\Request::input('sort') == 'name_a')
                    <a href="{{ route('list_product', ['sort'=>'name']) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                    @endif    

                    @if(\Request::input('sort') == 'fecha')
                    <a href="{{ route('list_product', ['sort'=>'fecha_a']) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    @elseif(\Request::input('sort') == 'fecha_a')
                    <a href="{{ route('list_product', ['sort'=>'fecha']) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                    @endif
            </p>

            <table class="table table-hover">
                <tr>
                    <th>Nombre</th>
                    <th>Reserva</th>
                    <th>P. Vidriero</th>
                    <th>P. Público</th>
                    <th>P. Instalación</th>
                    <th>Orca Instalación</th>
                    <th>Orca Suministro</th>
                    <th>Unidad</th>
                    <th>Creado</th>
                    <th></th>
                </tr>
                @foreach($products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->price1 }}</td>
                    <td>{{ $product->price2 }}</td>
                    <td>{{ $product->price3 }}</td>
                    <td>{{ $product->price4 }}</td>
                    <td>{{ $product->price5 }}</td>
                    <td>{{ $product->unity }}</td>
                    <td>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $product->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </td>
                    <td><a href="{{ route('edit_product', [$product->id]) }}" class="btn btn-warning btn-xs">Editar</a></td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $products->render()) !!}
        </div>
    </div>
</div>
@endsection
