@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-hover">
                <tr>
                    <th>Fecha</th>
                    <th>Nombre</th>
                    <th>Contacto</th>
                    <th>Mensaje</th>
                </tr>
                @foreach($buds as $b)
                <tr data-id="{{ $b->id }}">
                    <td>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $b->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </td>
                    <td>{{ $b->name }}</td>
                    <td>{{ $b->c }}</td>
                    <td>{{ $b->m }}</td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $buds->render()) !!} 
        </div>
    </div>
</div>

{!! Form::open(['route'=>['destroy_budget', ':ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){3
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection