@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <input type="text" class="form-control" placeholder="Buscar"><br>
            <table class="table table-hover">
                <tr>
                    <th>Nombre</th>
                    <th>Carga de Trabajo</th>
                    <th></th>
                </tr>
                <tr>
                    <td>Operador 1</td>
                    <td>3 Tareas por hacer (3 horas de tiempo estimado)</td> 
                    <td><a href="#" class="btn btn-info btn-xs">Ver Tareas</a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection
