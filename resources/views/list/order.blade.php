@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('partials.commands')
            @if(Session::has('message'))
                <br>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif

            <h1 class="text-center">Lista de Pedidos a Clientes</h1>
            <p class="pull-right">
                @if(\Request::input('m') == 't')
                    <a href="{{ route('list_order', ['sort'=>\Request::input('sort')]) }}">Por Terminar</a>
                @else
                    <a href="{{ route('list_order', ['m'=>'t', 'sort'=>\Request::input('sort')]) }}">Terminadas</a>
                @endif

                - Ordenar(
                    @if(\Request::input('sort') == 'name' || \Request::input('sort') == 'name_a')
                    Nombre
                    @else
                    <a href="{{ route('list_order', ['sort'=>'name', 'm'=>\Request::input('m')]) }}">Nombre</a>
                    @endif
                    /
                    @if(\Request::input('sort') == 'fecha' || \Request::input('sort') == 'fecha_a')
                    Fecha
                    @else
                    <a href="{{ route('list_order', ['sort'=>'fecha', 'm'=>\Request::input('m')]) }}">Fecha</a>
                    @endif
                    )    

                    @if(\Request::input('sort') == 'name')
                    <a href="{{ route('list_order', ['sort'=>'name_a', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    @elseif(\Request::input('sort') == 'name_a')
                    <a href="{{ route('list_order', ['sort'=>'name', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                    @endif    

                    @if(\Request::input('sort') == 'fecha')
                    <a href="{{ route('list_order', ['sort'=>'fecha_a', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    @elseif(\Request::input('sort') == 'fecha_a')
                    <a href="{{ route('list_order', ['sort'=>'fecha', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                    @endif
            </p>

            <table class="table table-hover">
                <tr>
                    <th>Cliente</th>
                    <th>Status</th>
                    <th>Avance</th>
                    <th>Creada el</th>
                    <th></th>
                </tr>
                @foreach($orders as $h => $order)
                <tr>
                    <td>{{ $order->client->name }}</td>
                    <td>
                        @if($order->status == "C")
                            Confirmado
                        @elseif($order->status == "S")
                            StandBy
                            @if($order->m != "")
                            : {{$order->m}}
                            @endif
                        @elseif($order->status == "P")
                            Presupuesto
                        @elseif($order->status == "T")
                            Terminada
                        @endif
                    </td>
                    <td>{{ $avan[$h] }} %</td>
                    <td>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $order->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </td>
                    <td>
                        <a href="{{ route('add_work_panorama', [$order->id]) }}" class="btn btn-success btn-xs">Panorama</a> 
                        <a href="{{ route('tech_take', [$order->id]) }}" class="btn btn-danger btn-xs">Avance</a> 
                        <a href="{{ route('pdf_cotizacion', [$order->id]) }}" class="btn btn-info btn-xs" target="_blank">Ver PDF</a>
                        <a href="{{ route('add_work_edit', [$order->id]) }}" class="btn btn-warning btn-xs">Editar</a> 
                    </td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $orders->render()) !!}
        </div>
    </div>
</div>
@endsection
