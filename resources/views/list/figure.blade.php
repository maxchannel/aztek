@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('partials.commands')
            <h1 class="text-center">Lista de Figuras</h1>
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif

            <table class="table table-hover">
                <tr>
                    <th>Nombre</th>
                    <th>Imágen</th>
                    <th>Creado</th>
                    <th></th>
                </tr>
                @foreach($figures as $i => $figure)
                <tr>
                    <td>{{ $figure->name }}</td>
                    <td>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal{{$figure->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                             <div class="modal-dialog modal-lg">
                               <div class="modal-content">
                                 <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                   <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                                 </div>
                                 <div class="modal-body">
                                    <img src="{{ $temp[$i] }}" class="img-responsive" />
                                 </div>
                                 <div class="modal-footer">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 </div>
                               </div>
                             </div>
                        </div>
                        <!-- Modal -->
                        <a href="" data-toggle="modal" data-target="#myModal{{$figure->id}}"><img src="{{ $temp[$i] }}" class="img-figure"  /></a>
                    </td>
                    <td>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $figure->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </td>
                    <td><a href="{{ route('edit_figure', [$figure->id]) }}" class="btn btn-warning btn-xs">Editar</a></td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $figures->render()) !!} 
        </div>
    </div>
</div>
@endsection
