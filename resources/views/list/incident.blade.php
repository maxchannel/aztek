@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('partials.commands')
            <h1 class="text-center">Lista de Incidentes en Obras</h1>

            <p class="pull-right">
                Ordenar(
                    @if(\Request::input('sort') == 'name' || \Request::input('sort') == 'name_a')
                    Nombre
                    @else
                    <a href="{{ route('list_incident', ['sort'=>'name']) }}">Nombre</a>
                    @endif
                    /
                    @if(\Request::input('sort') == 'fecha' || \Request::input('sort') == 'fecha_a')
                    Fecha
                    @else
                    <a href="{{ route('list_incident', ['sort'=>'fecha']) }}">Fecha</a>
                    @endif
                    )    

                    @if(\Request::input('sort') == 'name')
                    <a href="{{ route('list_incident', ['sort'=>'name_a']) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    @elseif(\Request::input('sort') == 'name_a')
                    <a href="{{ route('list_incident', ['sort'=>'name']) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                    @endif    

                    @if(\Request::input('sort') == 'fecha')
                    <a href="{{ route('list_incident', ['sort'=>'fecha_a']) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    @elseif(\Request::input('sort') == 'fecha_a')
                    <a href="{{ route('list_incident', ['sort'=>'fecha']) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                    @endif
            </p>

            <table class="table table-hover">
                <tr>
                    <th>Content</th>
                    <th>Pedido</th>
                    <th></th>
                </tr>
                @foreach($incidents as $incident)
                <tr>
                    <td>{{ $incident->content }}</td>
                    <td>
                        <script>
                        moment.locale("es");
                        document.writeln(moment.utc("{{ $incident->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                        </script>
                    </td>
                    <td><a href="{{ route('edit_incident', [$incident->id]) }}" class="btn btn-warning btn-xs">Editar</a></td>
                </tr>
                @endforeach
            </table>
            {!! str_replace('/?', '?', $incidents->render()) !!}
        </div>
    </div>
</div>
@endsection
