@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Configuración de Perfil</div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($user, ['route'=>'settings_store', 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        <label class="col-md-4 control-label">Nombre</label>
                        <div class="col-md-6">
                            {!! Form::text('name',null,['class'=>'form-control', 'maxlength'=>'30', 'placeholder'=>'Nombre para mostrar']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Modo</label>
                        <div class="col-md-6">
                            {!! Form::select('mode',
                                    ['orca' => 'Orca', 'azteca' => 'Azteca']
                                ,null,['class'=>'form-control']) !!}
                            
                        </div>
                    </div>   

                    </br>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i>Enviar
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
