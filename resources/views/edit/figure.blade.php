@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('list_figure') }}" class="btn btn-success">Volver</a>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Crear Figura</div>
                <div class="panel-body">

                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::model($figure,['route'=>['edit_figure_update', $figure->id], 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nombre</label>
                        <div class="col-md-6">
                           {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre del Producto']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Actual Imágen</label>
                        <div class="col-md-6">
                            <!-- Modal -->
                             <div class="modal fade" id="myModal{{$figure->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                                      </div>
                                      <div class="modal-body">
                                         <img src="{{ $imh }}" class="img-responsive" />
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                             </div>
                             <!-- Modal -->
                             <a href="" data-toggle="modal" data-target="#myModal{{$figure->id}}"><img src="{{ $imh }}" class="img-figure"  /></a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nueva Imágen de Figura</label>
                        <div class="col-md-6">
                           {!! Form::file('file', ['accept' => 'image/*']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Enviar
                            </button>
                        </div>
                    </div>
                {!! Form::close() !!}

                {!! Form::open(['route'=>['destroy_figure', $figure->id], 'method'=>'DELETE']) !!}
                {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm', 'style' => 'margin: 10px 0']) !!}
                {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $(".delete-button").click(function(){
        if (!confirm("Confirmas que quiere Eliminar?")){
          return false;
        }
    });
</script>
@endsection

