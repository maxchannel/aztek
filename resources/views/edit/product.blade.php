@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('list_product') }}" class="btn btn-success">Volver</a>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Producto</div>
                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                            <a href="{{ route('list_product') }}">Ver Productos</a>
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($product,['route'=>['edit_productStore', $product->id], 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre*</label>
                            <div class="col-md-6">
                               {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre del Producto']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Descripción</label>
                            <div class="col-md-6">
                                {!! Form::textarea('description',null,['class'=>'form-control', 'rows'=>'4']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Cantidad Actualmente en Stock*</label>
                            <div class="col-md-6">
                                {!! Form::text('quantity',null,['class'=>'form-control', 'placeholder'=>'Cantidad en Stock (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Cantidad Minima*</label>
                            <div class="col-md-6">
                                {!! Form::text('min',null,['class'=>'form-control', 'placeholder'=>'Cantidad Minima que se debe tener (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Cantidad Maxima*</label>
                            <div class="col-md-6">
                                {!! Form::text('max',null,['class'=>'form-control', 'placeholder'=>'Cantidad Maxima para almacenar (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Unitario*</label>
                            <div class="col-md-6">
                                {!! Form::text('price1',null,['class'=>'form-control', 'placeholder'=>'Precio por unidad (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Unitario 2*</label>
                            <div class="col-md-6">
                                {!! Form::text('price2',null,['class'=>'form-control', 'placeholder'=>'Precio por unidad (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Unitario 3*</label>
                            <div class="col-md-6">
                                {!! Form::text('price3',null,['class'=>'form-control', 'placeholder'=>'Precio por unidad (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Orca Instalación</label>
                            <div class="col-md-6">
                                {!! Form::text('price4',null,['class'=>'form-control', 'placeholder'=>'Precio por unidad (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Orca Suministro</label>
                            <div class="col-md-6">
                                {!! Form::text('price5',null,['class'=>'form-control', 'placeholder'=>'Precio por unidad (Númerico)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Unidad*</label>
                            <div class="col-md-6">
                                {!! Form::text('unity',null,['class'=>'form-control', 'placeholder'=>'Unidad de referencia (m, m2, piezas, etc...)']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                    <p>Al Eliminar Producto eliminarás los pedidos donde este el mismo</p>
                    {!! Form::open(['route'=>['destroy_product', $product->id], 'method'=>'DELETE']) !!}
                    {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm', 'style' => 'margin: 10px 0']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/delete.js') }}"></script>
@endsection


