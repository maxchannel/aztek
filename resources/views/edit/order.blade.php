@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Pedido</div>
                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                            <a href="{{ route('list_work') }}">Ver Pedido</a>
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($order,['route'=>['edit_orderStore', $order->id], 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre del Pedido']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                {!! Form::textarea('description',null,['class'=>'form-control', 'rows'=>'4', 'placeholder'=>'Descripción']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Limited at</label>
                            <div class="col-md-6">
                                {!! Form::text('limited_at',null,['class'=>'form-control', 'placeholder'=>'Nombre de la Obra']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                    </br><a href="{{ route('list_order_images', $order->id) }}" class="btn btn-success"><span class="glyphicon glyphicon-picture"></span> Editar Imágenes</a>
                    </br></br><button type="submit" class="btn btn-danger" onclick="return confirm('¿Confirma que realmente quiere ELIMINAR?')" ><span class="glyphicon glyphicon-trash"></span> Eliminar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
