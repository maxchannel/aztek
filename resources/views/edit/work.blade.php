@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('list_work') }}" class="btn btn-primary">Volver a Obras Orca</a>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Obra ({{ $work->name }})</div>
                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($work,['route'=>['edit_workStore', $work->id], 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6">
                                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre de la Obra']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                {!! Form::textarea('description',null,['class'=>'form-control', 'rows'=>'4', 'placeholder'=>'Descripción']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Residente</label>
                            <div class="col-md-6">
                                {!! Form::text('residente',null,['class'=>'form-control', 'placeholder'=>'Residente de la Obra']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Residente (Contacto)</label>
                            <div class="col-md-6">
                                {!! Form::text('residente_c',null,['class'=>'form-control', 'placeholder'=>'Residente (Contacto)']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Encargado</label>
                            <div class="col-md-6">
                                {!! Form::text('encargado',null,['class'=>'form-control', 'placeholder'=>'Encargado']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="col-md-2">
             <a href="{{ route('add_work_panorama_orca', [$work->id]) }}" class="btn btn-success">Ver Pedidos de esta Obra</a>
        </div><!-- /fin de col -->
    </div>
</div>
@endsection
