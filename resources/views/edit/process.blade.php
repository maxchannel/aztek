@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('list_process') }}" class="btn btn-success">Volver</a>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Proceso</div>
                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($process,['route'=>['edit_process_update', $process->id], 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre*</label>
                            <div class="col-md-6">
                               {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre del Producto']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Vidriero</label>
                            <div class="col-md-6">
                               {!! Form::text('price1',null,['class'=>'form-control', 'placeholder'=>'Precio Vidriero']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Público</label>
                            <div class="col-md-6">
                               {!! Form::text('price2',null,['class'=>'form-control', 'placeholder'=>'Precio Público']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Precio Instalación</label>
                            <div class="col-md-6">
                               {!! Form::text('price3',null,['class'=>'form-control', 'placeholder'=>'Precio Instalación']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Maquila</label>
                            <div class="col-md-6">
                                @if($process->maq == 1)
                                    {!! Form::checkbox('maq','yes',true) !!} Dejar vacio para que no se envie proceso a operador
                                @else
                                    {!! Form::checkbox('maq','yes',false) !!} Dejar vacio para que no se envie proceso a operador
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Unidad</label>
                            <div class="col-md-6">
                               {!! Form::text('unity',null,['class'=>'form-control', 'placeholder'=>'Unidad']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                    {!! Form::open(['route'=>['destroy_process', $process->id], 'method'=>'DELETE']) !!}
                    {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm', 'style' => 'margin: 10px 0']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/delete.js') }}"></script>
@endsection
