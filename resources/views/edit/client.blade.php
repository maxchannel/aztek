@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('list_client') }}" class="btn btn-success">Volver</a>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Cliente</div>
                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                            <a href="{{ route('list_client') }}">Ver Clientes</a>
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::model($client,['route'=>['edit_clientStore', $client->id], 'method'=>'PUT', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre*</label>
                            <div class="col-md-6">
                                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre del Cliente']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Domicilio</label>
                            <div class="col-md-6">
                                {!! Form::text('address',null,['class'=>'form-control', 'placeholder'=>'Domicilio']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Teléfono</label>
                            <div class="col-md-6">
                                {!! Form::text('telephone',null,['class'=>'form-control', 'placeholder'=>'Teléfono del Cliente']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tipo</label>
                            <div class="col-md-6">
                                {!! Form::select('type',
                                    ['' => 'Seleccionar Cliente','Vidriero' => 'Vidriero', 'Público' => 'Público']
                                ,null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                    <p>Al Eliminar Cliente eliminaras los pedidos del mismo</p>
                    {!! Form::open(['route'=>['destroy_client', $client->id], 'method'=>'DELETE']) !!}
                    {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm', 'style' => 'margin: 10px 0']) !!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $(".delete-button").click(function(){
        if (!confirm("Confirmas que quiere Eliminar?")){
          return false;
        }
    });
</script>
@endsection
