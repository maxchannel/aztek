@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @if(Auth::check())
                {!! Form::open(['route'=>'command_line', 'method'=>'GET', 'role'=>'form']) !!}
                    <div class="form-group form-group-lg">
                        {!! Form::text('query',null,['class'=>'form-control', 'placeholder'=>'Busqueda y Linea de Comandos']) !!}
                    </div>
                {!! Form::close() !!}<br><br>   

                @if(Auth::user()->isTech())
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div id="init-orca">
                            <a href="{{ route('tech_list_order_all') }}"></a><br>
                            <a href="{{ route('tech_list_order_all') }}"><img src="{{ asset('assets/images/logos/orca.png') }}" class="img-index"></a>

                            <!-- <a href="{{ route('tech_list_order_orca') }}"></a><br>
                            <a href="{{ route('tech_list_order_orca') }}"><img src="{{ asset('assets/images/logos/orca.png') }}" class="img-index"></a> -->
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div id="init-client">
                            <a href="{{ route('tech_list_order_all') }}"></a><br>
                            <a href="{{ route('tech_list_order_all') }}"><img src="{{ asset('assets/images/logos/azteca.jpg') }}" class="img-index"></a>

                            <!-- <a href="{{ route('tech_list_order') }}"></a><br>
                            <a href="{{ route('tech_list_order') }}"><img src="{{ asset('assets/images/logos/azteca.jpg') }}" class="img-index"></a> -->
                        </div>
                    </div>
                </div>
                @elseif(Auth::user()->isAdmin()) 
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div id="init-orca">
                            <a href="{{ route('change_mode', ['orca']) }}"></a><br>
                            <a href="{{ route('change_mode', ['orca']) }}"><img src="{{ asset('assets/images/logos/orca.png') }}" class="img-index"></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div id="init-client">
                            <a href="{{ route('change_mode', ['azteca']) }}"></a><br>
                            <a href="{{ route('change_mode', ['azteca']) }}"><img src="{{ asset('assets/images/logos/azteca.jpg') }}" class="img-index"></a>
                        </div>
                    </div>
                </div>
                @endif
        
            @else
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>              

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active">
                    <img src="{{ $imh }}" alt="Vidrios Azteca">
                    <div class="carousel-caption">
                      <a href="{{ route('cotizacion') }}" class="btn btn-success btn-lg">Cotizar</a>
                    </div>
                  </div>
                  <div class="item">
                    <img src="{{ $imh }}" alt="Vidrios Azteca">
                    <div class="carousel-caption">
                      <a href="{{ route('cotizacion') }}" class="btn btn-success btn-lg">Cotizar</a>
                    </div>
                  </div>
                  <div class="item">
                    <img src="{{ $imh }}" alt="Vidrios Azteca">
                    <div class="carousel-caption">
                      <a href="{{ route('cotizacion') }}" class="btn btn-success btn-lg">Cotizar</a>
                    </div>
                  </div>
                </div>              

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection