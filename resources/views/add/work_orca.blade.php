@extends('layouts.app')

@section('script_head3')
<style>
article, aside, figure, footer, header, hgroup,
menu, nav, section { display: block; }
#x { display:none; position:relative; z-index:200; float:right}
#previewPane { display: inline-block; }
</style>
@stop

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <h1>Crear Obra para Orca</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'add_work_orcaStore', 'method'=>'POST', 'role'=>'form']) !!}
                <br>
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre de la Obra']) !!}
                <br>
                {!! Form::label('residente', 'Residente') !!}
                {!! Form::text('residente',null,['class'=>'form-control', 'placeholder'=>'Residente']) !!}
                <br>
                {!! Form::label('residente_c', 'Contacto Residente') !!}
                {!! Form::text('residente_c',null,['class'=>'form-control', 'placeholder'=>'Contacto Residente']) !!}
                <br>
                {!! Form::label('encargado', 'Encargado') !!}
                {!! Form::text('encargado',null,['class'=>'form-control', 'placeholder'=>'Encargado']) !!}
                <br>


                </br><button type="submit" class="btn btn-primary">Enviar</button>
            </div><!-- /fin de col -->

            <div class="col-md-4">
                {!! Form::label('title', 'Fecha Inicio') !!}
                <br>
                <input type="date" name="start_at" min="2016-01-02" value="{{ date('Y-m-d') }}"/>
                <br>
                <br>
                {!! Form::label('description', 'Descripción') !!}
                {!! Form::textarea('description',null,['class'=>'form-control', 'rows'=>'4', 'placeholder'=>'Descripción']) !!}
                <!-- des --> 
                <br>
                {!! Form::close() !!}
                <div class="margin-artificial"></div>
            </div><!-- /fin de col -->
        </div><!-- /fin de row -->
    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/product.js') }}"></script>
<script src="{{ asset('assets/js/show.js') }}"></script>
<script src="{{ asset('assets/js/price.js') }}"></script>
<script src="{{ asset('assets/js/image.js') }}"></script>
<script src="{{ asset('assets/js/fijo.js') }}"></script>
<script src="{{ asset('assets/js/add.js') }}"></script>
@endsection




