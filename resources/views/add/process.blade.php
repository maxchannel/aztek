@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Crear Proceso</div>
                <div class="panel-body">

                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'add_process_create', 'method'=>'POST', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nombre*</label>
                        <div class="col-md-6">
                           {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre del Producto']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Precio Vidriero</label>
                        <div class="col-md-6">
                           {!! Form::text('price1',null,['class'=>'form-control', 'placeholder'=>'Precio Vidriero']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Precio Público</label>
                        <div class="col-md-6">
                           {!! Form::text('price2',null,['class'=>'form-control', 'placeholder'=>'Precio Público']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Precio Instalación</label>
                        <div class="col-md-6">
                           {!! Form::text('price3',null,['class'=>'form-control', 'placeholder'=>'Precio Instalación']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Unidad</label>
                        <div class="col-md-6">
                           {!! Form::text('unity',null,['class'=>'form-control', 'placeholder'=>'Unidad']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Maquila</label>
                        <div class="col-md-6">
                           {!! Form::checkbox('maquila', 'yes', false) !!} Dejar vacio para que no se envie proceso a operador
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Enviar
                            </button>
                        </div>
                    </div>
                {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
