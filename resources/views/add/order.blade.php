@extends('layouts.app')

@section('script_head3')
<style>
article, aside, figure, footer, header, hgroup,
menu, nav, section { display: block; }
#x { display:none; position:relative; z-index:200; float:right}
#previewPane { display: inline-block; }
</style>
@stop

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-6">
                <h1>Crear Pedido para Orca</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                        <a href="{{ route('list_work') }}">Ver Obras</a>
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'add_orderStore', 'method'=>'POST', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                <br>
                {!! Form::label('work_id', 'Obras Existentes') !!}
                {!! Form::select('work_id',[''=>'Seleccionar']+$works,null,['class'=>'form-control', 'onchange'=>'price()', 'required']) !!}
                <br>
                {!! Form::label('name', 'Area') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Area', 'required']) !!}
                <br>
                <!-- cristal -->
                {!! Form::label('material', 'Material') !!}
                <div id="add" class="btn btn-success btn-xs">Agregar</div><br><br>
                <div class="form-group">  
                    <div class="table-responsive">  
                        <table class="table table-bordered fijo_field table-margin">  
                            <tr>  
                                <td>{!! Form::text('cantidad[0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Cantidad']) !!}</td>
                                <td>{!! Form::text('ancho[0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Ancho']) !!}</td>
                                <td>{!! Form::text('largo[0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Largo']) !!}</td>
                                <td>{!! Form::select('cristal[0]',[''=>'Seleccionar']+$products,null,['class'=>'form-control name_list', 'id'=>'cristal_master' ,'onchange'=>'price()']) !!}</td>
                                <td>{!! Form::select('precio[0]',[
                                    ''=>'Seleccionar',
                                    '4'=>'Orca Suministro',
                                    '5'=>'Orca Instalación'
                                    ],null,['class'=>'form-control name_list', 'id'=>'precio_master' ,'onchange'=>'price()']) !!}</td>
                                <td><div id="addPro0" class="btn btn-info">+</div></td>  
                            </tr>
                        </table>  
                        <table class="table table-bordered table-margin" id="sp0">  
                            <tr>  
                                <td>{!! Form::text('cantidad_p[0][0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Cantidad']) !!}</td>
                                <td>{!! Form::select('process[0][0]',[''=>'Seleccionar']+$processes,null,['class'=>'form-control', 'id'=>'process_master', 'onchange'=>'price()']) !!}</td>
                                <td>{!! Form::select('p_p[0][0]',[
                                    ''=>'Seleccionar',
                                    '4'=>'Orca Suministro',
                                    '5'=>'Orca Instalación'
                                    ],null,['class'=>'form-control name_list', 'id'=>'process_m' ,'onchange'=>'price()']) !!}
                                </td>
                            </tr>  
                        </table>  

                        <div id="dynamic_field">  
                        </div>  
                    </div>  
                </div><br>

                <input type="hidden" id="p_t_count" value=" {{$p_t_count}}">
                @foreach($products_t as $product_t)
                    <input type="hidden" id="product_id_{{$product_t->id}}" value="{{$product_t->price}}">
                @endforeach
                {!! Form::label('status', 'Estado') !!}
                {!! Form::select('status',[''=>'Seleccionar', 'C'=>'Confirmado','P'=>'Presupuesto'],null,['class'=>'form-control', 'required'=>'required']) !!}
                <!-- cristal -->
                </br><button type="submit" class="btn btn-primary">Enviar</button>
            </div><!-- /fin de col -->

            <div class="col-md-4">
                <!-- des -->
                {!! Form::label('title', 'Fecha Inicio') !!}
                <br>
                <input type="date" name="start_at" min="2016-01-02" value="{{ date('Y-m-d') }}"/>
                <br>
                {!! Form::label('title', 'Fecha de Entrega') !!}
                <br>
                <input type="date" name="limited_at" min="2016-01-02" />
                <br>
                {!! Form::checkbox('instalation', 'yes', false) !!}
                {!! Form::label('instalation', ' Instalación') !!}
                {!! Form::checkbox('suministration', 'yes', false) !!}
                {!! Form::label('suministration', ' Suministro') !!}
                <br>
                <br>
                {!! Form::label('description', 'Descripción') !!}
                {!! Form::textarea('description',null,['class'=>'form-control', 'rows'=>'4', 'placeholder'=>'Descripción']) !!}
                <!-- des --> 

                <h4>Total: <div id="price_temp1"></div></h4>
                <p><div id="temp_2"></div></p>
                <p><div id="temp_1"></div></p>

                <input type="hidden" name="price_temp2" id="price_temp2" value=""/> 

                <!-- images -->
                <h3>Imágenes</h3>
                @if(Session::has('image-message'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('image-message') }}
                    </div>
                @endif
                <div class="form-group">
                    <!--
                    <div id="browse" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-picture"></span> Seleccionar imágenes</div>
                    -->
                    {!! Form::file('file[]',['multiple' => 'multiple', 'id' => 'multiple-files', 'accept' => 'image/*']) !!}
                </div>

                <!--
                <input type='file' onchange="readURL(this);" name="file[]" accept="image/*" /><br/>
                <span id="previewPane">
                    <img id="img_prev" src="#" alt="your image" />
                    <span id="x">[X]</span>
                </span>
                -->
                <!-- images -->
                
                <!-- figuras -->
                <h3>Figuras</h3>
                <div id="addFigure" class="btn btn-info btn-xs">Agregar Figura</div><br><br>
                <div class="row">
                    <div class="col-xs-5">
                        {!! Form::checkbox('figuras_check_a', 'value', false) !!}
                    </div>
                    <div class="col-xs-7">
                        <img src="{{ $fig_first }}" class="img-in-form" />
                    </div>
                </div><br>
                @include('partials.form.figuras')
                <!-- figuras -->


                <br>
                {!! Form::close() !!}
                <div class="margin-artificial"></div>
            </div><!-- /fin de col -->
        </div><!-- /fin de row -->
    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/product.js') }}"></script>
<script src="{{ asset('assets/js/show.js') }}"></script>
<script src="{{ asset('assets/js/price.js') }}"></script>
<script src="{{ asset('assets/js/image.js') }}"></script>
<script src="{{ asset('assets/js/fijo.js') }}"></script>
<script src="{{ asset('assets/js/add.js') }}"></script>
@endsection




