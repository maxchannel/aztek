@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Cotizar en Vidrios Azteca</div>
                <div class="panel-body">

                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>'cotizacion_c', 'method'=>'POST', 'role'=>'form']) !!}
                {!! Form::label('name', 'Nombre*') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Nombre Completo']) !!}
                {!! Form::label('c', 'Contacto*') !!}
                {!! Form::text('c',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Email y/o Teléfono']) !!}
                {!! Form::label('m', 'Mensaje*') !!}
                {!! Form::textarea('m',null,['class'=>'form-control', 'maxlength'=>'100', 'placeholder'=>'Detalles del Trabajo']) !!}
                <br>

                <button type="submit" class="btn btn-success">Enviar</button>
                {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-3"></div>
    </div>
</div>
@endsection
