@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Crear Incidente</div>
                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                            <a href="{{ route('list_incident') }}">Ver Incidentes</a>
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::open(['route'=>'add_incidentStore', 'method'=>'POST', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Descripción*</label>
                            <div class="col-md-6">
                                {!! Form::textarea('content',null,['class'=>'form-control', 'rows'=>'4']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Pedido donde ocurrio</label>
                            <div class="col-md-6">
                                {!! Form::select('order_id',array_merge(['' => 'Seleccionar Pedido'], $orders),null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
