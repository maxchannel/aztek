@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cliente</div>
                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('message') }}
                            <a href="{{ route('list_client') }}">Ver Clientes</a>
                        </div>
                    @endif
                    @include('partials.errorMessages')

                    {!! Form::open(['route'=>'add_clientStore', 'method'=>'POST', 'role'=>'form', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nombre*</label>
                            <div class="col-md-6">
                                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre del Cliente']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Domicilio</label>
                            <div class="col-md-6">
                                {!! Form::text('address',null,['class'=>'form-control', 'placeholder'=>'Domicilio']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Teléfono</label>
                            <div class="col-md-6">
                                {!! Form::text('telephone',null,['class'=>'form-control', 'placeholder'=>'Teléfono del Cliente']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tipo</label>
                            <div class="col-md-6">
                                {!! Form::select('type',
                                    ['' => 'Seleccionar Cliente','Vidriero' => 'Vidriero', 'Público' => 'Público']
                                ,null,['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
