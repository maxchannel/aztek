@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <h1>Agregar Medida Manual a Proceso</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($existencia > 0)
                    La Actual medida Manual es: {{ $medida_manual->can }}

                    <br><br><br>
                    <p>Al eliminar podras agregar una nueva</p>
                    {!! Form::open(['route'=>['destroy_manual_process', $medida_manual->id], 'method'=>'DELETE']) !!}
                    {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                @else
                    {!! Form::open(['route'=>['manual_process_az_store', $id, $order_id], 'method'=>'POST', 'id'=>'formulario', 'role'=>'form']) !!}
                    <br>
                    {!! Form::label('manual', 'Nueva Medida Manual') !!}
                    {!! Form::text('manual',null,['class'=>'form-control', 'placeholder'=>'Medida']) !!}
                    </br><button type="submit" class="btn btn-primary">Enviar</button>
                    {!! Form::close() !!}
                @endif
                
            </div><!-- /fin de col -->
            <div class="col-md-3">
                <br><br>
                <a href="{{ route('add_work_panorama', [$order_id]) }}" class="btn btn-success">Volver a editar Información</a>
            </div>
    </div>
</div>


@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/delete.js') }}"></script>
@endsection
