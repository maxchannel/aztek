@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-8">
                <h1>Imágenes en el Pedido</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($order->images->count() > 0)
                    @foreach($order->images as $i => $image)
                        <div class="col-sm-4">
                            <!-- Modal -->
                            <div class="modal fade" id="myModal{{$image->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog modal-lg">
                                   <div class="modal-content">
                                     <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                                     </div>
                                     <div class="modal-body">
                                        <img src="{{ $temp[$i] }}" class="img-responsive" />
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                     </div>
                                   </div>
                                 </div>
                            </div>
                            <!-- Modal -->
                            <a href="" data-toggle="modal" data-target="#myModal{{$image->id}}"><img src="{{ $temp[$i] }}" class="order-image-min" /></a>


                            {!! Form::open(['route'=>['delete_order_image_az', $image->id], 'method'=>'DELETE']) !!}
                            {!! Form::submit('Eliminar', ['class' => 'delete-button btn btn-danger btn-sm btn-block', 'style' => 'margin: 10px 0']) !!}
                            {!! Form::close() !!}
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene imágenes.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h2>Información</h2>
                <a href="{{ route('add_work_panorama', [$order->id]) }}" class="btn btn-success">Volver a editar Información</a>

                <!-- images -->
                {!! Form::open(['route'=>['list_order_images_store_az', $order->id], 'method'=>'PUT', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                <h3>Subir más Imágenes</h3>
                <p class="text-muted">Puedes subir multiples imágenes al Pedido</p>
                @if(Session::has('image-message'))
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('image-message') }}
                    </div>
                @endif
                
                {!! Form::file('file[]',['multiple' => 'multiple', 'id' => 'multiple-files', 'accept' => 'image/*']) !!}
                <br>
                <button type="submit" class="btn btn-primary">Enviar</button>

                {!! Form::close() !!}
                <!-- images -->
            </div>
    </div>
</div>
@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/img-upl.js') }}"></script>
<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $(".delete-button").click(function(){
        if (!confirm("Confirmas eliminar la imágen?")){
          return false;
        }
    });
</script>
@endsection
