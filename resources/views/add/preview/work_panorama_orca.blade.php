@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-2">
            </br><a href="{{ route('edit_work', [$work->id]) }}" class="btn btn-warning">Editar Obra</a>
            </br></br><a href="{{ route('list_work') }}" class="btn btn-success">Volver a Obras</a>
            </div>
            <div class="col-md-8">
                <h3>Obra: {{ $work->name }}</h3>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                <h4>Pedidos</h4>
                @if(\Request::input('m') == 't')
                    <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id, 'sort'=>\Request::input('sort')]) }}">Por Terminar</a>
                @else
                    <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id,'m'=>'t', 'sort'=>\Request::input('sort')]) }}">Terminadas</a>
                @endif

                @if($work->orders->count() > 0)
                    <p class="pull-right">
                        Ordenar(
                            @if(\Request::input('sort') == 'area' || \Request::input('sort') == 'area_a')
                            Area
                            @else
                            <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id,'sort'=>'area', 'm'=>\Request::input('m')]) }}">Area</a>
                            @endif
                            /
                            @if(\Request::input('sort') == 'fecha' || \Request::input('sort') == 'fecha_a')
                            Fecha
                            @else
                            <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id,'sort'=>'fecha', 'm'=>\Request::input('m')]) }}">Fecha</a>
                            @endif
                            )        

                            @if(\Request::input('sort') == 'area')
                            <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id, 'sort'=>'area_a', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                            @elseif(\Request::input('sort') == 'area_a')
                            <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id, 'sort'=>'area', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                            @endif        

                            @if(\Request::input('sort') == 'fecha')
                            <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id, 'sort'=>'fecha_a', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                            @elseif(\Request::input('sort') == 'fecha_a')
                            <a href="{{ route('add_work_panorama_orca', ['id'=>$work->id, 'sort'=>'fecha', 'm'=>\Request::input('m')]) }}"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>
                            @endif
                    </p>
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Area</th>
                            <th>Status</th>
                            <th>Avance</th>
                            <th>Creado</th>
                            <th></th>
                        </tr>
                            @foreach($results as $h => $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->name }}</td>
                                    <td>
                                        @if($order->status == "C")
                                            Confirmado
                                        @elseif($order->status == "S")
                                            StandBy
                                            @if($order->m != "")
                                            : {{$order->m}}
                                            @endif
                                        @elseif($order->status == "P")
                                            Presupuesto
                                        @elseif($order->status == "T")
                                            Terminada
                                        @endif
                                    </td>
                                    <td>{{ $avan[$h] }} %</td>
                                    <td>
                                        <script>
                                        moment.locale("es");
                                        document.writeln(moment.utc("{{ $order->created_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                                        </script>
                                    </td>
                                    <td>
                                        <a href="{{ route('add_work_panorama_orders', [$order->id]) }}" class="btn btn-success btn-xs">Panorama</a> 
                                        <a href="{{ route('tech_take_orca', [$order->id]) }}" class="btn btn-danger btn-xs">Avance</a> 
                                        <a href="{{ route('pdf_orca', [$order->id]) }}" class="btn btn-info btn-xs" target="_blank">Ver PDF</a>
                                        <a href="{{ route('add_work_orca_edit', [$order->id]) }}" class="btn btn-warning btn-xs">Editar Pedido</a> 
                                    </td>
                                </tr>
                            @endforeach
                    </table>
                    {!! str_replace('/?', '?', $results->appends(Request::only('sort'))->render()) !!} 
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Cliente aún no tiene pedidos.
                    </div>
                @endif

            </div><!-- /fin de col -->

            <div class="col-md-2">
            </div><!-- /fin de col -->

        </div><!-- /fin de row -->
        <br><br>

    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
@endsection