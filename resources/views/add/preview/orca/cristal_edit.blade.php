@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8"> 
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::open(['route'=>['list_order_products_store_orca', $order->id], 'method'=>'POST', 'role'=>'form']) !!}
                <br>
                <!-- cristal -->
                {!! Form::label('material', 'Material') !!}
                <div id="add" class="btn btn-success btn-xs">Agregar</div><br><br>
                <div class="form-group">  
                    <div class="table-responsive">  
                        <table class="table table-bordered fijo_field table-margin">  
                            <tr>  
                                <td>{!! Form::text('cantidad[0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Cantidad']) !!}</td>
                                <td>{!! Form::text('ancho[0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Ancho']) !!}</td>
                                <td>{!! Form::text('largo[0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Largo']) !!}</td>
                                <td>{!! Form::select('cristal[0]',[''=>'Seleccionar']+$products_z,null,['class'=>'form-control name_list', 'id'=>'cristal_master' ,'onchange'=>'price()']) !!}</td>
                                <td>{!! Form::select('precio[0]',[
                                    ''=>'Seleccionar',
                                    '4'=>'Orca Suministro',
                                    '5'=>'Orca Instalación'
                                    ],null,['class'=>'form-control name_list', 'id'=>'precio_master' ,'onchange'=>'price()']) !!}</td>
                                <td><div id="addPro0" class="btn btn-info">+</div></td>  
                            </tr>
                        </table>  
                        <table class="table table-bordered table-margin" id="sp0">  
                            <tr>  
                                <td>{!! Form::text('cantidad_p[0][0]',null,['class'=>'form-control','onchange'=>'price()', 'id'=>'', 'placeholder'=>'Cantidad']) !!}</td>
                                <td>{!! Form::select('process[0][0]',[''=>'Seleccionar']+$processes,null,['class'=>'form-control', 'id'=>'process_master', 'onchange'=>'price()']) !!}</td>
                                <td>{!! Form::select('p_p[0][0]',[
                                    ''=>'Seleccionar',
                                    '4'=>'Orca Suministro',
                                    '5'=>'Orca Instalación'
                                    ],null,['class'=>'form-control name_list', 'id'=>'process_m' ,'onchange'=>'price()']) !!}
                                </td>
                            </tr>  
                        </table>  

                        <div id="dynamic_field">  
                        </div>  
                    </div>  
                </div>

                <input type="hidden" id="p_t_count" value=" {{$p_t_count}}">
                @foreach($products_t as $product_t)
                    <input type="hidden" id="product_id_{{$product_t->id}}" value="{{$product_t->price}}">
                @endforeach

                <button type="submit" class="btn btn-primary">Enviar</button>
                {!! Form::close() !!}
                </br>
                </br>
                <!-- cristal -->


                @if($order->products->count() > 0)
                    <table class="table table-hover">
                        <tr>
                            <th>Piezas</th>
                            <th>Cristal</th>
                            <th>Ancho</th>
                            <th>Largo</th>
                            <th></th>
                        </tr>
                        @if($conteo > 0)
                            @foreach($products as $p_s)
                                <tr data-id="{{ $p_s->id }}">
                                    <td>{{ $p_s->cantidad }}</td>
                                    <td>
                                        {{ $p_s->product->name }}
                                        @if(count($p_s->processes) > 0)
                                            @foreach($p_s->processes as $process)
                                                <br>-{{ $process->name }} (x{{$process->pivot->cantidad}})
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{ $p_s->ancho }}</td>
                                    <td>{{ $p_s->largo }}</td> 
                                    <td><a href="#" class="btn-delete">Eliminar</a></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Cliente aún no tiene pedidos.
                    </div>
                @endif
            </div>
            <div class="col-md-2">
                <br><br>
                <a href="{{ route('add_work_panorama_orders', [$order->id]) }}" class="btn btn-success">Volver a editar Información</a>
            </div>
    </div>
</div>

{!! Form::open(['route'=>['delete_order_product_orca', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}

@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/product.js') }}"></script>
<script src="{{ asset('assets/js/show.js') }}"></script>
<script src="{{ asset('assets/js/price.js') }}"></script>
<script src="{{ asset('assets/js/image.js') }}"></script>
<script src="{{ asset('assets/js/fijo.js') }}"></script>
<script src="{{ asset('assets/js/add.js') }}"></script>
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection
