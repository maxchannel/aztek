@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
@endsection

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-2">
            </br><a href="{{ route('add_work_orca_edit', [$order->id]) }}" class="btn btn-primary">Editar</a>
            </div>
            <div class="col-md-8">
                <h3>#Folio ({{ $order->id }}) - Cliente: Orca - Obra: {{ $order->work->name }} - 
                    @if($order->status == "C")
                        Confirmado
                    @elseif($order->status == "S")
                        StandBy
                        @if($order->m != "")
                        : {{$order->m}}
                        @endif
                    @elseif($order->status == "P")
                        Presupuesto
                    @elseif($order->status == "T")
                        Terminada
                    @endif
                </h3>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                @if($order->products->count() > 0)
                    <table class="table table-hover">
                        <tr>
                            <th>Piezas</th>
                            <th>Cristal</th>
                            <th>Ancho</th>
                            <th>Largo</th>
                            <th>Precio Unitario</th>
                        </tr>
                        @if($conteo > 0)
                            <?php $fg = 0; ?>
                            @foreach($products as $key => $p_s)
                                <tr>
                                    <td>{{ $p_s->cantidad }}</td>
                                    <td>
                                        {{ $p_s->product->name }} ({{ $x[$key][$p_s->p] }}$)
                                        @if(count($p_s->processes) > 0)
                                            @foreach($p_s->processes as $process)
                                                <br>-{{ $process->name }} (x{{$process->pivot->cantidad}}) ({{ $process->unity }}) ({{ $stereo[$fg] }}$) ({{ $torr[$fg] }}$)
                                                <?php $fg++; ?>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{ $p_s->ancho }}</td>
                                    <td>{{ $p_s->largo }}</td> 
                                    <td>
                                        @if($order->price == 0)
                                            $ {{ $importe[$key] }}
                                        @else
                                            $ {{ $x[$key][$p_s->p] }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Cliente aún no tiene cristales añadidos.
                    </div>
                @endif

                <h5>Subtotal: ${{ $sub }}</h5>
                <h5>IVA: ${{ $iva }}</h5>
                <h5>Total: ${{ $precio }}</h5>

            </div><!-- /fin de col -->

            <div class="col-md-2">
                <p>
                    Inicio: 
                    <script>
                    moment.locale("es");
                    document.writeln(moment.utc("{{ $order->start_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                    </script>
                    ({{ $order->start_at }})
                </p>
                <p>
                    Entrega:
                    <script>
                    moment.locale("es");
                    document.writeln(moment.utc("{{ $order->limited_at }}", "YYYYMMDD hh:mm:ss").fromNow());
                    </script>
                    ({{ $order->limited_at }})
                </p>

            </br></br><a href="{{ route('pdf_orca', [$order->id]) }}" class="btn btn-warning" target="_blank"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Exportar a PDF</a></br>
            </br><a href="{{ route('add_work_panorama_orca', [$order->work->id]) }}" class="btn btn-success">Volver a Obra</a>
            </div><!-- /fin de col -->

        </div><!-- /fin de row -->

        <br><br>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                <h4>Procesos (<a href="{{ route('list_order_products_orca', [$order->id]) }}">Editar</a>)</h4>
                @if($conteo > 0)
                    @foreach($products as $key => $product)
                        <h6>{{ $cristales[$key] }}</h6>
                        @foreach($product->processes as $process)
                            -{{ $process->name }} (x{{$process->pivot->cantidad}})<br>
                        @endforeach
                    @endforeach
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene procesos.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h4>Imágenes (<a href="{{ route('list_order_images_orca', [$order->id]) }}">Editar</a>)</h4>
                @if($order->images->count() > 0)
                    <!-- Modal -->
                    <div class="modal fade" id="myModalImages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                         <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                             <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                               <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                             </div>
                             <div class="modal-body">
                                <img src="{{ $imh }}" class="img-responsive" />
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                             </div>
                           </div>
                         </div>
                    </div>
                    <!-- Modal -->
                    <a href="" data-toggle="modal" data-target="#myModalImages">
                        <img src="{{ $imh }}" class="img-responsive" />
                    </a>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene imágenes.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h4>Figuras (<a href="{{ route('list_order_figures_orca', [$order->id]) }}">Editar</a>)</h4>
                @if($order->figuresVigent->count() > 0)
                    <!-- Modal -->
                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                         <div class="modal-dialog modal-lg">
                           <div class="modal-content">
                             <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                               <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                             </div>
                             <div class="modal-body">
                                <img src="{{ $fig_first }}" class="img-responsive" />
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                             </div>
                           </div>
                         </div>
                    </div>
                    <!-- Modal -->
                    <a href="" data-toggle="modal" data-target="#myModal2">
                        <img src="{{ $fig_first }}" class="img-responsive" />
                    </a>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene Figuras.
                    </div>
                @endif
            </div>
            <div class="col-md-2">
                <h4>Operadores</h4>
                @if($order->techs->count() > 0)
                    @foreach($order->techs as $tech)
                        {{ $tech->name }}
                    @endforeach
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene operadores.
                    </div>
                @endif
            </div>
            <div class="col-md-1">
            </div>
        </div>

    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/product.js') }}"></script>
<script src="{{ asset('assets/js/price.js') }}"></script>
@endsection