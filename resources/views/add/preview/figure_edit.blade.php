@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-8">
                <h1>Figuras de Pedido</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif

                @if($order->figuresVigent->count() > 0)
                    <table class="table table-hover">
                        <tr>
                            <th>Figura</th>
                            <th>Imágen</th>
                            <th></th>
                        </tr>
                        @foreach($order->figuresVigent as $i => $figure)
                            <tr data-id="{{ $figure->pivot->id }}">
                                <td>{{ $figure->name }}</td>
                                <td>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal{{$figure->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                         <div class="modal-dialog modal-lg">
                                           <div class="modal-content">
                                             <div class="modal-header">
                                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                               <h4 class="modal-title" id="myModalLabel">Imágen</h4>
                                             </div>
                                             <div class="modal-body">
                                                <img src="{{ $temp[$i] }}" class="img-responsive" />
                                             </div>
                                             <div class="modal-footer">
                                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                             </div>
                                           </div>
                                         </div>
                                    </div>
                                    <!-- Modal -->
                                    <a href="" data-toggle="modal" data-target="#myModal{{$figure->id}}"><img src="{{ $temp[$i] }}" class="img-figure"  /></a>
                                </td>
                                <td>
                                    <a href="#" class="btn-delete">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    <div class="alert alert-warning alert-dismissable">
                        Este Pedido aún no tiene Figuras.
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <h2>Información</h2>
                <a href="{{ route('add_work_panorama', [$order->id]) }}" class="btn btn-success">Volver a editar Información</a>
                
                

                {!! Form::open(['route'=>['list_order_figures_store_az', $order->id], 'method'=>'PUT', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                <h3>Subir más Figuras</h3>
                @include('partials.errorMessages')
                <p class="text-muted">Puedes subir multiples figuras al Pedido</p>
                <button type="submit" class="btn btn-primary">Enviar</button><br><br>
                <div id="addFigure" class="btn btn-info btn-xs">Agregar Figura</div><br><br>
                <div class="row" id="ca1">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[0]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras1']) !!}
                    </div>
                </div><br>
                <div class="row" id="ca2">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[1]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras2']) !!}
                    </div>
                </div><br>
                <div class="row" id="ca3">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[2]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras3']) !!}
                    </div>
                </div><br>

                <div class="row" id="ca4">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[2]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras4']) !!}
                    </div>
                </div><br>
                <div class="row" id="ca5">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[2]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras5']) !!}
                    </div>
                </div><br>
                <div class="row" id="ca6">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[2]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras6']) !!}
                    </div>
                </div><br>
                <div class="row" id="ca7">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[2]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras7']) !!}
                    </div>
                </div><br>
                <div class="row" id="ca8">
                    <div class="col-xs-12">
                        {!! Form::select('figuras[2]',[''=>'Seleccionar']+$figures,null,['class'=>'form-control', 'id'=>'figuras8']) !!}
                    </div>
                </div><br>
                {!! Form::close() !!}
            </div>
    </div>
</div>

{!! Form::open(['route'=>['destroy_client_figure', ':USER_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
{!! Form::close() !!}

@endsection

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/product.js') }}"></script>
<script>
$(document).ready(function(){
    //Eliminar usuario
    $('.btn-delete').click(function(e){
        e.preventDefault();

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID',id);
        var data = form.serialize();

        row.fadeOut();

        $.post(url, data, function(result){
            alert(result.message);
        });
    });

});
</script>
@endsection
