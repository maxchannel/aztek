@extends('layouts.app')

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-2">
            </br><a href="{{ route('add_work', array_merge(Request::all())) }}" class="btn btn-primary">Editar</a>
            </div>
            <div class="col-md-8">
                <h1>Cotización para {{ $client->name }}</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                        <a href="{{ route('list_work') }}">Ver Obras</a>
                    </div>
                @endif
                @include('partials.errorMessages')

                <table class="table table-striped">
                    <tr>
                        <th>Piezas</th>
                        <th>Descripción</th>
                        <th>Ancho</th>
                        <th>Largo</th>
                        <th>Importe (MXN)</th>
                    </tr>
                    <tr>
                        <td>{{\Request::input('piezas1')}}</td>
                        <td>{{ $product->name }} (P. Unitario: {{ $product->price }}$)</td>
                        <td>{{ $ancho }}</td> 
                        <td>{{ $largo }}</td> 
                        <td>{{ $precio }} $</td> 
                    </tr>

                </table>
                <h5>Subtotal: {{ $precio }} $</h5>
                <h5>IVA (16%): {{ $iva }} $</h5>
                <h4>Total: {{ $precio_total }} $</h4>
                
            </div><!-- /fin de col -->

            <div class="col-md-2">
                
            </div><!-- /fin de col -->

        </div><!-- /fin de row -->
    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/product.js') }}"></script>
@endsection




