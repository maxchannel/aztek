@extends('layouts.app')

@section('content')
    <div class="container" id="main">
        <div class="row">
            <div class="col-md-2">
                <br><br>
                <a href="{{ route('list_order') }}" class="btn btn-info">Volver a Pedidos a Cliente</a>
            </div>
            <div class="col-md-8">
                <h1>Editar Pedido para Cliente</h1>
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('message') }}
                    </div>
                @endif
                @include('partials.errorMessages')

                {!! Form::model($pedido,['route'=>['add_work_edit_store',$pedido->id],'id'=>'formulario', 'role'=>'form', 'enctype' => 'multipart/form-data']) !!}
                <br>
                {!! Form::label('client_id', 'Cliente') !!}
                {!! Form::select('client_id',$clients,null,['class'=>'form-control']) !!}
                <br>
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Nombre de la Obra']) !!}
                <br>
                {!! Form::label('price', 'Precio Manual (0 para no tomar en cuenta)') !!}
                {!! Form::text('price',null,['class'=>'form-control', 'placeholder'=>'Precio']) !!}
                <br>
                @if($pedido->i == 1)
                    {!! Form::checkbox('i','yes',true) !!} IVA
                @else
                    {!! Form::checkbox('i','yes',false) !!} IVA
                @endif
                <br>
                {!! Form::label('pay', 'Avance de Cobranza') !!}
                {!! Form::text('pay',null,['class'=>'form-control', 'placeholder'=>'Precio']) !!}
                <br>
                {!! Form::label('description', 'Descripción') !!}
                {!! Form::textarea('description',null,['class'=>'form-control', 'placeholder'=>'Descripción']) !!}
                <br>
                {!! Form::label('status', 'Status') !!}
                {!! Form::select('status',
                                    ['' => 'Seleccionar','P' => 'Presupuesto', 'C' => 'Confirmado', 'S' => 'StandBy','T' => 'Terminado']
                                ,null,['class'=>'form-control']) !!}
                <br>
                {!! Form::label('m', 'Mensaje de StandBy (Vacio para no tomar en cuenta)') !!}
                {!! Form::textarea('m',null,['class'=>'form-control', 'placeholder'=>'Mensaje de StandBy']) !!}
                
                </br><button type="submit" class="btn btn-primary">Enviar</button>
                
            </div><!-- /fin de col -->

            <div class="col-md-2">
                <br><br>
                <a href="{{ route('add_work_panorama', [$pedido->id]) }}" class="btn btn-success">Volver a Panorama</a>
                <br><br>
                {!! Form::label('title', 'Fecha Inicio') !!}
                <br>
                <input type="date" name="start_at" min="2016-01-02" value="{{ $pedido->start_at }}"/>
                <br>
                {!! Form::label('title', 'Fecha de Entrega') !!}
                <br>
                <input type="date" name="limited_at" min="2016-01-02" value="{{ $pedido->limited_at }}"/>
                <br>

                {!! Form::close() !!}
            </div><!-- /fin de col -->


        </div><!-- /fin de row -->
    </div> <!-- /container -->
@stop

@section('script_footer')
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="{{ asset('assets/js/product.js') }}"></script>
<script src="{{ asset('assets/js/show.js') }}"></script>
<script src="{{ asset('assets/js/price.js') }}"></script>
<script src="{{ asset('assets/js/image.js') }}"></script>
@endsection




