@extends('layouts.app')

@section('script_head')
<script src="{{ asset('assets/js/chart.js') }}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">Obras para Cliente <a href="{{ route('list_order') }}">(Ver)</a></div>
                <div class="panel-body">
                    <canvas id="myChart"></canvas>
                    <script>
                    var ctx = document.getElementById("myChart");
                    var o_t = <?php echo $o_t; ?>;
                    var o_p = <?php echo $o_p; ?>;
                    var o_s = <?php echo $o_s; ?>;
                    var o_c = <?php echo $o_c; ?>;

                    var myChart = new Chart(ctx, {
                        type: 'polarArea',
                        data: {
                            datasets: [{
                                data: [
                                    o_t,
                                    o_p,
                                    o_s,
                                    o_c,
                                ],
                                backgroundColor: [
                                    "#FF6384",
                                    "#FFCE56",
                                    "#36A2EB",
                                    "#4CAF50",
                                ],
                                label: 'Obras por mes' // for legend
                            }],
                            labels: [
                                "Terminada",
                                "Presupuesto",
                                "StandBy",
                                "Confirmada",
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                    </script>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Obras para Orca <a href="{{ route('list_work') }}">(Ver)</a></div>
                <div class="panel-body">
                    <canvas id="myChartOrca"></canvas>
                    <script>
                    var ctx = document.getElementById("myChartOrca");
                    var oc_t = <?php echo $oc_t; ?>;
                    var oc_p = <?php echo $oc_p; ?>;
                    var oc_s = <?php echo $oc_s; ?>;
                    var oc_c = <?php echo $oc_c; ?>;

                    var myChart = new Chart(ctx, {
                        type: 'polarArea',
                        data: {
                            datasets: [{
                                data: [
                                    oc_t,
                                    oc_p,
                                    oc_s,
                                    oc_c,
                                ],
                                backgroundColor: [
                                    "#FF6384",
                                    "#FFCE56",
                                    "#36A2EB",
                                    "#4CAF50",
                                ],
                                label: 'Obras por mes' // for legend
                            }],
                            labels: [
                                "Terminada",
                                "Presupuesto",
                                "StandBy",
                                "Confirmada",
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                    </script>
                </div>
            </div>

            <!--
            <div class="panel panel-default">
                <div class="panel-heading">Ganancias vs Gastos(Por Mes)</div>
                <div class="panel-body">
                    <canvas id="myChart5"></canvas>
                    <script>
                    var ctx = document.getElementById("myChart5");
                    var data = {
                        labels: ["Material", "Nómina", "Impuestos", "Servicios", "Pagos", "Contabilidad", "Obras Completas"],
                        datasets: [
                            {
                                label: "Egresos",
                                backgroundColor: "rgba(179,181,198,0.2)",
                                borderColor: "rgba(179,181,198,1)",
                                pointBackgroundColor: "rgba(179,181,198,1)",
                                pointBorderColor: "#fff",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(179,181,198,1)",
                                data: [65, 59, 90, 81, 56, 55, 40]
                            },
                            {
                                label: "Ingresos",
                                backgroundColor: "rgba(255,99,132,0.2)",
                                borderColor: "rgba(255,99,132,1)",
                                pointBackgroundColor: "rgba(255,99,132,1)",
                                pointBorderColor: "#fff",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "rgba(255,99,132,1)",
                                data: [28, 48, 40, 19, 96, 27, 100]
                            }
                        ]
                    };
                    var myLineChart = new Chart(ctx, {
                        type: 'radar',
                        data: data,
                        options: {
                                scale: {
                                    reverse: true,
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }
                        }
                    });
                    </script>
                </div>
            </div>
        -->

        </div>


        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Procesos Terminados (Clientes)</div>
                <div class="panel-body">
                    <canvas id="myChart3"></canvas>
                    <script>
                    var ctx = document.getElementById("myChart3");
                    var pro_sep = <?php echo $pro_sep; ?>;
                    var pro_oct = <?php echo $pro_oct; ?>;
                    var pro_nov = <?php echo $pro_nov; ?>;
                    var pro_dic = <?php echo $pro_dic; ?>;
                    var pro_ene = <?php echo $pro_ene; ?>;
                    var pro_feb = <?php echo $pro_feb; ?>;
                    var pro_mar = <?php echo $pro_mar; ?>;
                    var pro_abr = <?php echo $pro_abr; ?>;
                    var pro_may = <?php echo $pro_may; ?>;
                    var pro_jun = <?php echo $pro_jun; ?>;
                    var pro_jul = <?php echo $pro_jul; ?>;
                    var pro_ago = <?php echo $pro_ago; ?>;
                    var data = {
                        labels: ["Septiembre", "Octubre", "Noviembre", "Diciembre", "Enero", "Febrero", "Marzo","Abril","Mayo","Junio","Julio","Agosto"],
                        datasets: [
                            {
                                label: "Procesos",
                                backgroundColor: "rgba(255,99,132,0.2)",
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 1,
                                hoverBackgroundColor: "rgba(255,99,132,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [pro_sep, pro_oct, pro_nov, pro_dic, pro_ene, pro_feb, pro_mar, pro_abr, pro_may, pro_jun, pro_jul, pro_ago],
                            }
                        ]
                    };
                    var myBarChart = new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: {
                            scales: {
                                    xAxes: [{
                                            stacked: true
                                    }],
                                    yAxes: [{
                                            stacked: true
                                    }]
                                }
                            }        

                    });
                    </script>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Procesos Terminados (Orca)</div>
                <div class="panel-body">
                    <canvas id="myChart4"></canvas>
                    <script>
                    var ctx = document.getElementById("myChart4");
                    var pro2_sep = <?php echo $pro2_sep; ?>;
                    var pro2_oct = <?php echo $pro2_oct; ?>;
                    var pro2_nov = <?php echo $pro2_nov; ?>;
                    var pro2_dic = <?php echo $pro2_dic; ?>;
                    var pro2_ene = <?php echo $pro2_ene; ?>;
                    var pro2_feb = <?php echo $pro2_feb; ?>;
                    var pro2_mar = <?php echo $pro2_mar; ?>;
                    var pro2_abr = <?php echo $pro2_abr; ?>;
                    var pro2_may = <?php echo $pro2_may; ?>;
                    var pro2_jun = <?php echo $pro2_jun; ?>;
                    var pro2_jul = <?php echo $pro2_jul; ?>;
                    var pro2_ago = <?php echo $pro2_ago; ?>;
                    var data = {
                        labels: ["Septiembre", "Octubre", "Noviembre", "Diciembre", "Enero", "Febrero", "Marzo","Abril","Mayo","Junio","Julio","Agosto"],
                        datasets: [
                            {
                                label: "Procesos",
                                backgroundColor: "rgba(255,99,132,0.2)",
                                borderColor: "rgba(255,99,132,1)",
                                borderWidth: 1,
                                hoverBackgroundColor: "rgba(255,99,132,0.4)",
                                hoverBorderColor: "rgba(255,99,132,1)",
                                data: [pro2_sep, pro2_oct, pro2_nov, pro2_dic, pro2_ene, pro2_feb, pro2_mar, pro2_abr, pro2_may, pro2_jun, pro2_jul, pro2_ago],
                            }
                        ]
                    };
                    var myBarChart = new Chart(ctx, {
                        type: 'bar',
                        data: data,
                        options: {
                            scales: {
                                    xAxes: [{
                                            stacked: true
                                    }],
                                    yAxes: [{
                                            stacked: true
                                    }]
                                }
                            }        

                    });
                    </script>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Clientes Agregados por Mes</div>
                <div class="panel-body">
                    <canvas id="myChart5"></canvas>
                    <script>
                    var ctx = document.getElementById("myChart5");
                    var cli_sep = <?php echo $cli_sep; ?>;
                    var cli_oct = <?php echo $cli_oct; ?>;
                    var cli_nov = <?php echo $cli_nov; ?>;
                    var cli_dic = <?php echo $cli_dic; ?>;
                    var cli_ene = <?php echo $cli_ene; ?>;
                    var cli_feb = <?php echo $cli_feb; ?>;
                    var cli_mar = <?php echo $cli_mar; ?>;
                    var cli_abr = <?php echo $cli_abr; ?>;
                    var cli_may = <?php echo $cli_may; ?>;
                    var cli_jun = <?php echo $cli_jun; ?>;
                    var cli_jul = <?php echo $cli_jul; ?>;
                    var cli_ago = <?php echo $cli_ago; ?>;
                    var data = {
                        labels: ["Septiembre", "Octubre", "Noviembre", "Diciembre", "Enero", "Febrero", "Marzo","Abril","Mayo","Junio","Julio","Agosto"],
                        datasets: [
                            {
                                label: "Clientes",
                                fill: false,
                                lineTension: 0.1,
                                backgroundColor: "rgba(75,192,192,0.4)",
                                borderColor: "rgba(75,192,192,1)",
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: "rgba(75,192,192,1)",
                                pointBackgroundColor: "#fff",
                                pointBorderWidth: 1,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                                pointHoverBorderColor: "rgba(220,220,220,1)",
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 10,
                                spanGaps: false,
                                data: [cli_sep, cli_oct, cli_nov, cli_dic, cli_ene, cli_feb, cli_mar, cli_abr, cli_may, cli_jun, cli_jul, cli_ago],
                            }
                        ]
                    };
                    var myBarChart = new Chart(ctx, {
                        type: 'line',
                        data: data,
                        options: {
                            scales: {
                                    xAxes: [{
                                            stacked: true
                                    }],
                                    yAxes: [{
                                            stacked: true
                                    }]
                                }
                            }        

                    });
                    </script>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
