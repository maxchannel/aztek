<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTechesTable extends Migration 
{

	public function up()
	{
		Schema::create('order_teches', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('order_teches');
	}

}
