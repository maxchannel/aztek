<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration 
{

	public function up()
	{
		Schema::create('budgets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('c');
			$table->text('m');
			$table->timestamps();

			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('budgets');
	}

}
