<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessesTable extends Migration 
{
	public function up()
	{
		Schema::create('processes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('price1');
            $table->string('price2');
            $table->string('price3');
            $table->string('price4');
            $table->string('price5');
            $table->boolean('maq')->default(1);
            $table->string('unity');
            
            $table->timestamps();
            $table->softDeletes();
        });
	}

	public function down()
	{
		Schema::drop('processes');
	}

}
