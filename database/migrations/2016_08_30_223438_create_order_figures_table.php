<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFiguresTable extends Migration 
{

	public function up()
	{
		Schema::create('order_figures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id')->unsigned();
            $table->integer('figure_id')->unsigned();
            
            $table->timestamps();

            
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('figure_id')->references('id')->on('figures')->onDelete('cascade');
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('order_figures');
	}

}
