<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientProcessesTable extends Migration 
{

	public function up()
	{
		Schema::create('client_processes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cantidad');
            $table->string('p');
            $table->boolean('confirmed');
            $table->integer('client_product_id')->unsigned();
            $table->integer('process_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('client_product_id')->references('id')->on('client_products')->onDelete('cascade');
            $table->foreign('process_id')->references('id')->on('processes')->onDelete('cascade');
            $table->softDeletes();
        });
	}

	public function down()
	{
		Schema::drop('client_processes');
	}

}
