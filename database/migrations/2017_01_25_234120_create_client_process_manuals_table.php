<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientProcessManualsTable extends Migration 
{

	public function up()
	{
		Schema::create('client_process_manuals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('can');
			$table->integer('client_process_id')->unsigned();

			$table->timestamps();
			$table->foreign('client_process_id')->references('id')->on('client_processes')->onDelete('cascade');
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('client_process_manuals');
	}

}
