<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->string('cantidad');
            $table->string('ancho');
            $table->string('largo');
            $table->string('p');
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('order_product');
    }
}
