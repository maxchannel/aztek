<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientProductsTable extends Migration 
{
	public function up()
    {
        Schema::create('client_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cantidad');
            $table->string('ancho');
            $table->string('largo');
            $table->string('p');
            $table->integer('client_order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->timestamps();

            $table->foreign('client_order_id')->references('id')->on('client_orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('client_products');
    }

}
