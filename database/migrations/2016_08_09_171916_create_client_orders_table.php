<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientOrdersTable extends Migration 
{

	public function up()
    {
        Schema::create('client_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('status');
            $table->string('m');
            $table->string('price');
            $table->string('pay');
            $table->boolean('i');
            $table->string('description');
            $table->boolean('confirmed');
            $table->integer('client_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->date('start_at');
            $table->date('limited_at');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('client_orders');
    }

}
