<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTechesTable extends Migration 
{

	public function up()
	{
		Schema::create('client_teches', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_order_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('client_order_id')->references('id')->on('client_orders')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('client_teches');
	}

}
