<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('avatar');
            $table->string('telephone');
            $table->string('type');
            $table->string('descuento');
            $table->timestamps();
            
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('clients');
    }
}
