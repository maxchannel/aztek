<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientFiguresTable extends Migration 
{

	public function up()
	{
		Schema::create('client_figures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_order_id')->unsigned();
            $table->integer('figure_id')->unsigned();
            $table->timestamps();

            $table->foreign('client_order_id')->references('id')->on('client_orders')->onDelete('cascade');
            $table->foreign('figure_id')->references('id')->on('figures')->onDelete('cascade');
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('client_figures');
	}

}
