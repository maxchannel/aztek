<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientImagesTable extends Migration 
{
	public function up()
    {
        Schema::create('client_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_order_id')->unsigned();
            $table->string('route');
            $table->timestamps();
            
            $table->foreign('client_order_id')->references('id')->on('client_orders')->onDelete('cascade');
            
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('client_images');
    }

}
