<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiguresTable extends Migration 
{

	public function up()
	{
		Schema::create('figures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('route');
			$table->timestamps();
			
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('figures');
	}

}
