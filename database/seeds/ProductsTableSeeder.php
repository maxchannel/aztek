<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Product;
use App\Process;
use App\Figure;
use App\Garbage;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        Figure::create([
            'name'=> $faker->text(8),
            'route'=> 'default.jpg',
        ]);

        Figure::create([
            'name'=> $faker->text(8),
            'route'=> 'default.jpg',
        ]);

        Figure::create([
            'name'=> $faker->text(8),
            'route'=> 'default.jpg',
        ]);

        Figure::create([
            'name'=> $faker->text(8),
            'route'=> 'default.jpg',
        ]);

        Figure::create([
            'name'=> $faker->text(8),
            'route'=> 'default.jpg',
        ]);




        Product::create([
            'name'=> 'Claro 3mm',
            'description'=> $faker->text(25),
            'price1'=> 150,
            'price2'=> 200,
            'price3'=> 370,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Claro 4mm',
            'description'=> $faker->text(25),
            'price1'=> 200,
            'price2'=> 270,
            'price3'=> 470,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Claro 6mm',
            'description'=> $faker->text(25),
            'price1'=> 300,
            'price2'=> 400,
            'price3'=> 605,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Claro 9mm',
            'description'=> $faker->text(25),
            'price1'=> 605,
            'price2'=> 806,
            'price3'=> 1086,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Claro 12mm',
            'description'=> $faker->text(25),
            'price1'=> 1075,
            'price2'=> 1430,
            'price3'=> 1705,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Tintex 6mm',
            'description'=> $faker->text(25),
            'price1'=> 390,
            'price2'=> 515,
            'price3'=> 720,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Tintex 9mm',
            'description'=> $faker->text(25),
            'price1'=> 850,
            'price2'=> 895,
            'price3'=> 1165,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Santinovo 4mm',
            'description'=> $faker->text(25),
            'price1'=> 370,
            'price2'=> 435,
            'price3'=> 650,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Santinovo 6mm',
            'description'=> $faker->text(25),
            'price1'=> 525,
            'price2'=> 630,
            'price3'=> 815,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Santinovo 9mm',
            'description'=> $faker->text(25),
            'price1'=> 800,
            'price2'=> 895,
            'price3'=> 1230,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Filtrasol 6mm',
            'description'=> $faker->text(25),
            'price1'=> 390,
            'price2'=> 460,
            'price3'=> 615,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Filtrasol 9mm',
            'description'=> $faker->text(25),
            'price1'=> 1035,
            'price2'=> 1290,
            'price3'=> 1625,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Bronce 6mm',
            'description'=> $faker->text(25),
            'price1'=> 460,
            'price2'=> 570,
            'price3'=> 770,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Chino Hojas 6mm',
            'description'=> $faker->text(25),
            'price1'=> 310,
            'price2'=> 420,
            'price3'=> 580,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Luna 3mm',
            'description'=> $faker->text(25),
            'price1'=> 280,
            'price2'=> 335,
            'price3'=> 560,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Luna 4mm',
            'description'=> $faker->text(25),
            'price1'=> 340,
            'price2'=> 403,
            'price3'=> 650,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Luna 6mm',
            'description'=> $faker->text(25),
            'price1'=> 505,
            'price2'=> 615,
            'price3'=> 760,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Tintex 6mm',
            'description'=> $faker->text(25),
            'price1'=> 505,
            'price2'=> 615,
            'price3'=> 840,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Tintex 9mm',
            'description'=> $faker->text(25),
            'price1'=> 1060,
            'price2'=> 1230,
            'price3'=> 1510,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Santinovo 6mm',
            'description'=> $faker->text(25),
            'price1'=> 695,
            'price2'=> 750,
            'price3'=> 960,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Product::create([
            'name'=> 'Santinovo 9mm',
            'description'=> $faker->text(25),
            'price1'=> 1050,
            'price2'=> 1100,
            'price3'=> 1340,
            'price4'=> 540,
            'price5'=> 220,
            'unity'=> 'm2',
            'quantity'=> $faker->randomElement($array = array (3,54,6)),
            'min'=> $faker->randomElement($array = array (7,2,4)),
            'max'=> $faker->randomElement($array = array (20,32,11)),
        ]);

        Garbage::create([
            'a'=> 2,
            'l'=> 5,
            'product_id'=> $faker->randomElement($array = array (1,3,5)),
        ]);
        Garbage::create([
            'a'=> 2,
            'l'=> 5,
            'product_id'=> $faker->randomElement($array = array (1,3,5)),
        ]);
        Garbage::create([
            'a'=> 2,
            'l'=> 5,
            'product_id'=> $faker->randomElement($array = array (1,3,5)),
        ]);

        Process::create([
            'name'=> 'Corte de Vidrio',
            'unity'=> 'corte'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 10',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 12',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 14',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 15',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 16',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 18',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 20',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 24',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 25',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 28',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 30',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 35',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 40',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø 50',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);

        Process::create([
            'name'=> 'Barreno Ø Avellanados',
            'price1'=> 15,
            'price2'=> 20,
            'unity'=> 'veces'
        ]);


        //Resaques
        /*
        Process::create([
            'name'=> 'Resaque Clip',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Resaque Bisagra',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Resaque Herraje Superior',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Resaque Herraje Inferior',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        //Resaques

        //Canto
        Process::create([
            'name'=> 'Canto Plano',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Canto Boleado',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Canto Filo Muerto',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        //Canto

        //Bisel
        Process::create([
            'name'=> 'Bisel 1/2 Recto',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Bisel 1 1/2 Recto',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Bisel 2 Recto',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Bisel 2 1/2 Recto',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Bisel 1/2 Forma',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Bisel 1 1/2 Forma',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Bisel 2 Forma',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Bisel 2 1/2 Forma',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        //Bisel

        //Pelicula
        Process::create([
            'name'=> 'Pelicula Blanca',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Pelicula Seguridad',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Pelicula Esmerilada',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        //Pelicula

        //Esmeril
        Process::create([
            'name'=> 'Esmeril Completo',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Esmeril Imitación Bisel',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Esmeril Rayado',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Esmeril Diseño',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Esmeril Grabado Sencillo',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Esmeril Grabado Profundo',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]);
        Process::create([
            'name'=> 'Esmeril Cenefa Para Luna',
            'price1'=> 3,
            'price2'=> 4,
            'price3'=> 5,
        ]); */
        //Esmeril



    }
}
