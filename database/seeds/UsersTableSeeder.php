<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User;
use App\Client;
use App\Garbage;
use App\Incident;
use App\Login;
use App\Order;
use App\OrderProduct;
use App\OrderImage;
use App\OrderProcess;
use App\OrderFigure;
use App\Product;
use App\Work;
use App\Budget;
use App\ClientOrder;
use App\ClientImage;
use App\ClientProduct;
use App\ClientProcess;
use App\ClientProcessManual;
use App\ClientFigure;
use App\UserNotification;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        User::create([
            'name'=> 'Admin',
            'email'=> 'admin@hotmail.com',
            'type'=> 'admin',
            'mode'=> 'azteca',
            'password'=>\Hash::make('secret'),
        ]);

        UserNotification::create([
            'm'=> 'Bienvenido',
            'user_id'=> 1,
            'v'=>0
        ]);

        User::create([
            'name'=> 'Operador',
            'email'=> 'operador@hotmail.com',
            'type'=> 'tech',
            'mode'=> 'azteca',
            'password'=>\Hash::make('secret'),
        ]);

        foreach(range(1, 3) as $index)//Minimo 6 sino truena
        {
            Budget::create([
                'name'=> $faker->name,
                'c'=> $faker->phoneNumber,
                'm'=> $faker->text(30),
            ]);

            $user = User::create([
                'name'=> $faker->name,
                'email'=> $faker->email,
                'type'=> $faker->randomElement(['admin','root','tech']),
                'mode'=> 'azteca',
                'password'=>\Hash::make('secret'),
            ]);

            $client = Client::create([
                'name'=> $faker->name,
                'address'=> $faker->streetAddress,
                'telephone'=> $faker->phoneNumber,
                'type'=> 'Vidriero',
                'descuento'=> $faker->randomElement($array = array (0,25,10)),
            ]);

            // $product = Product::create([
            //     'name'=> $faker->jobTitle(),
            //     'description'=> $faker->text(25),
            //     'price'=> 37,
            //     'unity'=> 'm2',
            //     'quantity'=> $faker->randomElement($array = array (3,54,6)),
            //     'min'=> $faker->randomElement($array = array (7,2,4)),
            //     'max'=> $faker->randomElement($array = array (20,32,11)),
            // ]);

            $work = Work::create([
                'name'=> $faker->text(25),
                'status' => 'hecho',
                'residente'=> 'Juan',
                'residente_c'=> '33447826',
                'encargado'=> 'Martin',
                'description'=> 'Des '.$index,
                'client_id'=> $client->id,
                'user_id'=> $user->id
            ]);

            $order = Order::create([
                'name'=> $faker->text(25),
                'description'=> $faker->text(25),
                'status'=> $faker->randomElement($array = array ('C')),
                'price'=> 0,
                'pay'=> 0,
                'work_id'=> $work->id,
                'user_id'=> $user->id
            ]);

            $o_product = OrderProduct::create([
                'cantidad'=> $faker->randomElement($array = array (1,2)),
                'product_id'=> $faker->randomElement($array = array (1,2,3)),
                'ancho'=> $faker->randomElement($array = array (2,0.4,1)),
                'largo'=> $faker->randomElement($array = array (2,0.8,1)),
                'p'=> 2,
                'order_id'=> $order->id
            ]);

            //Corte
            OrderProcess::create([
                'cantidad'=> 1,
                'confirmed'=> 0,
                'p'=> 1,
                'order_product_id'=> $o_product->id,
                'process_id'=> 1,

            ]);

            OrderProcess::create([
                'cantidad'=> 1,
                'confirmed'=> 0,
                'p'=> 1,
                'order_product_id'=> $o_product->id,
                'process_id'=> 2

            ]);

            OrderProcess::create([
                'cantidad'=> 1,
                'confirmed'=> 0,
                'p'=> 1,
                'order_product_id'=> $o_product->id,
                'process_id'=> 2

            ]);

            OrderImage::create([
                'route'=> 'default.jpg',
                'order_id'=> $order->id
            ]);

            OrderImage::create([
                'route'=> 'default.jpg',
                'order_id'=> $order->id
            ]);

            OrderFigure::create([
                'figure_id'=> 1,
                'order_id'=> $order->id
            ]);

            //Pedido para cliente
            $order_client = ClientOrder::create([
                'name'=> $faker->text(25),
                'description'=> $faker->text(25),
                'start_at'=> '2016-09-04',
                'limited_at'=> '2016-09-29',
                'status'=> $faker->randomElement($array = array ('C')),
                'price'=> 0,
                'pay'=> 0,
                'client_id'=> $client->id,
                'user_id'=> $user->id
            ]);

            $client_p = ClientProduct::create([
                'cantidad'=> 1,
                'product_id'=> $index,
                'ancho'=> $faker->randomElement($array = array (2,0.4,1)),
                'largo'=> $faker->randomElement($array = array (2,0.8,1)),
                'p'=> 1,
                'client_order_id'=> $order_client->id
            ]);

            //Corte
            ClientProcess::create([
                'cantidad'=> 1,
                'confirmed'=> 0,
                'p'=> 1,
                'client_product_id'=> $client_p->id,
                'process_id'=> 1

            ]);

            ClientProcess::create([
                'cantidad'=> 1,
                'confirmed'=> 0,
                'p'=> 1,
                'client_product_id'=> $client_p->id,
                'process_id'=> 3

            ]);

            $proceso_a = ClientProcess::create([
                'cantidad'=> 1,
                'confirmed'=> 0,
                'p'=> 1,
                'client_product_id'=> $client_p->id,
                'process_id'=> 3

            ]);

            

            ClientImage::create([
                'route'=> 'default.jpg',
                'client_order_id'=> $order_client->id
            ]);


            ClientFigure::create([
                'figure_id'=> 1,
                'client_order_id'=> $order_client->id
            ]);

            //Pedido para cliente

            Incident::create([
                'content'=> $faker->text(25),
                'order_id'=> $order->id,
                'user_id'=> $user->id
            ]);

            Login::create([
                'access'=> $index,
                'ip'=> $faker->ipv4(),
                'user_id'=> $user->id
            ]);
        }

    }
}
