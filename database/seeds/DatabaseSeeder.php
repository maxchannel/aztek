<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call('ProductsTableSeeder');
        $this->call('MaquilaTableSeeder');
        $this->call('UsersTableSeeder');
    }
}
