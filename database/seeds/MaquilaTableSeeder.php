<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Product;
use App\Process;
use App\Figure;
use App\Garbage;

class MaquilaTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        Process::create([
            'name'=> ucwords(strtolower('RESAQUE CHICO')),
            'price1'=> 25,
            'price2'=> 35,
            'unity'=> 'veces',
        ]);

        Process::create([
            'name'=> ucwords(strtolower('RESAQUE GRANDE')),
            'price1'=> 25,
            'price2'=> 35,
            'unity'=> 'veces',
        ]);

        Process::create([
            'name'=> ucwords(strtolower('TEMPLADO 6mm')),
            'price1'=> 140,
            'price2'=> 150,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('TEMPLADO 9mm')),
            'price1'=> 150,
            'price2'=> 160,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('TEMPLADO 12mm')),
            'price1'=> 200,
            'price2'=> 250,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CANTO PLANO 6mm')),
            'price1'=> 15,
            'price2'=> 18,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CANTO PLANO 9mm')),
            'price1'=> 20,
            'price2'=> 25,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CANTO PLANO 12mm')),
            'price1'=> 25,
            'price2'=> 30,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CANTO BOLEADO FORMA 6mm')),
            'price1'=> 23,
            'price2'=> 26,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CANTO BOLEADO FORMA 9mm')),
            'price1'=> 33,
            'price2'=> 36,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CANTO BOLEADO FORMA 12mm')),
            'price1'=> 43,
            'price2'=> 46,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('FILOS MUERTO 4, 6mm')),
            'price1'=> 8,
            'price2'=> 8,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('FILOS MUERTO 9, 12 mm')),
            'price1'=> 12,
            'price2'=> 12,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL RECTO 3/4"  6mm')),
            'price1'=> 22,
            'price2'=> 28,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL RECTO 3/4"  9mm')),
            'price1'=> 45,
            'price2'=> 65,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL RECTO 1"  6mm')),
            'price1'=> 49,
            'price2'=> 45,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL RECTO  1 1/2"  6mm')),
            'price1'=> 28,
            'price2'=> 32,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL RECTO  1 1/2"  9mm y 12mm')),
            'price1'=> 64,
            'price2'=> 52,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CHAFLAN 6mm FORMA')),
            'price1'=> 26,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL FORMA 1"  6mm')),
            'price1'=> 33,
            'price2'=> 35,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL FORMA 1"  9mm')),
            'price1'=> 59,
            'price2'=> 65,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL FORMA 1 1/2"  6mm')),
            'price1'=> 39,
            'price2'=> 40,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISEL FORMA 1 1/2" 9mm')),
            'price1'=> 74,
            'price2'=> 80,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PELICULA GRABADO')),
            'price2'=> 350,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PELICULA FRANJAS')),
            'price2'=> 250,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PELICULA PRIVACIDAD')),
            'price1'=> 90,
            'price2'=> 115,
            'price3'=> 220,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PELICULA SEGURIDAD')),
            'price1'=> 125,
            'price2'=> 160,
            'price2'=> 250,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('ACRILICO TRANSP. HUMO')),
            'price2'=> 600,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PLASTICO GRABADO')),
            'price2'=> 350,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('POLICARBONATO')),
            'price2'=> 380,
            'price3'=> 450,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CENEFA PARA LUNA')),
            'price1'=> 18,
            'price2'=> 40,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('IMITACION BISEL')),
            'price1'=> 150,
            'price2'=> 250,
            'unity'=> 'ml',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('RAYADO P/CANCEL BAÑO')),
            'price1'=> 190,
            'price2'=> 350,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('DISEÑO P/CANCEL BAÑO')),
            'price1'=> 280,
            'price2'=> 500,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('GRABADO SENCILLO')),
            'price1'=> 350,
            'price2'=> 630,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('GRABADO SENCILLO PROFUNDO')),
            'price1'=> 500,
            'price2'=> 900,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('GRABADO SOMBREADO')),
            'price1'=> 500,
            'price2'=> 900,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BAJO RELIEVE')),
            'price1'=> 900,
            'price2'=> 1500,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('LAQUEADO')),
            'price1'=> 100,
            'price2'=> 150,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('VITRAL SENCILLO')),
            'price1'=> 2700,
            'price2'=> 3800,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PINTURA')),
            'price1'=> 450,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PINTURA ROJA')),
            'price1'=> 500,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('KIT HERRAJES P/CANCEL BAÑO ABATIBLE')),
            'price1'=> 1200,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('KIT HERRAJES P/CANCEL BAÑO CORREDIZO')),
            'price1'=> 1400,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CLIP')),
            'price1'=> 180,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISAGRA V-M')),
            'price1'=> 400,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BISAGRA V-V')),
            'price1'=> 650,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('BOTON JALADERA')),
            'price1'=> 150,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('PESTAÑA POLICARBONATO')),
            'price1'=> 150,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('CHAPETONES ')),
            'price1'=> 150,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('TELA MOSQUITERA')),
            'price1'=> 250,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('JALADERA TOALLERO BRK 45x20')),
            'price1'=> 665,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('JALADERA TOALLERO HERRALUM 45x15')),
            'price1'=> 335,
            'unity'=> 'm2',
        ]);
        Process::create([
            'name'=> ucwords(strtolower('JALADERA TOALLERO 45x30 HERR. 2261')),
            'price1'=> 650,
            'unity'=> 'm2',
        ]);

    }
}
