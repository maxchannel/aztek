<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';
    protected $fillable = ['name', 'description', 'price1','price2', 'price3','price4','price5', 'unity', 'quantity', 'min', 'max'];
    protected $dates = ['deleted_at'];

    public function orders()
    {
         return $this->belongsToMany('App\Order');
    }

}
