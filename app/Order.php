<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';
    protected $fillable = ['name', 'description', 'status','m','price','pay','i','limited_at','work_id','user_id','start_at','limited_at'];
    protected $dates = ['deleted_at'];

    public function work()
    {
        return $this->belongsTo('App\Work');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function images()
    {
        return $this->hasMany('App\OrderImage');
    }

    public function del_products()
    {
        return $this->hasMany('App\OrderProduct');
    }

    public function products()
    {
       return $this->belongsToMany('App\Product', 'order_product','order_id', 'product_id')->withPivot('cantidad','id', 'deleted_at','ancho','largo');
    }

    public function techs()
    {
       return $this->belongsToMany('App\User', 'order_teches','order_id', 'user_id')->withPivot('id','deleted_at');
    }

    public function figuresVigent() 
    {
        return $this->figures()->wherePivot('deleted_at', NULL);
    }
    
    public function figures()
    {
       return $this->belongsToMany('App\Figure', 'order_figures','order_id', 'figure_id')->withPivot('id','deleted_at');
    }

}
