<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientProduct extends Model 
{

	use SoftDeletes;

    protected $table = 'client_products';
    protected $fillable = ['cantidad', 'product_id', 'p','client_order_id', 'ancho', 'largo'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\ClientOrder');
    }

    public function product()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

    public function del_processes()
    {
        return $this->hasMany('App\ClientProcess');
    }

    public function processes()
    {
       return $this->belongsToMany('App\Process', 'client_processes','client_product_id', 'process_id')->withPivot('id', 'confirmed', 'cantidad', 'p', 'user_id');
    }

    public function processesMaquilables() 
    {
        return $this->processes()->where('maq', 1);
    }

}
