<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientProcessManual extends Model
{
	use SoftDeletes;

    protected $table = 'client_process_manuals';
    protected $fillable = ['can', 'client_process_id'];
    protected $dates = ['deleted_at'];

    public function process()
    {
        return $this->belongsTo('App\ClientProcess');
    }

}
