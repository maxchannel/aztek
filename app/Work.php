<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Work extends Model
{
    use SoftDeletes;

    protected $table = 'works';
    protected $fillable = ['name', 'status', 'description','residente','residente_c','encargado','client_id','user_id','start_at','limited_at'];
    protected $dates = ['deleted_at'];

    public function client()
    {
        return $this->hasOne('App\Client','id','client_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

}
