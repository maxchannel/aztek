<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientFigure extends Model 
{
	use SoftDeletes;

    protected $table = 'client_figures';
    protected $fillable = ['figure_id', 'client_order_id'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\ClientOrder');
    }

    public function figures()
    {
        return $this->hasMany('App\Figure'); 
    }

}
