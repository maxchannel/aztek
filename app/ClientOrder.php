<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientOrder extends Model 
{
	use SoftDeletes;

    protected $table = 'client_orders';
    protected $fillable = ['name', 'description','status','m','price','pay','limited_at', 'client_id','user_id','start_at','limited_at'];
    protected $dates = ['deleted_at'];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function images()
    {
        return $this->hasMany('App\ClientImage');
    }

    public function del_techs()
    {
        return $this->hasMany('App\ClientTech');
    }

    public function del_figures()
    {
        return $this->hasMany('App\ClientFigure');
    }

    public function del_products()
    {
        return $this->hasMany('App\ClientProduct');
    }


    public function products()
    {
       return $this->belongsToMany('App\Product', 'client_products','client_order_id', 'product_id')->withPivot('cantidad','id', 'deleted_at','ancho','largo');
    }

    public function techs()
    {
       return $this->belongsToMany('App\User', 'client_teches','client_order_id', 'user_id')->withPivot('id','deleted_at');
    }

    public function figuresVigent() 
    {
        return $this->figures()->wherePivot('deleted_at', NULL);
    }
    
    public function figures()
    {
       return $this->belongsToMany('App\Figure', 'client_figures','client_order_id', 'figure_id')->withPivot('id','deleted_at');
    }
}
