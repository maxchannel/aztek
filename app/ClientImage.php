<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientImage extends Model 
{
	use SoftDeletes;

    protected $table = 'client_images';
    protected $fillable = ['route', 'client_order_id'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\ClientOrder');
    }

}
