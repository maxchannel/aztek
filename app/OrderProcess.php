<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProcess extends Model
{
	use SoftDeletes;

    protected $table = 'order_processes';
    protected $fillable = ['cantidad', 'confirmed','p', 'process_id', 'order_product_id'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
