<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'clients';
    protected $fillable = ['name', 'address', 'telephone', 'type', 'descuento'];
    protected $dates = ['deleted_at'];

    public function orders()
    {
        return $this->hasMany('App\ClientOrder');
    }

    //Deleting related rows
    protected static function boot() 
    {
        parent::boot();

        static::deleting(function($client) 
        {
            //Borrando las relaciones tambien
            foreach($client->orders as $order)
            {
                $order->images()->delete();
                $order->del_techs()->delete();
                $order->del_figures()->delete();
                
                $order->del_products()->delete();
                $order->delete();
            }
        });
    }


}
