<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientProcess extends Model 
{
	use SoftDeletes;

    protected $table = 'client_processes';
    protected $fillable = ['cantidad', 'confirmed','p', 'process_id', 'client_product_id'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\ClientProduct');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function manual()
    {
        return $this->hasOne('App\ClientProcessManual', 'id', 'client_process_id');
    }

}
