<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderImage extends Model
{
	use SoftDeletes;

    protected $table = 'order_images';
    protected $fillable = ['route', 'order_id'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

}
