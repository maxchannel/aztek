<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Login extends Model
{
    use SoftDeletes;

    protected $table = 'logins';
    protected $fillable = ['access', 'user_id'];
    protected $dates = ['deleted_at'];
}
