<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderProduct extends Model
{
    use SoftDeletes;

    protected $table = 'order_product';
    protected $fillable = ['cantidad','product_id', 'order_id', 'ancho', 'largo','p'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function product()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

    public function del_processes()
    {
        return $this->hasMany('App\OrderProcess');
    }

    public function processes()
    {
       return $this->belongsToMany('App\Process', 'order_processes','order_product_id', 'process_id')->withPivot('id', 'confirmed', 'cantidad', 'p', 'user_id');
    }

    public function processesMaquilables() 
    {
        return $this->processes()->where('maq', 1);
    }

}
