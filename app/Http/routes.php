<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

//Guest users
Route::group(['prefix' => 'i', 'middleware' => ['guest']], function () {
    //Login
    Route::get('/login', ['as' => 'login', 'uses' => 'SignController@login']);
    Route::post('/login', ['as' => 'loginSend', 'uses' => 'SignController@loginSend']);
    //Singup
    Route::get('/signup', ['as' => 'signup', 'uses' => 'SignController@signup']);
    Route::post('/signup', ['as' => 'signupStore', 'uses' => 'SignController@signupStore']); 
    //Singup
    Route::get('/cotizacion', ['as' => 'cotizacion', 'uses' => 'BudgetController@create']);
    Route::post('/cotizacion', ['as' => 'cotizacion_c', 'uses' => 'BudgetController@store']);      
});

//Auth users
Route::group(['prefix' => 'i', 'middleware' => ['auth']], function () {
    Route::get('/notifications', ['as' => 'notifications', 'uses' => 'PublicController@notifications']);
    Route::delete('/notifications/destroy/{id}', ['as' => 'destroy_not', 'uses' => 'PublicController@destroy_not']);
    //Settings
    Route::get('/settings', ['as' => 'settings', 'uses' => 'UserController@settings']);
    Route::put('/settings', ['as' => 'settings_store', 'uses' => 'UserController@settings_store']);
    Route::get('/settings/change/{new_mode}', ['as' => 'change_mode', 'uses' => 'PublicController@change_mode']);
    //Logout
    Route::get('/logout', ['as' => 'logout', 'uses' => 'SignController@logout']);

});

//Admin users
Route::group(['prefix' => 'admin', 'middleware' => ['auth','is_admin']], function () {

    //Budget
    Route::get('/list/budget', ['as' => 'list_budget', 'uses' => 'BudgetController@show']);
    Route::delete('/list/budget/destroy/{id}', ['as' => 'destroy_budget', 'uses' => 'BudgetController@destroy']);
	
    //Client
    Route::get('/add/client', ['as' => 'add_client', 'uses' => 'ClientController@index']);
    Route::post('/add/client', ['as' => 'add_clientStore', 'uses' => 'ClientController@store']);
    Route::get('/edit/client/{id}', ['as' => 'edit_client', 'uses' => 'ClientController@edit_client']);
    Route::put('/edit/client/{id}', ['as' => 'edit_clientStore', 'uses' => 'ClientController@edit_clientStore']);
	Route::get('/list/client', ['as' => 'list_client', 'uses' => 'ClientController@list_client']);
    Route::delete('/destroy/client/{id}', ['as' => 'destroy_client', 'uses' => 'ClientController@destroy']);

    //Product
    Route::get('/add/product', ['as' => 'add_product', 'uses' => 'ProductController@index']);
    Route::post('/add/product', ['as' => 'add_productStore', 'uses' => 'ProductController@store']);
    Route::get('/edit/product/{id}', ['as' => 'edit_product', 'uses' => 'ProductController@edit_product']);
    Route::put('/edit/product/{id}', ['as' => 'edit_productStore', 'uses' => 'ProductController@edit_productStore']);
    Route::get('/list/product', ['as' => 'list_product', 'uses' => 'ProductController@list_product']);
    Route::delete('/destroy/product/¡{id}', ['as' => 'destroy_product', 'uses' => 'ProductController@destroy']);

    //Figure
    Route::get('/add/figure', ['as' => 'add_figure', 'uses' => 'FigureController@index']);
    Route::post('/add/figure', ['as' => 'add_figure_create', 'uses' => 'FigureController@create']);
    Route::get('/edit/figure/{id}', ['as' => 'edit_figure', 'uses' => 'FigureController@edit']);
    Route::put('/edit/figure/{id}', ['as' => 'edit_figure_update', 'uses' => 'FigureController@update']);
    Route::get('/list/figure', ['as' => 'list_figure', 'uses' => 'FigureController@show']);
    Route::delete('/destroy/figure/{id}', ['as' => 'destroy_figure', 'uses' => 'FigureController@destroy']);

    //Process
    Route::get('/add/process', ['as' => 'add_process', 'uses' => 'ProcessController@index']);
    Route::post('/add/process', ['as' => 'add_process_create', 'uses' => 'ProcessController@create']);
    Route::get('/edit/process/{id}', ['as' => 'edit_process', 'uses' => 'ProcessController@edit']);
    Route::put('/edit/process/{id}', ['as' => 'edit_process_update', 'uses' => 'ProcessController@update']);
    Route::get('/list/process', ['as' => 'list_process', 'uses' => 'ProcessController@show']);
    Route::delete('/destroy/process/¡{id}', ['as' => 'destroy_process', 'uses' => 'ProcessController@destroy']);

    //Incident
    Route::get('/add/incident', ['as' => 'add_incident', 'uses' => 'IncidentController@index']);
    Route::post('/add/incident', ['as' => 'add_incidentStore', 'uses' => 'IncidentController@store']);
    Route::get('/edit/incident/{id}', ['as' => 'edit_incident', 'uses' => 'IncidentController@edit_incident']);
    Route::put('/edit/incident/{id}', ['as' => 'edit_incidentStore', 'uses' => 'IncidentController@update']);
    Route::get('/list/incident', ['as' => 'list_incident', 'uses' => 'IncidentController@list_incident']);

    //Garbage
    Route::get('/list/garbage', ['as' => 'list_garbage', 'uses' => 'GarbageController@list_garbage']);
    Route::delete('/list/garbage/destroy/{id}', ['as' => 'destroy_gar', 'uses' => 'GarbageController@destroy_gar']);
    

    //ORCA
    Route::get('/add/work/panorama-work/{id}', ['as' => 'add_work_panorama_orca', 'uses' => 'OrcaController@panorama_orca']);
    Route::get('/add/work/panorama-order/{id}', ['as' => 'add_work_panorama_orders', 'uses' => 'OrcaController@panorama_orca_orders']);
    //Work
    Route::get('/list/work', ['as' => 'list_work', 'uses' => 'OrcaController@list_work']);
    Route::get('/add/work-orca', ['as' => 'add_work_orca', 'uses' => 'WorkController@add_work_orca']); //Orca
    Route::post('/add/work-orca', ['as' => 'add_work_orcaStore', 'uses' => 'WorkController@add_work_indieCreate']);
    Route::get('/edit/work/{id}', ['as' => 'edit_work', 'uses' => 'OrcaController@edit_work']);
    Route::put('/edit/work/{id}', ['as' => 'edit_workStore', 'uses' => 'OrcaController@edit_workStore']);
    Route::get('/list/work/orders/{id}', ['as' => 'list_work_orders', 'uses' => 'WorkController@list_work_orders']);//Relations
    //Order
    Route::get('/add/work-orca/edit/{id}', ['as' => 'add_work_orca_edit', 'uses' => 'WorkController@edit_orca']);//Editando
    Route::post('/add/work-orca/edit/{id}', ['as' => 'add_work_edit_orca_store', 'uses' => 'WorkController@edit_orcaStore']);//Editando
    Route::get('/add/order', ['as' => 'add_order', 'uses' => 'OrderController@index']);
    Route::post('/add/order', ['as' => 'add_orderStore', 'uses' => 'OrderController@store']);
    Route::get('/edit/order/{id}', ['as' => 'edit_order', 'uses' => 'OrderController@edit_order']);
    Route::put('/edit/order/{id}', ['as' => 'edit_orderStore', 'uses' => 'OrderController@edit_orderStore']);
    Route::get('/list/order', ['as' => 'list_order', 'uses' => 'OrderController@list_order']);

    //Edit Cristales
    Route::get('/list/orca/products/{id}', ['as' => 'list_order_products_orca', 'uses' => 'CristalController@orca']);
    Route::post('/list/orca/products/{id}', ['as' => 'list_order_products_store_orca', 'uses' => 'CristalController@list_order_products_store_orca']);
    Route::delete('/list/orca/products/destroy/{id}', ['as' => 'delete_order_product_orca', 'uses' => 'CristalController@delete_order_product_orca']);
    //Edit images
    Route::get('/list/orca/images/{id}', ['as' => 'list_order_images_orca', 'uses' => 'WorkController@list_order_images_orca']);
    Route::put('/list/orca/images/{id}', ['as' => 'list_order_images_store_orca', 'uses' => 'WorkController@list_order_images_store_orca']);
    Route::delete('/list/orca/images/destroy/{id}', ['as' => 'delete_order_image_orca', 'uses' => 'WorkController@delete_order_image_orca']);
    //Edit Figures
    Route::get('/list/orca/figures/{id}', ['as' => 'list_order_figures_orca', 'uses' => 'WorkController@list_order_figures_orca']);
    Route::put('/list/orca/figures/{id}', ['as' => 'list_order_figures_store_orca', 'uses' => 'WorkController@list_order_figures_store_orca']);
    Route::delete('/orca/destroy/figures/{id}', ['as' => 'destroy_client_figure_orca', 'uses' => 'WorkController@destroy_client_figure_orca']);
    //ORCA


    //AZTECA
    Route::get('/add/work', ['as' => 'add_work', 'uses' => 'WorkController@index']);//Azteca
    Route::post('/add/work/list', ['as' => 'add_work_store', 'uses' => 'WorkController@store']);//Guardar y Previo
    Route::get('/add/work/panorama/{id}', ['as' => 'add_work_panorama', 'uses' => 'WorkController@panorama']);//Eligiendo que hacer con pedido
    Route::get('/add/work/edit/{id}', ['as' => 'add_work_edit', 'uses' => 'WorkController@edit']);//Editando
    Route::post('/add/work/edit/{id}', ['as' => 'add_work_edit_store', 'uses' => 'WorkController@editStore']);//Editando
    Route::post('/add/work/update/{id}', ['as' => 'add_work_update_status', 'uses' => 'WorkController@updateStatus']);//Editando
    //Edit images
    Route::get('/list/az/images/{id}', ['as' => 'list_order_images_az', 'uses' => 'WorkController@list_order_images_az']);
    Route::put('/list/az/images/{id}', ['as' => 'list_order_images_store_az', 'uses' => 'WorkController@list_order_images_store_az']);
    Route::delete('/list/az/images/destroy/{id}', ['as' => 'delete_order_image_az', 'uses' => 'WorkController@delete_order_image_az']);
    //Edit Figures
    Route::get('/list/az/figures/{id}', ['as' => 'list_order_figures_az', 'uses' => 'WorkController@list_order_figures_az']);
    Route::put('/list/az/figures/{id}', ['as' => 'list_order_figures_store_az', 'uses' => 'WorkController@list_order_figures_store_az']);
    Route::delete('/list/destroy/figures/{id}', ['as' => 'destroy_client_figure', 'uses' => 'WorkController@destroy_client_figure']);
    //Edit Cristales
    Route::get('/list/az/products/{id}', ['as' => 'list_order_products_az', 'uses' => 'CristalController@index']);
    Route::post('/list/az/products/{id}', ['as' => 'list_order_products_store_az', 'uses' => 'CristalController@list_order_products_store_az']);
    Route::delete('/list/az/products/destroy/{id}', ['as' => 'delete_order_product_az', 'uses' => 'CristalController@delete_order_product_az']);
    //Edit images
    Route::get('/list/images/{id}', ['as' => 'list_order_images', 'uses' => 'OrderController@list_order_images']);
    Route::put('/list/images/{id}', ['as' => 'list_order_images_store', 'uses' => 'OrderController@list_order_images_store']);
    Route::delete('/list/images/destroy/{id}', ['as' => 'delete_order_image', 'uses' => 'OrderController@delete_order_image']);
    //Edit products
    Route::get('/list/products/{id}', ['as' => 'list_order_products', 'uses' => 'OrderController@list_order_products']);
    Route::put('/list/products/{id}', ['as' => 'list_order_products_store', 'uses' => 'OrderController@list_order_products_store']);
    Route::delete('/list/products/destroy/{id}', ['as' => 'delete_order_product', 'uses' => 'OrderController@delete_order_product']);
    //Agregar medida manual
    Route::get('/process/az/manual/{id}/{order_id}', ['as' => 'manual_process_az', 'uses' => 'WorkController@manual_process_az']);
    Route::post('/process/az/manual/{id}/{order_id}', ['as' => 'manual_process_az_store', 'uses' => 'WorkController@manual_process_az_store']);
    Route::delete('/process/destroy/manual/{id}', ['as' => 'destroy_manual_process', 'uses' => 'WorkController@destroy_manual_process']);
    //AZTECA


    //All
    Route::get('/statistic/stock', ['as' => 'statistic_stock', 'uses' => 'StatisticController@stock']);
    Route::get('/search', ['as' => 'command_line', 'uses' => 'SearchController@search']);
    Route::get('/list/operator', ['as' => 'list_operator', 'uses' => 'PublicController@list_operator']);
    Route::get('/pdf/cotizacion/{id}', ['as' => 'pdf_cotizacion', 'uses' => 'PDFController@index']);
    Route::get('/pdf/orca/{id}', ['as' => 'pdf_orca', 'uses' => 'PDFController@orca']);
});

//Tech users
Route::group(['prefix' => 'tech', 'middleware' => ['auth','is_tech']], function () {
    Route::get('/all/list', ['as' => 'tech_list_order_all', 'uses' => 'TechController@all_list']);

    Route::get('/order/list', ['as' => 'tech_list_order', 'uses' => 'TechController@clientes']);
    Route::get('/order/details/{id}', ['as' => 'tech_details', 'uses' => 'TechController@tech_details']);
    Route::get('/order/take/{id}', ['as' => 'tech_take', 'uses' => 'TechController@tech_take']);
    Route::post('/order/take/{id}', ['as' => 'tech_take_update', 'uses' => 'TechController@tech_take_update']);
    Route::post('/order/take/confirm/{id}', ['as' => 'tech_take_indie', 'uses' => 'TechController@tech_take_indie']);
    Route::get('/orca/figures-az/{id}', ['as' => 'list_figures_az', 'uses' => 'TechController@list_figures_az']);
    //Orca
    Route::get('/orca/list', ['as' => 'tech_list_order_orca', 'uses' => 'TechController@orca']);
    Route::get('/orca/details/{id}', ['as' => 'tech_details_orca', 'uses' => 'TechController@tech_details_orca']);
    Route::get('/orca/take/{id}', ['as' => 'tech_take_orca', 'uses' => 'TechController@tech_take_orca']);
    Route::post('/orca/take/{id}', ['as' => 'tech_take_update_orca', 'uses' => 'TechController@tech_take_update_orca']);
    Route::post('/orca/take/confirm/{id}', ['as' => 'tech_take_indie_orca', 'uses' => 'TechController@tech_take_indie_orca']);
    Route::get('/orca/figures/{id}', ['as' => 'list_figures', 'uses' => 'TechController@list_figures']);
    
    //Desecho
    Route::get('/garbage/create', ['as' => 'garbage', 'uses' => 'GarbageController@garbage']);
    Route::post('/garbage/create', ['as' => 'garbage_create', 'uses' => 'GarbageController@garbage_create']);
});