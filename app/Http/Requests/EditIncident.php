<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditIncident extends Request 
{
	public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'content' => 'required|max:900',

        ];
    }

}
