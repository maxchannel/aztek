<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateOrcaOrder extends Request 
{

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name' => 'max:100',
            'work_id' => 'required|numeric',
            'price' => 'numeric',
            'pay' => 'numeric'
		];
	}

}
