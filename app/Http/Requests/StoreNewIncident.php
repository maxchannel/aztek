<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewIncident extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'content' => 'required|max:900',
            'order_id' => 'required',
        ];
    }
}
