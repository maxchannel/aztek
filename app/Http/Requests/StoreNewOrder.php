<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewOrder extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'description' => 'max:900',
            'work_id' => 'required',
        ];
    }
}
