<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreEditWork extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'client_id' => 'required|numeric',
            'price' => 'numeric',
            'pay' => 'numeric'
        ];
    }
}
