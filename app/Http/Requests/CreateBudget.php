<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateBudget extends Request
{
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
            'name' => 'required|max:100',
            'c' => 'required|max:100',
            'm' => 'required|max:500',
        ];
	}

}
