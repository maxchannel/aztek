<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateFiguresOder extends Request
{
	public function authorize()
    {
        return true;
    }

	public function rules()
    {
        $rules = [];

        foreach($this->request->get('figuras') as $key => $val)
        {
            $rules['figuras.'.$key] = 'numeric';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('figuras') as $key => $val)
        {
            $messages['figuras.'.$key.'.max'] = 'La  "Figura '.$key.'" debe ser númerico';
        }
        return $messages;
    }

}
