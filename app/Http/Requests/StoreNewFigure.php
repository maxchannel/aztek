<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewFigure extends Request 
{
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
            'name' => 'required|max:100',
            'file' => 'mimes:jpeg,bmp,png|max:10000'
        ];
	}

}
