<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewWorkIndie extends Request 
{

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name' => 'required|max:100',
            'description' => 'max:900',
            'residente' => 'required|max:100',
            'residente_c' => 'required|max:100',
            'encargado' => 'required|max:100',
		];
	}

}
