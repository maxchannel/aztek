<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewProductOrder extends Request 
{
	public function authorize()
    {
        return true;
    }

	public function rules()
    {
        $rules = [
            ''
        ];

        foreach($this->request->get('cantidad') as $key => $val)
        {
            $rules['cantidad.'.$key] = 'numeric';
        }

        foreach($this->request->get('cristal') as $key => $val)
        {
            $rules['cristal.'.$key] = 'numeric';
        }

        foreach($this->request->get('ancho') as $key => $val)
        {
            $rules['ancho.'.$key] = 'numeric';
        }

        foreach($this->request->get('largo') as $key => $val)
        {
            $rules['largo.'.$key] = 'numeric';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];
        foreach($this->request->get('cantidad') as $key => $val)
        {
            $messages['cantidad.'.$key.'.max'] = 'La  "Cantidad '.$key.'" debe ser númerico';
        }
        foreach($this->request->get('cristal') as $key => $val)
        {
            $messages['cristal.'.$key.'.max'] = 'El  "Cristal '.$key.'" debe ser númerico';
        }
        foreach($this->request->get('ancho') as $key => $val)
        {
            $messages['ancho.'.$key.'.max'] = 'El  "Ancho '.$key.'" debe ser númerico';
        }
        foreach($this->request->get('largo') as $key => $val)
        {
            $messages['largo.'.$key.'.max'] = 'El  "Largo '.$key.'" debe ser númerico';
        }
        return $messages;
    }

}
