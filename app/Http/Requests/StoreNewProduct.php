<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewProduct extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:200',
            'description' => 'max:500',
            'quantity' => 'numeric',
            'min' => 'numeric',
            'max' => 'numeric',
            'price1' => 'required|numeric',
            'price2' => 'numeric',
            'price3' => 'numeric',
            'price4' => 'numeric',
            'price5' => 'numeric',
            'unity' => 'required',
        ];
    }
}
