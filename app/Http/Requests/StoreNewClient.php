<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreNewClient extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'address' => 'max:400',
            'telephone' => 'numeric',
            'type' => 'required'
        ];
    }
}
