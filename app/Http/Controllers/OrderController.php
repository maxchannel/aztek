<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreNewOrder;
use App\Work;
use App\Product;
use App\Figure;
use App\Process;
use App\Client;
use App\ClientOrder;
use App\ClientProcess;
use App\ClientProduct;
use App\Order;
use App\OrderFigure;
use App\OrderProduct;
use App\OrderProcess;
use App\OrderImage;
use Aws\S3\S3Client;
use Aws\Common\Aws;

class OrderController extends Controller
{
    public function index()
    {
        $works = Work::orderBy('name', 'ASC')->lists('name', 'id');
        $figures = Figure::orderBy('name', 'ASC')->lists('name', 'id');
        $processes = Process::where('id','!=',1)->orderBy('name', 'ASC')->lists('name', 'id');
        $products = Product::orderBy('name', 'ASC')->lists('name', 'id');
        

        $processes_t = Process::all();
        $products_t = Product::all();
        $p_t_count = count($products_t);

        //AWS
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        $fig_first = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/default.jpg');
        //AWS

        return view('add.order', 
            compact('works', 'products', 'processes', 'p_t_count', 'processes_t','products_t', 'figures', 'fig_first'));
    }

    public function store(StoreNewOrder $request)
    {
        $order = new Order;
        $order->fill($request->all());
        $order->status = $request->input('status');
        $order->user_id = \Auth::user()->id;
        $order->limited_at = $request->input('limited_at');
        if($request->input('instalation') == 'yes')
        {
            $order->instalation = 1;
        }else
        {
            $order->instalation = 0;
        }
        if($request->input('suministration') == 'yes')
        {
            $order->suministration = 1;
        }else
        {
            $order->suministration = 0;
        }
        $order->save();

        //Cristales 
        $cristal = $request->input('cristal');
        $cantidad = $request->input('cantidad');
        $ancho = $request->input('ancho');
        $largo = $request->input('largo');
        $process = $request->input('process');
        $cantidad_p = $request->input('cantidad_p');
        $p_p = $request->input('p_p');
        $precio = $request->input('precio');

        $number = count($cristal);  
        if($number > 0)  
        {  
            for($i=0; $i<$number; $i++)  
            {
                if(!empty($cantidad[$i]) && !empty($cristal[$i]) && !empty($ancho[$i]) && !empty($largo[$i]) && !empty($precio[$i]))//Solo si se envia nombre y contenido
                {  
                    $order_product = new OrderProduct;
                    $order_product->order_id = $order->id;
                    $order_product->cantidad = $cantidad[$i];
                    $order_product->product_id = $cristal[$i];
                    $order_product->ancho = $ancho[$i];
                    $order_product->largo = $largo[$i];
                    $order_product->p = $precio[$i];
                    $order_product->save();

                    //Procesos
                    $p_count = count($process[0]);  //Error
                    if($p_count > 0)  
                    {  
                        for($j=0; $j<$p_count; $j++)  
                        {
                            if(!empty($process[$i][$j]) && !empty($cantidad_p[$i][$j]) && !empty($p_p[$i][$j]))//Solo si se envia nombre y contenido
                            {  
                                $order_process = new OrderProcess;
                                $order_process->cantidad = 1;
                                $order_process->p = 1;
                                $order_process->confirmed = 0;
                                $order_process->order_product_id = $order_product->id;
                                $order_process->process_id = 1;
                                $order_process->save();

                                for($b=0; $b < $cantidad_p[$i][$j]; $b++) 
                                { 
                                    $order_process = new OrderProcess;
                                    $order_process->cantidad = 1;
                                    $order_process->p = $p_p[$i][$j];
                                    $order_process->confirmed = 0;
                                    $order_process->order_product_id = $order_product->id;
                                    $order_process->process_id = $process[$i][$j];
                                    $order_process->save();
                                }
                            }   
                        } 
                    } 
                    //Procesos
                }  
            } 
        }
        //Cristales

        //Figuras al cristal
        if($request->input('figuras_check_a') == true)
        {
            $fig = new OrderFigure;
            $fig->order_id = $order->id;
            $fig->figure_id = 1;
            $fig->save();
        }

        $figuras = $request->input('figuras');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($figuras[$i]))//Solo si se envia nombre y contenido
            {  
                $fig2 = new OrderFigure;
                $fig2->order_id = $order->id;
                $fig2->figure_id = $figuras[$i];
                $fig2->save();
            }
        }
        //Figuras al cristal

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $llaves=>$file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:5000'];
                $validation = \Validator::make(['file'=> $file], $rules);    
                if(!$validation->fails()) 
                {
                    if($llaves < 15)//Solo 15 imagenes por anuncio
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(15).".".$extension;    
                        $path = storage_path().'/uploads/'.$newName;

                        //Insert in db
                        $image = new OrderImage;
                        $image->order_id = $order->id;
                        $image->route = $newName;
                        $image->save();  
                        
                        ///Move file to images/post
                        $file->move(storage_path().'/uploads/', $newName);

                        //Resize
                        \Image::make($path)->resize(900, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($path);

                        $aws = Aws::factory(config('filesystems.disks.s3images'));
                        $client = $aws->get('S3');
                        $result = $client->putObject(array(
                            'Bucket'     => config('filesystems.disks.s3images.bucket'),
                            'SourceFile' => $path,
                            'Key'        => 'images/obras/'.$newName,
                            'ACL'        => 'public-read'
                        ));

                        \File::delete($path);
                    }else
                    {
                        \Session::flash('image-message', 'Solo se agregaron 15 imágenes');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                }
            }
        }
        //Image

        return \Redirect::route('add_work_panorama_orders', ['id'=>$order->id])->with('message', 'Creado con Éxito');
    }

    public function add_work_panorama_orca($id)//Descontinuado
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);
        $products = OrderProduct::where('client_order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        $cris=0;
        $pros=0;
        //Price Automatico
        if($order->price == 0)
        {
            //Sumando Precio de cristal
            foreach($products as $key => $c_product) 
            { 
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 
                //$x[$key][4] = $c_product->product->price4; 
                //$x[$key][5] = $c_product->product->price5; 

                $cristales[$key] = $c_product->product->name;
                $cris = $cris+($x[$c_product->p]*$c_product->cantidad*$c_product->ancho*$c_product->largo);    

                //Precio de los procesos
                foreach($c_product->processes as $process) 
                {
                    $y[$key][1] = $process->price1;
                    $y[$key][2] = $process->price2;
                    $y[$key][3] = $process->price3;
                    //$y[$key][4] = $process->price4;
                    //$y[$key][5] = $process->price5;

                    $pros = $pros+($y[$key][$process->pivot->p]*$process->pivot->cantidad);
                }    

            }
            
            if($order->i > 0)//IVA
            {
                $sub = $cris+$pros;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $cris+$pros;
            }
            //return $precio;
        }else//Price Manual
        {
            if($order->i > 0)//IVA
            {
                $sub = $order->price;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $order->price;
            }

            foreach($products as $key => $c_product) 
            {
                $cristales[$key] = $c_product->product->name;
            }
        }

        return view('add.preview.work_panorama', compact('order', 'products', 'conteo', 'cristales', 'precio', 
            'iva', 'sub', 'x', 'y'));
    }

    public function list_order()//Pedidos a Clientes
    {
        if(\Request::input('m') == "t")
        {
            $con1 = ClientOrder::where('status','T');
            $k = ClientOrder::where('status','T')->orderBy('created_at', 'DESC')->get();
        }else{
            $con1 = ClientOrder::where('status','C')->orWhere('status','S')->orWhere('status','P');
            $k = ClientOrder::where('status','C')->orWhere('status','S')->orWhere('status','P')->orderBy('created_at', 'DESC')->get();
        }

        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'name')
            {
                $con1->orderBy('name','DESC');
            }elseif($sort == 'name_a')
            {
                $con1->orderBy('name','ASC');
            }elseif($sort == 'fecha')
            {
                $con1->orderBy('created_at','DESC');
            }elseif($sort == 'fecha_a')
            {
                $con1->orderBy('created_at','ASC');
            }
        }else
        {
            $con1->orderBy('created_at', 'DESC');
        }

        //Avance
        foreach ($k as $key => $order) 
        {
            $avan[$key] = 0;
            foreach($order->del_products as $k => $product)
            {
                $t = ClientProcess::where('client_product_id', $product->id)->count();
                $c = ClientProcess::where('client_product_id', $product->id)->where('confirmed', 1)->count();
                if($t > 0) 
                {
                    $avan[$key] = $avan[$key] + round($c*(100/$t));
                }
            }
        }

        $orders = $con1->paginate(25);
        return view('list.order', compact('orders', 'avan'));
    }

    public function edit_order($id)
    {
    	$order = Order::findOrFail($id);

        return view('edit.order', compact('order'));
    }

    public function edit_orderStore($id, StoreNewOrder $request)
    {
    	$order = Order::findOrFail($id);
    	$order->name = $request->input('name');
    	$order->save();

        return \Redirect::back()->with(['message'=>'Editado con Exito']);
    }

    //Images en el order
    public function delete_order_image($id)//Delete
    {
        $order_image = OrderImage::find($id);
        $this->notFoundUnless($order_image);
        $order_image->delete();

        return \Redirect::back()->with('message', 'Eliminada con exito');
    }

    //Material
    public function list_order_products($id)
    {
        $order = Order::find($id);

        return view('list.relations.material', compact('order'));
    }

    public function delete_order_product($id)//Delete
    {
        $order_product = OrderProduct::findOrFail($id);
        $order_product->delete();
    }
    //Material

    public function list_order_images($id)
    {
        $order = Order::find($id);

        return view('list.relations.image', compact('order'));
    }

    public function list_order_images_store($id)
    {
        $order = Order::findOrFail($id);

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                $validation = \Validator::make(['file'=> $file], $rules);    

                if(!$validation->fails()) 
                {
                    //Limitando el numero de imagenes
                    if($order->images()->count() < 15)
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(6).".".$extension;        

                        //Insert in db
                        $image = new OrderImage;
                        $image->order_id = $order->id;
                        $image->route = $newName;
                        $image->save();        

                        ///Move file to images/post
                        $file->move('images/obras/',$newName); 
                    }else
                    {
                        return \Redirect::back()->with('image-message', 'Alcanzaste el limite de 12 imágenes por anuncio');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes menores a 4MB');
                }
            }
        }else
        {
            return \Redirect::back()->with('image-message', 'Debes seleccionar al menos 1 imágen');
        }

        $order->save();
        return \Redirect::back()->with(['message'=>'Se agregaron nuevas imágenes a tu pedido']);
    }


}