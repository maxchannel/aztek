<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\EditWork;
use App\Work;
use App\Order;
use App\OrderProduct;
use App\OrderImage;
use App\OrderFigure;
use App\OrderProcess;
use Aws\S3\S3Client;
use Aws\Common\Aws;

class OrcaController extends Controller 
{
	public function panorama_orca($id)
    {
        $work = Work::find($id);
        $this->notFoundUnless($work);

        //$consulta = Order::where('work_id',$work->id)->orderBy('created_at', 'DESC');
        if(\Request::input('m') == "t")
        {
            $consulta = Order::where('work_id',$work->id)->where('status','T')->orderBy('created_at', 'DESC');
        }else{
            $consulta = Order::where('work_id',$work->id)->where('status','C')->orWhere('status','S')->orWhere('status','P')->orderBy('created_at', 'DESC');
        }


        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'area')
            {
                $consulta->orderBy('name', 'DESC');
            }elseif($sort == 'area_a')
            {
                $consulta->orderBy('name', 'ASC');
            }elseif($sort == 'fecha')
            {
                $consulta->orderBy('created_at', 'DESC');
            }elseif($sort == 'fecha_a')
            {
                $consulta->orderBy('created_at', 'ASC');
            }
        }

        $k = $consulta->get();

        //Avance
        foreach ($k as $key => $order) 
        {
            $avan[$key] = 0;
            foreach($order->del_products as $product)
            {
                $t = OrderProcess::where('order_product_id', $product->id)->count();
                $c = OrderProcess::where('order_product_id', $product->id)->where('confirmed', 1)->count();
                if($t > 0) 
                {
                    $avan[$key] = $avan[$key] + round($c*(100/$t));
                }
            }
        }

        $results = $consulta->paginate(25);
        return view('add.preview.work_panorama_orca', compact('work','results', 'avan'));
    }

    public function panorama_orca_orders($id)
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);
        $products = OrderProduct::where('order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        if($order->price == 0)//Price Automatico
        {
            $prev=0;
            $fg = 0;
            //Sumando Precio de cristal
            foreach($products as $key => $c_product) 
            { 
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 
                $x[$key][4] = $c_product->product->price4; 
                $x[$key][5] = $c_product->product->price5; 

                $cristales[$key] = $c_product->product->name;
                $importe[$key] = $x[$key][$c_product->p]*$c_product->ancho*$c_product->largo;

                //Precio de los procesos
                foreach($c_product->processes as $j => $process) 
                {
                    $y[$j][1] = $process->price1;
                    $y[$j][2] = $process->price2;
                    $y[$j][3] = $process->price3; 
                    $y[$j][4] = $process->price4; 
                    $y[$j][5] = $process->price5; 
                    $stereo[$fg] = $y[$j][$process->pivot->p];
                    $torr[$fg] = 0;

                    if($process->unity == "ml")//Perimetro
                    {
                        $importe[$key] += $process->pivot->cantidad*($stereo[$fg] * (($c_product->largo+$c_product->ancho)*2));
                        $torr[$fg] = $process->pivot->cantidad*($stereo[$fg] * (($c_product->largo+$c_product->ancho)*2));
                    }elseif($process->unity == "veces")//Perimetro
                    {
                        $importe[$key] += $process->pivot->cantidad*$stereo[$fg];
                        $torr[$fg] = $process->pivot->cantidad*$stereo[$fg];
                    }else
                    {
                        $importe[$key] += $process->pivot->cantidad*($stereo[$fg]*($c_product->largo*$c_product->ancho));
                        $torr[$fg] = $process->pivot->cantidad*($stereo[$fg]*($c_product->largo*$c_product->ancho));
                    }

                    $fg++;
                }    

                $importe[$key] *= $c_product->cantidad;
                $prev += $importe[$key];
            }
            
            if($order->i > 0)//IVA
            {
                $sub = $prev;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $prev;
            }

        }else//Price Manual
        {
            if($order->i > 0)//IVA
            {
                $sub = $order->price;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $order->price;
            }

            $fg = 0;
            foreach($products as $key => $c_product) 
            {
                $cristales[$key] = $c_product->product->name;
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 

                foreach($c_product->processes as $j => $process) 
                {
                    $y[$j][1] = $process->price1;
                    $y[$j][2] = $process->price2;
                    $y[$j][3] = $process->price3; 
                    $stereo[$fg] = $y[$j][$process->pivot->p];
                    $torr[$fg] = 0;

                    if($process->unity == "ml")//Perimetro
                    {
                        $torr[$fg] = $process->pivot->cantidad*($stereo[$fg] * (($c_product->largo+$c_product->ancho)*2));
                    }elseif($process->unity == "veces")//Perimetro
                    {
                        $torr[$fg] = $process->pivot->cantidad*$stereo[$fg];
                    }else
                    {
                        $torr[$fg] = $process->pivot->cantidad*($stereo[$fg]*($c_product->largo*$c_product->ancho));
                    }

                    $fg++;
                }
            }
                
        }

        //AWS
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        if($order->images->count() > 0)//Imagen de AWS
        {
            $imh = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$order->images->first()->route);
        }
        if($order->figuresVigent->count() > 0)//Figuras de AWS
        {
            $fig_first = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$order->figures->first()->route);
        }
        //AWS

        return view('add.preview.work_panorama_orders', compact('order', 'products', 'conteo', 'cristales', 'precio', 
            'iva', 'sub', 'x', 'y', 'imh', 'fig_first', 'importe', 'stereo', 'torr'));
    }

    public function list_work()
    {
        $works = Work::orderBy('name','DESC')->paginate(25);
        
        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'name')
            {
                $works = Work::orderBy('name','DESC')->paginate(25);
            }elseif($sort == 'name_a')
            {
                $works = Work::orderBy('name','ASC')->paginate(25);
            }elseif($sort == 'fecha')
            {
                $works = Work::orderBy('created_at','DESC')->paginate(25);
            }elseif($sort == 'fecha_a')
            {
                $works = Work::orderBy('created_at','ASC')->paginate(25);
            }
        }

        return view('list.work', compact('works'));
    }

    public function edit_work($id)
    {
    	$work = Work::findOrFail($id);

        return view('edit.work', compact('work'));
    }

    public function edit_workStore($id, EditWork $request)
    {
    	$work = Work::findOrFail($id);
    	$work->fill($request->all());
    	$work->save();

        return \Redirect::back()->with(['message'=>'Editado con Exito']);
    }


}
