<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use App\ClientOrder;
use App\ClientProduct;
use App\Order;
use App\OrderProduct;
use App\ClientProcessManual;

class PDFController extends Controller 
{

	public function index($id)
	{
		$order = ClientOrder::find($id);
        $this->notFoundUnless($order);
        $products = ClientProduct::where('client_order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        $total_m2 = 0;

        if($order->price == 0)//Price Automatico
        {
            $prev=0;
            $fg = 0;
            $fh = 0;
            //Sumando Precio de cristal
            foreach($products as $key => $c_product) 
            { 
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 

                $cristales[$key] = $c_product->product->name;
                $importe[$key] = $x[$key][$c_product->p]*$c_product->ancho*$c_product->largo;
                $vidrio[$key] = $importe[$key];
                //Total de m2
                $total_m2 += $c_product->ancho*$c_product->largo;

                //Precio de los procesos
                foreach($c_product->processes as $j => $process) 
                {
                    //Si existe una medida enviada manualmente se guarda en el array
                    if(ClientProcessManual::where('client_process_id', $process->pivot->id)->exists())
                    {
                        $process_manual = ClientProcessManual::where('client_process_id', $process->pivot->id)->first();
                        $manual[$fh] = $process_manual->can;
                    }else
                    {
                        $manual[$fh] = "";
                    }
                    
                    $y[$j][1] = $process->price1;
                    $y[$j][2] = $process->price2;
                    $y[$j][3] = $process->price3; 
                    $stereo[$fh] = $y[$j][$process->pivot->p];
                    $torr[$fh] = 0;

                    if($process->unity == "ml")//Perimetro
                    {
                        if($manual[$fh] != "")//Si hay medida ingresada manualmente
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh] * $manual[$fh]);
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh] * $manual[$fh]);
                        }else
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh] * (($c_product->largo+$c_product->ancho)*2));
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh] * (($c_product->largo+$c_product->ancho)*2));
                        }
                    }elseif($process->unity == "veces")//Perimetro
                    {
                        $importe[$key] += $process->pivot->cantidad*$stereo[$fh];
                        $torr[$fh] = $process->pivot->cantidad*$stereo[$fh];
                    }elseif($process->unity == "m2")//Si hay medida ingresada manualmente
                    {
                        if($manual[$fh] != "")
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh]*$manual[$fh]);
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh]*$manual[$fh]);
                        }else
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh]*($c_product->largo*$c_product->ancho));
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh]*($c_product->largo*$c_product->ancho));
                        }
                    }

                    $fg++;

                } 

                $fh++;//Incremento de medidas manuales
                $importe[$key] *= $c_product->cantidad;
                $prev += $importe[$key];
            }
            
            if($order->i > 0)//IVA
            {
                $sub = $prev;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $prev;
            }

        }else//Price Manual
        {
            $importe = 0;
            if($order->i > 0)//IVA
            {
                $sub = $order->price;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $order->price;
            }

            foreach($products as $key => $c_product) 
            {
                $cristales[$key] = $c_product->product->name;
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3;
                $x[$key][4] = $c_product->product->price4;
                $x[$key][5] = $c_product->product->price5; 

                foreach($c_product->processes as $process) 
                {
                    $y[$key][1] = $process->price1;
                    $y[$key][2] = $process->price2;
                    $y[$key][3] = $process->price3; 
                    $y[$key][4] = $process->price4; 
                    $y[$key][5] = $process->price5; 
                }
            }
        }

        $orden = \PDF::loadView('pdf.cotizacion', ['modo'=>1,'order'=>$order, 'sub'=>$sub, 'iva'=>$iva, 'precio'=>$precio,'conteo'=>$conteo,'products'=>$products, 'x'=>$x, 'y'=>$y, 'importe'=>$importe])->setPaper('letter', 'portrait');
        return $orden->stream();
	}

    public function orca($id)
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);
        $products = OrderProduct::where('order_id', $order->id)->where('deleted_at',NULL)->with('product')->get();
        $conteo = count($products);

        if($conteo == 0)
        {
            $x[0][0] = 0;
            $y[0][0] = 0;
            $importe[0] = 0;
        }

        if($order->price == 0)//Price Automatico
        {
            $prev=0;
            $fg = 0;
            //Sumando Precio de cristal
            foreach($products as $key => $c_product) 
            { 
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 
                $x[$key][4] = $c_product->product->price4; 
                $x[$key][5] = $c_product->product->price5; 

                $cristales[$key] = $c_product->product->name;
                $importe[$key] = $x[$key][$c_product->p]*$c_product->ancho*$c_product->largo;

                //Precio de los procesos
                foreach($c_product->processes as $j => $process) 
                {
                    $y[$j][1] = $process->price1;
                    $y[$j][2] = $process->price2;
                    $y[$j][3] = $process->price3; 
                    $y[$j][4] = $process->price4; 
                    $y[$j][5] = $process->price5; 
                    $stereo[$fg] = $y[$j][$process->pivot->p];
                    $torr[$fg] = 0;

                    if($process->unity == "ml")//Perimetro
                    {
                        $importe[$key] += $process->pivot->cantidad*($stereo[$fg] * (($c_product->largo+$c_product->ancho)*2));
                        $torr[$fg] = $process->pivot->cantidad*($stereo[$fg] * (($c_product->largo+$c_product->ancho)*2));
                    }elseif($process->unity == "veces")//Perimetro
                    {
                        $importe[$key] += $process->pivot->cantidad*$stereo[$fg];
                        $torr[$fg] = $process->pivot->cantidad*$stereo[$fg];
                    }else
                    {
                        $importe[$key] += $process->pivot->cantidad*($stereo[$fg]*($c_product->largo*$c_product->ancho));
                        $torr[$fg] = $process->pivot->cantidad*($stereo[$fg]*($c_product->largo*$c_product->ancho));
                    }

                    $fg++;
                }    

                $importe[$key] *= $c_product->cantidad;
                $prev += $importe[$key];
            }
            
            if($order->i > 0)//IVA
            {
                $sub = $prev;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $prev;
            }

        }else//Price Manual
        {
            $importe = 0;
            if($order->i > 0)//IVA
            {
                $sub = $order->price;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $order->price;
            }

            foreach($products as $key => $c_product) 
            {
                $cristales[$key] = $c_product->product->name;
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 
                $x[$key][4] = $c_product->product->price4; 
                $x[$key][5] = $c_product->product->price5; 

                foreach($c_product->processes as $process) 
                {
                    $y[$key][1] = $process->price1;
                    $y[$key][2] = $process->price2;
                    $y[$key][3] = $process->price3; 
                    $y[$key][4] = $process->price4; 
                    $y[$key][5] = $process->price5; 
                }
            }
        }

        $orden = \PDF::loadView('pdf.cotizacion', ['modo'=>2,'order'=>$order, 'sub'=>$sub, 'iva'=>$iva, 'precio'=>$precio,'conteo'=>$conteo,'products'=>$products, 'x'=>$x, 'y'=>$y, 'importe'=>$importe])->setPaper('letter', 'portrait');
        return $orden->stream();
    }

    public function master()
    {
        return view('pdf.cotizacion');
    }

}
