<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\EditUserRequest;

class UserController extends Controller 
{
    public function settings()
    {
        $user = User::findOrFail(\Auth::user()->id);

        return view('settings.index', compact('user'));
    }

    public function settings_store(EditUserRequest $request)
    {
        $user = User::findOrFail(\Auth::user()->id);
        $user->fill($request->all());
        $user->save();

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }


}
