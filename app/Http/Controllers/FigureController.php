<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Figure;
use App\Http\Requests\StoreNewFigure;
use App\Http\Requests\EditFigure;
use Aws\Common\Aws;

class FigureController extends Controller 
{
	public function index()
	{
        return view('add.figure');
    }

	public function create(StoreNewFigure $request)
	{
    	$figure = new Figure;
        $figure->name = $request->input('name');

        //Image
        $file = \Input::file('file');
        $extension = $file->getClientOriginalExtension(); 
        $newName = str_random(15).".".$extension;    
        $path = storage_path().'/uploads/'.$newName;

        $file->move(storage_path().'/uploads/', $newName);

        //Resize
        \Image::make($path)->resize(900, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save($path);

   
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        $result = $client->putObject(array(
            'Bucket'     => config('filesystems.disks.s3images.bucket'),
            'SourceFile' => $path,
            'Key'        => 'images/figuras/'.$newName,
            'ACL'        => 'public-read'
        ));

        \File::delete($path);
        //Image

        $figure->route = $newName;
    	$figure->save();

        return \Redirect::route('list_figure')->with('message', 'Creado Con Éxito');
    }

    public function edit($id)
	{
    	$figure = Figure::find($id);
        $this->notFoundUnless($figure);

        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        $imh = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$figure->route);

        return view('edit.figure', compact('figure', 'imh'));
    }

	public function update($id, EditFigure $request)
	{
        $file = \Input::file('file');
        $validation = \Validator::make(['file'=> $file], ['file' => 'required|image|max:9000']);
        
        if(!$validation->fails())
        {
            //Update database and delete old image
            $figure = Figure::find($id);

            //Image
            $file = \Input::file('file');
            $extension = $file->getClientOriginalExtension(); 
            $newName = str_random(15).".".$extension;    
            $file->move(storage_path().'/uploads/', $newName);
            $path = storage_path().'/uploads/'.$newName;

            //Resize
            \Image::make($path)->resize(900, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path);
           
            $aws = Aws::factory(config('filesystems.disks.s3images'));
            $client = $aws->get('S3');
            $result = $client->putObject(array(
                'Bucket'     => config('filesystems.disks.s3images.bucket'),
                'SourceFile' => $path,
                'Key'        => 'images/figuras/'.$newName,
                'ACL'        => 'public-read'
            ));        

            if($figure->route != 'default.jpg')//Nunca se elimina la default
            {
            \Storage::disk('s3images')->delete('images/figuras/'.$figure->route);
            }
            \File::delete($path);
            //Image

            $figure->route = $newName;
            $figure->name = $request->input('name');
            $figure->save();    

            return \Redirect::back()->with('message', 'Actualizado');
        }else 
        {
            return \Redirect::back()->withErrors($validation);
        }
    }

    public function show()
	{
    	$figures = Figure::orderBy('name','ASC')->paginate(25);

        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');

        foreach($figures as $i => $fig)
        {
            $temp[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$fig->route);
        }

        return view('list.figure', compact('figures', 'temp'));
    }

    public function destroy($id)
	{
        $figure = Figure::find($id);
        $this->notFoundUnless($figure);

        if($figure->route != "default.jpg")
        {
            \Storage::disk('s3images')->delete('images/figuras/'.$figure->route);
            $figure->delete();//Delete db
        }
        
        return \Redirect::route('list_figure')->with('message', 'Eliminado con exito');
    }
	

}
