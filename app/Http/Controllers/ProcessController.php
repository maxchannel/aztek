<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Process;
use App\Http\Requests\CreateProcess;
use App\Http\Requests\EditProcess;

class ProcessController extends Controller
{
	public function index()
	{
        return view('add.process');
    }

	public function create(CreateProcess $request)
	{
    	$process = new Process;
        $process->fill($request->all());
        //Checkbox
        if($request->input('maq') == 'yes')
        {
            $process->maq = 1;
        }else
        {
            $process->maq = 0;
        }
    	$process->save();

        return \Redirect::back()->with('message', 'Creado Con Éxito');
    }

    public function edit($id)
	{
    	$process = Process::find($id);
        $this->notFoundUnless($process);

        return view('edit.process', compact('process'));
    }

	public function update($id, EditProcess $request)
	{
    	$process = Process::findOrFail($id);
    	$process->fill($request->all());
        //Checkbox
        if($request->input('maq') == 'yes')
        {
            $process->maq = 1;
        }else
        {
            $process->maq = 0;
        }
        $process->save();

        return \Redirect::back()->with(['message'=>'Editado con Exito']);
    }

    public function show()
	{
    	$processes = Process::orderBy('name', 'ASC')->paginate(25);

        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'name')
            {
                $processes = Process::orderBy('name', 'DESC')->paginate(25);
            }elseif($sort == 'name_a')
            {
                $processes = Process::orderBy('name', 'ASC')->paginate(25);
            }elseif($sort == 'fecha')
            {
                $processes = Process::orderBy('created_at', 'DESC')->paginate(25);
            }elseif($sort == 'fecha_a')
            {
                $processes = Process::orderBy('created_at', 'ASC')->paginate(25);
            }
        }

        return view('list.process', compact('processes'));
    }

    public function destroy($id)
    {
        $delete = Process::find($id);
        $this->notFoundUnless($delete);
        $delete->delete();//Delete db
        return \Redirect::route('list_process')->with('message', 'Eliminado con exito');
    }

	

}
