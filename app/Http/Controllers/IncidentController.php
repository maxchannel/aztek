<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreNewIncident;
use App\Http\Requests\EditIncident;
use App\Incident;
use App\Order;

class IncidentController extends Controller
{
    public function index()
    {
        $orders = Order::lists('name', 'id');

        return view('add.incident', compact('orders'));
    }

    public function store(StoreNewIncident $request)
    {
        $incident = new Incident;
        $incident->user_id = \Auth::user()->id;
        $incident->fill($request->all());
        $incident->save();

        return \Redirect::back()->with('message', 'Creado Con Éxito');
    }

    public function list_incident()
    {
        $incidents = Incident::orderBy('content', 'ASC')->paginate(25);

        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'name')
            {
                $incidents = Incident::orderBy('content', 'DESC')->paginate(25);
            }elseif($sort == 'name_a')
            {
                $incidents = Incident::orderBy('content', 'ASC')->paginate(25);
            }elseif($sort == 'fecha')
            {
                $incidents = Incident::orderBy('created_at', 'DESC')->paginate(25);
            }elseif($sort == 'fecha_a')
            {
                $incidents = Incident::orderBy('created_at', 'ASC')->paginate(25);
            }
        }

        return view('list.incident', compact('incidents'));
    }

    public function edit_incident($id)
    {
        $incident = Incident::findOrFail($id);
        $orders = Order::lists('name', 'id');

        return view('edit.incident', compact('incident', 'orders'));
    }

    public function update($id, EditIncident $request)
    {
        $incident = Incident::find($id);
        $incident->fill($request->all());
        $incident->save();

        return \Redirect::back()->with(['message'=>'Editado con Exito']);
    }
}