<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\UserNotification;

class PublicController extends Controller
{
    public function notifications()
    {
        //Cambiando a visto
        if(\Auth::user()->getNotifications() > 0)
        {
            UserNotification::where('user_id', \Auth::user()->id)->update(array('v' => 1));
        }
        $nots = UserNotification::where('user_id', \Auth::user()->id)->orderBy('created_at','DESC')->paginate(20);

        return view('public.notifications', compact('nots'));
    }

    public function destroy_not($id)
    {
        $n = UserNotification::find($id);
        $n->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function list_operator()
    {
        return view('list.operator');
    }

    public function change_mode($new_mode)
    {
    	if($new_mode == "orca" || $new_mode == "azteca") 
    	{
    		$user = User::find(\Auth::user()->id);
            $user->mode = $new_mode;
            $user->save();
    	}else
    	{
    		return \Redirect::route('home');
    	}
        

        return \Redirect::back()->with('message', 'Perfil Actualizado');
    }

}
