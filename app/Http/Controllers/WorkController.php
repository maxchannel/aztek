<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreNewWork;
use App\Http\Requests\StoreEditWork;
use App\Http\Requests\StoreNewWorkOrca;
use App\Http\Requests\CreateFiguresOder;
use App\Http\Requests\StoreNewWorkIndie;
use App\Http\Requests\UpdateOrcaOrder;
use App\Http\Requests\ProcessManualRequest;
use App\Work;
use App\Product;
use App\Process;
use App\Figure;
use App\User;
use App\UserNotification;
use App\Client;
use App\ClientOrder;
use App\ClientImage;
use App\ClientProduct;
use App\ClientProcess;
use App\ClientProcessManual;
use App\ClientFigure;
use App\Order;
use App\OrderProduct;
use App\OrderImage;
use App\OrderFigure;
use App\OrderProcess;
use Aws\S3\S3Client;
use Aws\Common\Aws;

class WorkController extends Controller
{
    //Azteca
    public function index()
    {
        $figures = Figure::orderBy('name', 'ASC')->lists('name', 'id');
        $clients = Client::orderBy('name', 'ASC')->lists('name', 'id');
        $processes = Process::where('id','!=',1)->orderBy('name', 'ASC')->lists('name', 'id');
        $products = Product::orderBy('name', 'ASC')->lists('name', 'id');
        
        $clients_t = Client::all();
        $processes_t = Process::all();
        $products_t = Product::all();
        $p_t_count = count($products_t);

        $barrenos = Process::where('name','like', '%barreno%')->lists('name', 'id');
        $resaques = Process::where('name','like', '%resaque%')->lists('name', 'id');
        $cantos = Process::where('name','like', '%canto%')->lists('name', 'id');
        $biseles = Process::where('name','like', '%bisel%')->lists('name', 'id');
        $peliculas = Process::where('name','like', '%pelicula%')->lists('name', 'id');
        $esmeriles = Process::where('name','like', '%esmeril%')->lists('name', 'id');

        //AWS
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        $fig_first = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/default.jpg');
        //AWS

        return view('add.work', compact('products', 'clients', 'barrenos', 'resaques', 'cantos', 
            'biseles', 'peliculas', 'esmeriles', 'processes', 'clients_t', 'products_t','p_t_count', 'fig_first',
            'processes_t', 'figures'));
    }

    public function store(StoreNewWork $request)
    {
        //Viendo si selecciono o mando al cliente manual
        if($request->input('client_id') != "")
        {
            $cliente_id_new = $request->input('client_id');
        }elseif($request->input('client_new_name') != "")//Creando cliente si se mando manual
        {
            $client = new Client;
            $client->name = $request->input('client_new_name');
            $client->save();
            
            $cliente_id_new = $client->id;
        }

        $order = new ClientOrder;
        $order->fill($request->all());
        $order->status = $request->input('status');;
        $order->price = 0;
        $order->client_id = $cliente_id_new;
        $order->user_id = \Auth::user()->id;
        $order->save();

        //Cristales 
        $cristal = $request->input('cristal');
        $cantidad = $request->input('cantidad');
        $ancho = $request->input('ancho');
        $largo = $request->input('largo');
        $process = $request->input('process');
        $cantidad_p = $request->input('cantidad_p');
        $p_p = $request->input('p_p');
        $precio = $request->input('precio'); 

        $number = count($cristal);  
        if($number > 0)  
        {  
            for($i=0; $i<$number; $i++)  
            {
                if(!empty($cantidad[$i]) && !empty($cristal[$i]) && !empty($ancho[$i]) && !empty($largo[$i]) && !empty($precio[$i]))//Solo si se envia nombre y contenido
                {  
                    $client_order = new ClientProduct;
                    $client_order->client_order_id = $order->id;
                    $client_order->cantidad = $cantidad[$i];
                    $client_order->product_id = $cristal[$i];
                    $client_order->ancho = $ancho[$i];
                    $client_order->largo = $largo[$i];
                    $client_order->p = $precio[$i];
                    $client_order->save();

                    //Descuento de cristal
                    $xyz = Product::find($cristal[$i]);
                    $xyz->quantity = $xyz->quantity - $cantidad[$i];
                    $xyz->save();

                    //Descontando cristal
                    $quit = Product::find($cristal[$i]);
                    $quit->quantity = $quit->quantity - $client_order->cantidad;
                    $quit->save();
                    //Agregando el corte de vidrio para los operadores
                    $order_product = new ClientProcess;
                    $order_product->cantidad = 1;
                    $order_product->confirmed = 0;
                    $order_product->p = 1;
                    $order_product->client_product_id = $client_order->id;
                    $order_product->process_id = 1;
                    $order_product->save();

                    //Procesos
                    $p_count = count($process[0]);  //Error
                    if($p_count > 0)  
                    {  
                        for($j=0; $j<$p_count; $j++)  
                        {
                            if(!empty($process[$i][$j]) && !empty($cantidad_p[$i][$j]) && !empty($p_p[$i][$j]))//Solo si se envia nombre y contenido
                            {  
                                

                                for($b=0; $b < $cantidad_p[$i][$j]; $b++) 
                                { 
                                    $order_product = new ClientProcess;
                                    $order_product->cantidad = 1;
                                    $order_product->confirmed = 0;
                                    $order_product->p = $p_p[$i][$j];
                                    $order_product->client_product_id = $client_order->id;
                                    $order_product->process_id = $process[$i][$j];
                                    $order_product->save();
                                }

                            }   
                        } 
                    } 
                    //Procesos
                }  
            } 
        }
        //Cristales

        //Figuras al cristal
        if($request->input('figuras_check_a') == true)
        {
            $fig = new ClientFigure;
            $fig->client_order_id = $order->id;
            $fig->figure_id = 1;
            $fig->save();
        }

        $figuras = $request->input('figuras');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($figuras[$i]))//Solo si se envia nombre y contenido
            {  
                $order_product = new ClientFigure;
                $order_product->client_order_id = $order->id;
                $order_product->figure_id = $figuras[$i];
                $order_product->save();
            }
        }
        //Figuras al cristal

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $llaves=>$file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:5000'];
                $validation = \Validator::make(['file'=> $file], $rules);    
                if(!$validation->fails()) 
                {
                    if($llaves < 15)//Solo 15 imagenes por anuncio
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(15).".".$extension;    
                        $path = storage_path().'/uploads/'.$newName;

                        //Insert in db
                        $image = new ClientImage;
                        $image->client_order_id = $order->id;
                        $image->route = $newName;
                        $image->save();    

                        ///Move file to images/post
                        $file->move(storage_path().'/uploads/', $newName);

                        //Resize
                        \Image::make($path)->resize(900, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($path);

                        //Upload to Amazon
                        $aws = Aws::factory(config('filesystems.disks.s3images'));
                        $client = $aws->get('S3');
                        $result = $client->putObject(array(
                            'Bucket'     => config('filesystems.disks.s3images.bucket'),
                            'SourceFile' => $path,
                            'Key'        => 'images/obras/'.$newName,
                            'ACL'        => 'public-read'
                        ));

                        \File::delete($path);
                    }else
                    {
                        \Session::flash('image-message', 'Solo se agregaron 12 imágenes');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                }
            }
        }
        //Image

        return \Redirect::route('add_work_panorama', ['id'=>$order->id])->with('message', 'Creado con Éxito');
    }

    public function panorama($id)
    {
        $order = ClientOrder::find($id);
        $this->notFoundUnless($order);
        $products = ClientProduct::where('client_order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        $total_m2 = 0;

        if($order->price == 0)//Price Automatico
        {
            $prev=0;
            $fg = 0;
            $fh = 0;
            //Sumando Precio de cristal
            foreach($products as $key => $c_product) 
            { 
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 

                $cristales[$key] = $c_product->product->name;
                $importe[$key] = $x[$key][$c_product->p]*$c_product->ancho*$c_product->largo;
                $vidrio[$key] = $importe[$key];
                

                //Precio de los procesos
                foreach($c_product->processes as $j => $process) 
                {
                    //Total de m2
                    $total_m2 += $c_product->ancho*$c_product->largo;
                    
                    //Si existe una medida enviada manualmente se guarda en el array
                    if(ClientProcessManual::where('client_process_id', $process->pivot->id)->exists())
                    {
                        $process_manual = ClientProcessManual::where('client_process_id', $process->pivot->id)->first();
                        $manual[$fh] = $process_manual->can;
                    }else
                    {
                        $manual[$fh] = "";
                    }
                    
                    $y[$j][1] = $process->price1;
                    $y[$j][2] = $process->price2;
                    $y[$j][3] = $process->price3; 
                    $stereo[$fh] = $y[$j][$process->pivot->p];
                    $torr[$fh] = 0;

                    if($process->unity == "ml")//Perimetro
                    {
                        if($manual[$fh] != "")//Si hay medida ingresada manualmente
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh] * $manual[$fh]);
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh] * $manual[$fh]);
                        }else
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh] * (($c_product->largo+$c_product->ancho)*2));
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh] * (($c_product->largo+$c_product->ancho)*2));
                        }
                    }elseif($process->unity == "veces")//Perimetro
                    {
                        $importe[$key] += $process->pivot->cantidad*$stereo[$fh];
                        $torr[$fh] = $process->pivot->cantidad*$stereo[$fh];
                    }elseif($process->unity == "m2")//Si hay medida ingresada manualmente
                    {
                        if($manual[$fh] != "")
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh]*$manual[$fh]);
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh]*$manual[$fh]);
                        }else
                        {
                            $torr[$fh] = $process->pivot->cantidad*($stereo[$fh]*($c_product->largo*$c_product->ancho));
                            $importe[$key] += $process->pivot->cantidad*($stereo[$fh]*($c_product->largo*$c_product->ancho));
                        }
                    }

                    $fg++;

                } 

                $fh++;//Incremento de medidas manuales
                $importe[$key] *= $c_product->cantidad;
                $prev += $importe[$key];
            }
            
            if($order->i > 0)//IVA
            {
                $sub = $prev;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $prev;
            }

        }else//Price Manual
        {
            if($order->i > 0)//IVA
            {
                $sub = $order->price;
                $iva = ($sub/100)*16;
                $precio = $sub + $iva;
            }else//Sin
            {
                $sub = 0;
                $iva = 0;
                $precio = $order->price;
            }

            $fg = 0;
            $fh = 0;
            foreach($products as $key => $c_product) 
            {
                //Si existe una medida enviada manualmente se guarda en el array
                $manual[$fh] = "";

                $cristales[$key] = $c_product->product->name;
                $x[$key][1] = $c_product->product->price1;
                $x[$key][2] = $c_product->product->price2;
                $x[$key][3] = $c_product->product->price3; 
                $importe[$key] = $x[$key][$c_product->p]*$c_product->ancho*$c_product->largo;
                $vidrio[$key] = $importe[$key];

                foreach($c_product->processes as $j => $process) 
                {
                    $y[$j][1] = $process->price1;
                    $y[$j][2] = $process->price2;
                    $y[$j][3] = $process->price3; 
                    $stereo[$fh] = $y[$j][$process->pivot->p];
                    $torr[$fh] = 0;

                    if($process->unity == "ml")//Perimetro
                    {
                        $torr[$fh] = $process->pivot->cantidad*($stereo[$fh] * (($c_product->largo+$c_product->ancho)*2));
                    }elseif($process->unity == "veces")//Perimetro
                    {
                        $torr[$fh] = $process->pivot->cantidad*$stereo[$fh];
                    }else
                    {
                        $torr[$fh] = $process->pivot->cantidad*($stereo[$fh]*($c_product->largo*$c_product->ancho));
                    }

                    $fg++;
                }
                $fh++;//Incremento de medidas manuales
            }
                
        }

        //AWS
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        if($order->images->count() > 0)//Imagen de AWS
        {
            $imh = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$order->images->first()->route);
        }
        if($order->figuresVigent->count() > 0)//Figuras de AWS
        {
            $fig_first = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$order->figures->first()->route);
        }
        //AWS
        //return $manual;

        return view('add.preview.work_panorama', compact('order', 'products', 'conteo', 'cristales', 'precio', 
            'iva', 'sub', 'x', 'y', 'imh', 'fig_first', 'importe', 'stereo', 'torr', 'vidrio', 'manual', 'total_m2'));
    }

    

    //Edit
    public function edit($id)//Viene del work_list, sabemos del boton con un hidden input
    {
        $pedido = ClientOrder::find($id);

        $products = Product::lists('name', 'id');
        $clients = Client::lists('name', 'id');
        //Procesos
        $processes = Process::lists('name', 'id');
        $barrenos = Process::where('name','like', '%barreno%')->lists('name', 'id');
        $resaques = Process::where('name','like', '%resaque%')->lists('name', 'id');
        $cantos = Process::where('name','like', '%canto%')->lists('name', 'id');
        $biseles = Process::where('name','like', '%bisel%')->lists('name', 'id');
        $peliculas = Process::where('name','like', '%pelicula%')->lists('name', 'id');
        $esmeriles = Process::where('name','like', '%esmeril%')->lists('name', 'id');

        return view('add.preview.work_edit', compact('pedido', 'products', 'clients', 'barrenos', 
            'resaques', 'cantos', 'biseles', 'peliculas', 'esmeriles', 'processes'));
    }

    public function editStore($id, StoreEditWork $request)//Viene del work_list, sabemos del boton con un hidden input
    {
        $pedido = ClientOrder::find($id);
        $this->notFoundUnless($pedido);

        switch($request->input('status')) 
        {
            case 'C':
                $us = User::where('type', 'tech')->get();
                foreach($us as $u) {
                    $not = new UserNotification;
                    $not->m = \Auth::user()->name.' confirmo un pedido <a href="'.route('tech_take',[$pedido->id]).'">Ver Pedido</a>';
                    $not->user_id = $u->id;
                    $not->v = 0;
                    $not->save();
                }
                break;

            case 'S':
                $us = User::where('type', 'tech')->get();
                foreach($us as $u) {
                    $not = new UserNotification;
                    $not->m = \Auth::user()->name.' puso en StandBy un pedido <a href="'.route('tech_take',[$pedido->id]).'">Ver Pedido</a>';
                    $not->user_id = $u->id;
                    $not->v = 0;
                    $not->save();
                }
                break;

            case 'T':
                $us = User::where('type', 'tech')->get();
                foreach($us as $u) {
                    $not = new UserNotification;
                    $not->m = \Auth::user()->name.' ha Terminado un pedido <a href="'.route('tech_take',[$pedido->id]).'">Ver Pedido</a>';
                    $not->user_id = $u->id;
                    $not->v = 0;
                    $not->save();
                }
                break;
        }

        $pedido->fill($request->all());
        if($request->input('i') == 'yes')
        {
            $pedido->i = 1;
        }else
        {
            $pedido->i = 0;
        }
        $pedido->save();

        return \Redirect::back()->with('message', 'Editado con Éxito');

    }
    //Edit

    public function preview(StoreNewWork $request)
    {
        $products = Product::lists('name', 'id');
        $clients = Client::lists('name', 'id');
        $processes = Process::lists('name', 'id');

        return view('add.preview.work', compact('products', 'clients', 'processes'));
    }

    public function list_order_images_az($id)
    {
        $order = ClientOrder::find($id);
        $this->notFoundUnless($order);

        // Create a service builder using a configuration file
        // Get the client from the builder by namespace
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');

        foreach($order->images as $i => $image)
        {
            $temp[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$image->route);
            //echo $temp[$i];
        }
        //return 'STOP';

        return view('add.preview.image_edit', compact('order', 'temp'));
    }

    public function list_order_images_store_az($id)
    {
        $order = ClientOrder::findOrFail($id);

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                $validation = \Validator::make(['file'=> $file], $rules);    

                if(!$validation->fails()) 
                {
                    //Limitando el numero de imagenes
                    if($order->images()->count() < 15)
                    {   
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(15).".".$extension;    
                        $path = storage_path().'/uploads/'.$newName;

                        //Insert in db
                        $image = new ClientImage;
                        $image->client_order_id = $order->id;
                        $image->route = $newName;
                        $image->save(); 

                        ///Move file to images/post
                        $file->move(storage_path().'/uploads/', $newName);

                        //Resize
                        \Image::make($path)->resize(900, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($path);

                        $aws = Aws::factory(config('filesystems.disks.s3images'));
                        $client = $aws->get('S3');
                        $result = $client->putObject(array(
                            'Bucket'     => config('filesystems.disks.s3images.bucket'),
                            'SourceFile' => $path,
                            'Key'        => 'images/obras/'.$newName,
                            'ACL'        => 'public-read'
                        ));

                        \File::delete($path);
                    }else
                    {
                        return \Redirect::back()->with('image-message', 'Alcanzaste el limite de 12 imágenes por anuncio');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes menores a 4MB');
                }
            }
        }else
        {
            return \Redirect::back()->with('image-message', 'Debes seleccionar al menos 1 imágen');
        }

        $order->save();
        return \Redirect::back()->with(['message'=>'Se agregaron nuevas imágenes a tu pedido']);
    }

    public function delete_order_image_az($id)//Delete
    {
        $order_image = ClientImage::find($id);
        $this->notFoundUnless($order_image);

        if($order_image->route != "default.jpg")
        {
            \Storage::disk('s3images')->delete('images/obras/'.$order_image->route);
            $order_image->delete();//Delete db
        }
        
        return \Redirect::back()->with('message', 'Eliminada con exito');
    }

    public function list_order_figures_az($id)
    {
        $order = ClientOrder::find($id);
        $figures = Figure::lists('name', 'id');

        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');

        foreach($order->figuresVigent as $i => $fig)
        {
            $temp[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$fig->route);
        }

        return view('add.preview.figure_edit', compact('order', 'figures', 'temp'));
    }

    public function list_order_figures_store_az($id, CreateFiguresOder $request)
    {
        $figuras = $request->input('figuras');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($figuras[$i]))//Solo si se envia nombre y contenido
            {  
                $figure = new ClientFigure;
                $figure->client_order_id = $id;
                $figure->figure_id = $figuras[$i];
                $figure->save();
            }
        }

        return \Redirect::back()->with(['message'=>'Se agregaron nuevas figuras']);
    }

    public function destroy_client_figure($id)
    {
        $client_figure = ClientFigure::findOrFail($id);
        $client_figure->delete();

        $message = 'Anuncio Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    //Azteca








    //Orca
    public function add_work_orca()
    {
        $figures = Figure::lists('name', 'id');
        $clients = Client::lists('name', 'id');
        $clients_t = Client::all();
        $processes = Process::lists('name', 'id');
        $processes_t = Process::all();
        $products = Product::lists('name', 'id');
        $products_t = Product::all();
        $p_t_count = count($products_t);

        return view('add.work_orca', compact('products', 'clients', 'barrenos', 'resaques', 'cantos', 
            'biseles', 'peliculas', 'esmeriles', 'processes', 'clients_t', 'products_t','p_t_count',
            'processes_t', 'figures'));
    }

    public function add_work_indieCreate(StoreNewWorkIndie $request)
    {
        //Work
        $work = new Work;
        $work->fill($request->all());
        $work->status = 'hecho';
        $work->client_id = 1; //Directamente a Orca
        $work->user_id = \Auth::user()->id;
        $work->save();
        //Work

        return \Redirect::back()->with('message', 'Creado con Éxito');
    }

    public function add_work_orcaStore(StoreNewWorkOrca $request)
    {
        //Pedido
        $order = new Order;
        $order->fill($request->all());
        $order->status = $request->input('status');;
        $order->work_id = $work->id;
        $order->user_id = \Auth::user()->id;
        $order->save();
        //Pedido

        return $request->input('suministration').' '.$request->input('instalation');

        //Figuras al cristal
        if($request->input('figuras_check_a') == true)
        {
            $fig = new OrderFigure;
            $fig->order_id = $order->id;
            $fig->figure_id = 1;
            $fig->save();
        }

        $figuras = $request->input('figuras');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($figuras[$i]))//Solo si se envia nombre y contenido
            {  
                $order_product = new OrderFigure;
                $order_product->order_id = $order->id;
                $order_product->figure_id = $figuras[$i];
                $order_product->save();
            }
        }
        //Figuras al cristal

        //Cristales 
        $cristal = $request->input('cristal');
        $cantidad = $request->input('cantidad');
        $ancho = $request->input('ancho');
        $largo = $request->input('largo');
        $process = $request->input('process');
        $cantidad_p = $request->input('cantidad_p');

        $number = count($cristal);  
        if($number > 0)  
        {  
            for($i=0; $i<$number; $i++)  
            {
                if(!empty($cantidad[$i]) && !empty($cristal[$i]) && !empty($ancho[$i]) && !empty($largo[$i]))//Solo si se envia nombre y contenido
                {  
                    $order_product = new OrderProduct;
                    $order_product->order_id = $order->id;
                    $order_product->cantidad = $cantidad[$i];
                    $order_product->product_id = $cristal[$i];
                    $order_product->ancho = $ancho[$i];
                    $order_product->largo = $largo[$i];
                    $order_product->save();

                    //Procesos
                    $p_count = count($process[0]);  //Error
                    if($p_count > 0)  
                    {  
                        for($j=0; $j<$p_count; $j++)  
                        {
                            if(!empty($process[$i][$j]) && !empty($cantidad_p[$i][$j]))//Solo si se envia nombre y contenido
                            {  
                                $order_pros = new OrderProcess;
                                $order_pros->cantidad = $cantidad_p[$i][$j];
                                $order_pros->confirmed = 0;
                                $order_pros->order_product_id = $order_product->id;
                                $order_pros->process_id = $process[$i][$j];
                                $order_pros->save();
                            }   
                        } 
                    } 
                    //Procesos
                }  
            } 
        }
        //Cristales


        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $llaves=>$file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:5000'];
                $validation = \Validator::make(['file'=> $file], $rules);    
                if(!$validation->fails()) 
                {
                    if($llaves < 15)//Solo 15 imagenes por anuncio
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(8).".".$extension;    
                        ///Move file to images/post
                        $file->move('images/obras/',$newName); 
                        //Insert in db
                        $image = new OrderImage;
                        $image->order_id = $order->id;
                        $image->route = $newName;
                        $image->save();    
                    }else
                    {
                        \Session::flash('image-message', 'Solo se agregaron 15 imágenes');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes');
                }
            }
        }
        //Image

        return \Redirect::back()->with('message', 'Creado con Éxito');
    }
    //Orca

    public function list_work_orders($id)
    {
        $work = Work::find($id);

        return view('list.relations.work', compact('work'));
    }

    public function edit_orca($id)//Viene del work_list, sabemos del boton con un hidden input
    {
        $order = Order::find($id);
        $works = Work::lists('name', 'id');

        return view('add.preview.orca.work_edit', compact('order', 'works'));
    }

    public function edit_orcaStore($id, UpdateOrcaOrder $request)//Viene del work_list, sabemos del boton con un hidden input
    {
        $pedido = Order::find($id);
        $this->notFoundUnless($pedido);
        $pedido->fill($request->all());

        switch($request->input('status')) 
        {
            case 'C':
                $us = User::where('type', 'tech')->get();
                foreach($us as $u) {
                    $not = new UserNotification;
                    $not->m = \Auth::user()->name.' confirmo un pedido <a href="'.route('tech_take_orca',[$pedido->id]).'">Ver Pedido</a>';
                    $not->user_id = $u->id;
                    $not->v = 0;
                    $not->save();
                }
                break;

            case 'S':
                $us = User::where('type', 'tech')->get();
                foreach($us as $u) {
                    $not = new UserNotification;
                    $not->m = \Auth::user()->name.' puso en StandBy un pedido <a href="'.route('tech_take_orca',[$pedido->id]).'">Ver Pedido</a>';
                    $not->user_id = $u->id;
                    $not->v = 0;
                    $not->save();
                }
                break;

            case 'T':
                $us = User::where('type', 'tech')->get();
                foreach($us as $u) {
                    $not = new UserNotification;
                    $not->m = \Auth::user()->name.' ha Terminado un pedido <a href="'.route('tech_take_orca',[$pedido->id]).'">Ver Pedido</a>';
                    $not->user_id = $u->id;
                    $not->v = 0;
                    $not->save();
                }
                break;
        }
        
        if($request->input('i') == 'yes')
        {
            $pedido->i = 1;
        }else
        {
            $pedido->i = 0;
        }
        if($request->input('instalation') == 'yes')
        {
            $pedido->instalation = 1;
        }else
        {
            $pedido->instalation = 0;
        }
        if($request->input('suministration') == 'yes')
        {
            $pedido->suministration = 1;
        }else
        {
            $pedido->suministration = 0;
        }

        $pedido->save();
        return \Redirect::back()->with('message', 'Editado con Éxito');

    }

    public function list_order_images_orca($id)
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);

        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');

        foreach($order->images as $i => $image)
        {
            $temp[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$image->route);
        }

        return view('add.preview.orca.image_edit', compact('order', 'temp'));
    }

    public function list_order_images_store_orca($id)
    {
        $order = Order::findOrFail($id);

        //Image
        $files = \Input::file('file');
        if(!empty($files[0]))//Solo si se manda 1 imagen
        {
            foreach($files as $file) 
            {
                // Validate files from input file
                $rules = ['file' => 'mimes:jpeg,bmp,png|max:4000'];
                $validation = \Validator::make(['file'=> $file], $rules);    

                if(!$validation->fails()) 
                {
                    //Limitando el numero de imagenes
                    if($order->images()->count() < 15)
                    {
                        //Rename file
                        $extension = $file->getClientOriginalExtension(); 
                        $newName = str_random(15).".".$extension;    
                        $path = storage_path().'/uploads/'.$newName;

                        //Insert in db
                        $image = new OrderImage;
                        $image->order_id = $order->id;
                        $image->route = $newName;
                        $image->save();  

                        ///Move file to images/post
                        $file->move(storage_path().'/uploads/', $newName);

                        //Resize
                        \Image::make($path)->resize(900, null, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($path);

                        $aws = Aws::factory(config('filesystems.disks.s3images'));
                        $client = $aws->get('S3');
                        $result = $client->putObject(array(
                            'Bucket'     => config('filesystems.disks.s3images.bucket'),
                            'SourceFile' => $path,
                            'Key'        => 'images/obras/'.$newName,
                            'ACL'        => 'public-read'
                        ));

                        \File::delete($path);
                    }else
                    {
                        return \Redirect::back()->with('image-message', 'Alcanzaste el limite de 12 imágenes por anuncio');
                    }

                }else 
                {
                    \Session::flash('image-message', 'Algunos archivos no eran imágenes, Solo se procesarón las imágenes menores a 4MB');
                }
            }
        }else
        {
            return \Redirect::back()->with('image-message', 'Debes seleccionar al menos 1 imágen');
        }

        $order->save();
        return \Redirect::back()->with(['message'=>'Se agregaron nuevas imágenes a tu pedido']);
    }

    public function delete_order_image_orca($id)//Delete
    {
        $order_image = OrderImage::find($id);
        $this->notFoundUnless($order_image);

        if($order_image->route != "default.jpg")
        {
            \Storage::disk('s3images')->delete('images/obras/'.$order_image->route);
            $order_image->delete();//Delete db
        }

        return \Redirect::back()->with('message', 'Eliminada con exito');
    }

    public function list_order_figures_orca($id)
    {
        $order = Order::find($id);
        $figures = Figure::lists('name', 'id');

        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');

        foreach($order->figuresVigent as $i => $fig)
        {
            $temp[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$fig->route);
        }

        return view('add.preview.orca.figure_edit', compact('order', 'figures', 'temp'));
    }

    public function list_order_figures_store_orca($id, CreateFiguresOder $request)
    {
        $figuras = $request->input('figuras');
        for($i=0; $i<=7; $i++)//Soloo contando 8 atributos
        { 
            if(!empty($figuras[$i]))//Solo si se envia nombre y contenido
            {  
                $figure = new OrderFigure;
                $figure->order_id = $id;
                $figure->figure_id = $figuras[$i];
                $figure->save();
            }
        }

        return \Redirect::back()->with(['message'=>'Se agregaron nuevas figuras']);
    }

    public function destroy_client_figure_orca($id)
    {
        $client_figure = OrderFigure::findOrFail($id);
        $client_figure->delete();

        $message = 'Anuncio Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function manual_process_az($id, $order_id)//Agregar medida manual
    {
        $process = ClientProcess::find($id);
        $this->notFoundUnless($process);
        //Si existe una medida enviada manualmente se guarda en el array
        if(ClientProcessManual::where('client_process_id', $process->id)->exists())
        {
            $existencia = 1;
            $medida_manual = ClientProcessManual::where('client_process_id', $id)->first();
        }else
        {
            $existencia = 0;
            $medida_manual = "";
        }

        return view('add.preview.process_edit', compact('process', 'id', 'order_id', 'existencia', 'medida_manual'));
    }

    public function manual_process_az_store($id, $order_id, ProcessManualRequest $request)
    {
        //Si existe una medida enviada manualmente se guarda en el array
        $m = new ClientProcessManual;
        $m->client_process_id = $id;
        $m->can = $request->input('manual'); 
        $m->save();

        return \Redirect::back()->with(['message'=>'Se Agregó']);
    }

    public function destroy_manual_process($id)//Delete
    {
        $order_image = ClientProcessManual::find($id);
        $order_image->delete();

        return \Redirect::back()->with('message', 'Eliminado');
    }

}
