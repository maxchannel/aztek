<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ClientOrder;
use App\Client;
use App\Order;
use App\OrderProcess;
use App\ClientProcess;

class StatisticController extends Controller
{
    public function stock()
    {
    	//Clientes
    	$o_p = ClientOrder::where('status','P')->count();
    	$o_t = ClientOrder::where('status','T')->count();
    	$o_s = ClientOrder::where('status','S')->count();
    	$o_c = ClientOrder::where('status','C')->count();

    	//Orca
    	$oc_p = Order::where('status','P')->count();
    	$oc_t = Order::where('status','T')->count();
    	$oc_s = Order::where('status','S')->count();
    	$oc_c = Order::where('status','C')->count();

    	//Procesos terminado
    	$pro_sep = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','09')->count();
    	$pro_oct = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','10')->count();
    	$pro_nov = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','11')->count();
    	$pro_dic = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','12')->count();
    	$pro_ene = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','01')->count();
    	$pro_feb = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','02')->count();
    	$pro_mar = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','03')->count();
    	$pro_abr = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','04')->count();
    	$pro_may = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','05')->count();
    	$pro_jun = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','06')->count();
    	$pro_jul = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','07')->count();
    	$pro_ago = ClientProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','08')->count();

    	//Procesos terminado Orca
    	$pro2_sep = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','09')->count();
    	$pro2_oct = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','10')->count();
    	$pro2_nov = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','11')->count();
    	$pro2_dic = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2016')->whereMonth('created_at','=','12')->count();
    	$pro2_ene = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','01')->count();
    	$pro2_feb = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','02')->count();
    	$pro2_mar = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','03')->count();
    	$pro2_abr = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','04')->count();
    	$pro2_may = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','05')->count();
    	$pro2_jun = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','06')->count();
    	$pro2_jul = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','07')->count();
    	$pro2_ago = OrderProcess::where('confirmed',1)->whereYear('created_at','=','2017')->whereMonth('created_at','=','08')->count();

        //Cliente
        $cli_sep = Client::whereYear('created_at','=','2016')->whereMonth('created_at','=','09')->count();
        $cli_oct = Client::whereYear('created_at','=','2016')->whereMonth('created_at','=','10')->count();
        $cli_nov = Client::whereYear('created_at','=','2016')->whereMonth('created_at','=','11')->count();
        $cli_dic = Client::whereYear('created_at','=','2016')->whereMonth('created_at','=','12')->count();
        $cli_ene = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','01')->count();
        $cli_feb = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','02')->count();
        $cli_mar = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','03')->count();
        $cli_abr = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','04')->count();
        $cli_may = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','05')->count();
        $cli_jun = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','06')->count();
        $cli_jul = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','07')->count();
        $cli_ago = Client::whereYear('created_at','=','2017')->whereMonth('created_at','=','08')->count();

        return view('statistic.stock', 
        	compact('o_t', 'o_p', 'o_s', 'o_c', 'oc_t', 'oc_p', 'oc_s', 'oc_c', 
        		'pro_sep', 'pro_oct', 'pro_nov', 'pro_dic', 'pro_ene', 'pro_feb', 'pro_mar', 'pro_abr', 'pro_may', 'pro_jun', 'pro_jul', 'pro_ago',
        		'pro2_sep', 'pro2_oct', 'pro2_nov', 'pro2_dic', 'pro2_ene', 'pro2_feb', 'pro2_mar', 'pro2_abr', 'pro2_may', 'pro2_jun', 'pro2_jul', 'pro2_ago',
                'cli_sep', 'cli_oct', 'cli_nov', 'cli_dic', 'cli_ene', 'cli_feb', 'cli_mar', 'cli_abr', 'cli_may', 'cli_jun', 'cli_jul', 'cli_ago'));
    }


}
