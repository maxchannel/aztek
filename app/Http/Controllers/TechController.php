<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ClientOrder;
use App\Client;
use App\ClientTech;
use App\ClientImage;
use App\ClientProduct;
use App\ClientProcess;
use App\ClientFigure;
use App\Order;
use App\User;
use App\UserNotification;
use App\OrderTech;
use App\OrderImage;
use App\OrderProduct;
use App\OrderProcess;
use App\OrderFigure;
use App\Product;
use App\Garbage;
use App\Http\Requests\NewTake;
use App\Http\Requests\CreateGarbageRequest;
use Aws\Common\Aws;

class TechController extends Controller 
{
    public function all_list()
    {
        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'fecha')
            {
                $orders = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
                $k = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
            }elseif($sort == 'fecha_a')
            {
                $orders = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->get();
                $k = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->get();
            }
        }else
        {
            $orders = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
            $k = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
        }

        //Avance
        foreach ($k as $key => $order) 
        {
            $avan[$key] = 0;
            foreach($order->del_products as $product)
            {
                $t = ClientProcess::where('client_product_id', $product->id)->count();
                $c = ClientProcess::where('client_product_id', $product->id)->where('confirmed', 1)->count();
                if($t > 0) 
                {
                    $avan[$key] = $avan[$key] + round($c*(100/$t));
                }
            }
        }



        //Orca
        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'fecha')
            {
                $orders2 = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
                $u = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
            }elseif($sort == 'fecha_a')
            {
                $orders2 = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->get();
                $u = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->get();
            }
        }else
        {
            $orders2 = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
            $u = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
        }

        //Avance
        foreach($u as $key => $order2) 
        {
            $avan2[$key] = 0;
            foreach($order2->del_products as $product2)
            {
                $y = OrderProcess::where('order_product_id', $product2->id)->count();
                $o = OrderProcess::where('order_product_id', $product2->id)->where('confirmed', 1)->count();
                if($y > 0) 
                {
                    $avan2[$key] = $avan2[$key] + round($o*(100/$y));
                }
            }
        }
        
        return view('tech.all', compact('orders', 'avan', 'orders2', 'avan2'));
    }

	public function clientes()
    {
        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'fecha')
            {
                $orders = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->paginate(25);
                $k = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
            }elseif($sort == 'fecha_a')
            {
                $orders = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->paginate(25);
                $k = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->get();
            }
        }else
        {
            $orders = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->paginate(25);
            $k = ClientOrder::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
        }

        //Avance
        foreach ($k as $key => $order) 
        {
            $avan[$key] = 0;
            foreach($order->products as $product)
            {
                $t = ClientProcess::where('client_product_id', $product->id)->count();
                $c = ClientProcess::where('client_product_id', $product->id)->where('confirmed', 1)->count();
                if($t > 0) 
                {
                    $avan[$key] = $avan[$key] + round($c*(100/$t));
                }
            }
        }
        
        return view('tech.order', compact('orders', 'avan'));
    }

    public function tech_details($id)
    {
        $order = ClientOrder::find($id);
        $this->notFoundUnless($order);

        $query = \Request::input('query');
        $products = ClientProduct::where('client_order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        $precio = 0;
        //Sumando Precio de cristal
        foreach($products as $key => $product) 
        {
            $cristales[$key] = $product->product->name;
            $precio = $precio + ($product->product->price*$product->cantidad*$product->ancho*$product->largo);

            //Precio de los procesos
            foreach($product->processes as $process) 
            {
                $precio = $precio+$process->price;
            }

        }

        //AWS
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        if($order->images->count() > 0)//Imagen de AWS
        {
            $imh = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$order->images->first()->route);
        }
        foreach($order->images as $i => $image)
        {
            $temp[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$image->route);
        }

        if($order->figuresVigent->count() > 0)//Figuras de AWS
        {
            $imf = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$order->figuresVigent->first()->route);
        }
        foreach($order->figuresVigent as $i => $fig)
        {
            $tem[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$fig->route);
        }
        //AWS

        return view('tech.panorama', compact('order', 'products', 'conteo', 'cristales', 'precio', 'imh', 'temp', 'imf', 'tem'));
    }

    public function tech_take($id)
    {
        $order = ClientOrder::find($id);
        $this->notFoundUnless($order);
        $products = ClientProduct::where('client_order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        foreach($products as $key => $product) 
        {
            $cristales[$key] = $product->product->name;

            foreach($product->del_processes as $llave => $process)
            {
                if($process->user_id != NULL)
                {
                    $stereo[$key][$llave] = $process->user->name;
                }
            }
        }

        return view('tech.take', compact('order', 'products', 'conteo', 'cristales', 'precio', 'procesos', 'stereo'));
    }

    public function tech_take_update($id, NewTake $request)
    { 
        $proceso_hecho = $request->input('proceso_hecho');
        $proceso_id_value = $request->input('proceso_id_value');
        $products = ClientProduct::where('client_order_id', $id)->get();
        $cristales = count($products);
 
        if($cristales > 0)  
        {  
            for($i=0; $i<$cristales; $i++)//Cristales
            {
                $p_count = count($proceso_hecho[$i]);
                if($p_count > 0)  
                {  
                    for($j=0; $j<$p_count; $j++)//Procesos
                    {
                        $process = ClientProcess::find($proceso_id_value[$i][$j]);          

                        if($proceso_hecho[$i][$j] == 'yes')
                        {
                            $process->confirmed = 1;
                            $process->user_id = \Auth::user()->id;
                        }else
                        {
                            $process->confirmed = 0;
                        }
                        $process->save();  
                    } 
                } 
            } 
        }

        return \Redirect::back()->with('message', 'Actualizado con Éxito');
    }

    public function tech_take_indie($id)
    {
        $a = new ClientTech;
        $a->client_order_id = $id;
        $a->user_id = \Auth::user()->id;
        $a->save();

        //Nots
        $us = User::where('type', 'admin')->get();
        foreach($us as $u) {
            $not = new UserNotification;
            $not->m = \Auth::user()->name." tomo el pedido de ".\Request::input('cli');
            $not->user_id = $u->id;
            $not->v = 0;
            $not->save();
        }

        
        return \Redirect::back()->with('message', 'Haz tomado este pedido, puedes avanzar en los procesos del mismo.');
    }

    public function list_figures_az($id)
    {
        $order = ClientOrder::find($id);

        return view('tech.list_figures', compact('order'));
    }

    //ORCA
    public function orca()
    {
        //$orders = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->paginate(25);
        //$k = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'fecha')
            {
                $orders = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->paginate(25);
                $k = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
            }elseif($sort == 'fecha_a')
            {
                $orders = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->paginate(25);
                $k = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'ASC')->get();
            }
        }else
        {
            $orders = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->paginate(25);
            $k = Order::where('status','C')->orWhere('status','S')->orderBy('created_at', 'DESC')->get();
        }

        //Avance
        foreach ($k as $key => $order) 
        {
            $avan[$key] = 0;
            foreach($order->products as $product)
            {
                $t = OrderProcess::where('order_product_id', $product->id)->count();
                $c = OrderProcess::where('order_product_id', $product->id)->where('confirmed', 1)->count();
                if($t > 0) 
                {
                    $avan[$key] = $avan[$key] + round($c*(100/$t));
                }
            }
        }

        return view('tech.orca.order', compact('orders', 'avan'));
    }

    public function tech_details_orca($id)
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);

        $query = \Request::input('query');
        $products = OrderProduct::where('order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        $precio = 0;
        //Sumando Precio de cristal
        foreach($products as $key => $product) 
        {
            $cristales[$key] = $product->product->name;
            $precio = $precio + ($product->product->price*$product->cantidad*$product->ancho*$product->largo);

            //Precio de los procesos
            foreach($product->processes as $process) 
            {
                $precio = $precio+$process->price;
            }

        }

        //AWS
        $aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        if($order->images->count() > 0)//Imagen de AWS
        {
            $imh = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$order->images->first()->route);
        }
        foreach($order->images as $i => $image)
        {
            $temp[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/obras/'.$image->route);
        }

        if($order->figuresVigent->count() > 0)//Figuras de AWS
        {
            $imf = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$order->figuresVigent->first()->route);
        }
        foreach($order->figuresVigent as $i => $fig)
        {
            $tem[$i] = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), 'images/figuras/'.$fig->route);
        }
        //AWS

        return view('tech.orca.panorama', compact('order', 'products', 'conteo', 'cristales', 'precio', 'imh', 'temp', 'imf', 'tem'));
    }

    public function tech_take_orca($id)
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);
        $products = OrderProduct::where('order_id', $order->id)->with('product')->get();
        $conteo = count($products);

        foreach($products as $key => $product) 
        {
            $cristales[$key] = $product->product->name;

            foreach($product->del_processes as $llave => $process)
            {
                if($process->user_id != NULL)
                {
                    $stereo[$key][$llave] = $process->user->name;
                }
            }
        }

        return view('tech.orca.take', compact('order', 'products', 'conteo', 'cristales', 'precio', 'procesos', 'stereo'));
    }

    public function tech_take_update_orca($id, NewTake $request)
    { 
        $proceso_hecho = $request->input('proceso_hecho');
        $proceso_id_value = $request->input('proceso_id_value');
        $products = OrderProduct::where('order_id', $id)->get();
        $cristales = count($products);
 
        if($cristales > 0)  
        {  
            for($i=0; $i<$cristales; $i++)//Cristales
            {
                $p_count = count($proceso_hecho[$i]);
                if($p_count > 0)  
                {  
                    for($j=0; $j<$p_count; $j++)//Procesos
                    {
                        $process = OrderProcess::find($proceso_id_value[$i][$j]);
                        $this->notFoundUnless($process);            

                        if($proceso_hecho[$i][$j] == 'yes')
                        {
                            $process->confirmed = 1;
                            $process->user_id = \Auth::user()->id;
                        }else
                        {
                            $process->confirmed = 0;
                        }
                        $process->save();  
                    } 
                } 
            } 
        }

        return \Redirect::back()->with('message', 'Actualizado con Éxito');
    }

    public function tech_take_indie_orca($id)
    {
        $a = new OrderTech;
        $a->order_id = $id;
        $a->user_id = \Auth::user()->id;
        $a->save();

        //Nots
        $us = User::where('type', 'admin')->get();
        foreach($us as $u) {
            $not = new UserNotification;
            $not->m = \Auth::user()->name." tomo el pedido de la obra ".\Request::input('cli');
            $not->user_id = $u->id;
            $not->v = 0;
            $not->save();
        }
        
        return \Redirect::back()->with('message', 'Haz tomado este pedido, puedes avanzar en los procesos del mismo.');
    }

    public function list_figures($id)
    {
        $order = Order::find($id);

        return view('tech.orca.list_figures', compact('order'));
    }

}
