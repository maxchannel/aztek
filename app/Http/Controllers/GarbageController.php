<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Garbage;
use App\Product;
use App\User;
use App\UserNotification;
use Illuminate\Http\Request;
use App\Http\Requests\CreateGarbageRequest;

class GarbageController extends Controller 
{
	public function garbage()
    {
        $products = Product::lists('name', 'id');

        return view('tech.garbage', compact('products'));
    }

    public function garbage_create(CreateGarbageRequest $request)
    {
        $g = new Garbage;
        $g->fill($request->all());
        $g->save();

        $us = User::where('type', 'admin')->get();
        foreach($us as $u) {
            $not = new UserNotification;
            $not->m = \Auth::user()->name.' agrego un Desecho <a href="'.route('list_garbage').'">Ver Desechos</a>';
            $not->user_id = $u->id;
            $not->v = 0;
            $not->save();
        }

        return \Redirect::back()->with('message', 'Se agrego con éxito');
    }

    public function list_garbage()
    {
        $garbages = Garbage::paginate(25);

        return view('list.garbage', compact('garbages'));
    }

    public function destroy_gar($id)
    {
        $g = Garbage::findOrFail($id);
        $g->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }


}
