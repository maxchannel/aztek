<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreNewProduct;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
        return view('add.product');
    }

    public function store(StoreNewProduct $request)
    {
    	$product = new Product;
    	$product->fill($request->all());
    	$product->save();

        return \Redirect::back()->with('message', 'Creado Con Éxito');
    }

    public function list_product()
    {
    	$products = Product::orderBy('created_at', 'DESC')->paginate(25);

        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'name')
            {
                $products = Product::orderBy('name', 'DESC')->paginate(25);
            }elseif($sort == 'name_a')
            {
                $products = Product::orderBy('name', 'ASC')->paginate(25);
            }elseif($sort == 'fecha')
            {
                $products = Product::orderBy('created_at', 'DESC')->paginate(25);
            }elseif($sort == 'fecha_a')
            {
                $products = Product::orderBy('created_at', 'ASC')->paginate(25);
            }
        }

        return view('list.product', compact('products'));
    }

    public function edit_product($id)
    {
    	$product = Product::find($id);

        return view('edit.product', compact('product'));
    }

    public function edit_productStore($id, StoreNewProduct $request)
    {
        $product = Product::find($id);
        $product->fill($request->all());
        $product->save();

        return \Redirect::back()->with(['message'=>'Editado con Exito']);
    }

    public function destroy($id)
    {
        $delete = Product::find($id);
        $this->notFoundUnless($delete);        
        $delete->delete();//Delete db
        return \Redirect::route('list_product')->with('message', 'Eliminado con exito');
    }
}
