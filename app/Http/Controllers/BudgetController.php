<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBudget;
use Illuminate\Http\Request;
use App\Budget;
use App\User;
use App\UserNotification;

class BudgetController extends Controller 
{
	public function create()
	{
		return view('add.budget');
	}

	public function store(CreateBudget $request)
	{
		$bud = new Budget;
		$bud->fill($request->all());
		$bud->save();

		//Nots
        $us = User::where('type', 'admin')->get();
        foreach($us as $u) {
            $not = new UserNotification;
            $not->m = 'Se agrego una nueva Cotización de un cliente. <a href="'.route('list_budget').'">Ver Cotizaciones</a>';
            $not->user_id = $u->id;
            $not->v = 0;
            $not->save();
        }

		return \Redirect::back()->with('message', 'Cotización enviada con Éxito, nos pondremos en contacto contigo');
	}

	public function show()
	{
    	$buds = Budget::orderBy('created_at','DESC')->paginate(25);

        return view('list.budget', compact('buds'));
    }

	public function destroy($id)
	{
        $g = Budget::find($id);
        $g->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }
}
