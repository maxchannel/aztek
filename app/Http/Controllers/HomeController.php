<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Aws\Common\Aws;

class HomeController extends Controller
{
    public function index()
    {
    	$aws = Aws::factory(config('filesystems.disks.s3images'));
        $client = $aws->get('S3');
        $imh = $client->getObjectUrl(config('filesystems.disks.s3images.bucket'), '/slide/home.png');
        
        return view('welcome', compact('imh'));
    }
}
