<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Work;
use App\Order;
use App\ClientOrder;
use App\Client;
use App\Product;
use App\Process;

class SearchController extends Controller 
{
    public function index()
    {
        return view('search.index');
    }

    public function search()
    {
        $query = \Request::input('query');
        $start_at = \Request::input('start_at');
        $end_at = \Request::input('end_at');
        $sort = \Request::input('sort');
        
        //Comandos
        switch($query) 
        {
            case 'crear cliente':
                return \Redirect::route('add_client');
                break;

        }

        //Busqueda

        //Obra
        $obra = Work::orderBy('created_at', 'DESC');
        if($start_at)
        {
            $obra->where('created_at', '>', $start_at);
        }

        if($end_at)
        {
            $obra->where('created_at', '<', $end_at);
        }
        $obra->where('name','like', '%'.$query.'%');
        $obra->orWhere('description','like', '%'.$query.'%');
        $results_obra = $obra->get();
        //Obra

        //Pedido
        $pedido = ClientOrder::orderBy('created_at', 'DESC');
        if($start_at)
        {
            $pedido->where('created_at', '>', $start_at);
        }

        if($end_at)
        {
            $pedido->where('created_at', '<', $end_at);
        }
        $pedido->where('name','like', '%'.$query.'%');
        $pedido->orWhere('description','like', '%'.$query.'%');
        $results_pedido = $pedido->get();
        //Pedido

        //Cliente
        $cliente = Client::orderBy('created_at', 'DESC');
        if($start_at)
        {
            $cliente->where('created_at', '>', $start_at);
        }

        if($end_at)
        {
            $cliente->where('created_at', '<', $end_at);
        }
        $cliente->where('name','like', '%'.$query.'%');
        $cliente->orWhere('address','like', '%'.$query.'%');
        $results_cliente = $cliente->get();
        //Cliente

        //Product
        $product = Product::orderBy('created_at', 'DESC');
        if($start_at)
        {
            $product->where('created_at', '>', $start_at);
        }

        if($end_at)
        {
            $product->where('created_at', '<', $end_at);
        }
        $product->where('name','like', '%'.$query.'%');
        $product->orWhere('description','like', '%'.$query.'%');
        $results_product = $product->get();
        //Product

        //Process
        $process = Process::orderBy('created_at', 'DESC');
        if($start_at)
        {
            $process->where('created_at', '>', $start_at);
        }

        if($end_at)
        {
            $process->where('created_at', '<', $end_at);
        }
        $process->where('name','like', '%'.$query.'%');
        $results_process = $process->get();
        //Process
        


        return view('list.search.search', compact('results_obra', 'results_pedido', 'results_cliente', 'results_product', 
            'sort', 'query', 'results_process'));
    }
}
