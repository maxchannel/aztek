<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ClientOrder;
use App\ClientProduct;
use App\Figure;
use App\Order;
use App\OrderProduct;
use App\OrderProcess;
use App\Client;
use App\Process;
use App\Product;
use App\ClientProcess;
use App\Http\Requests\NewProductOrder;

class CristalController extends Controller 
{

	public function index($id)
	{
        $order = ClientOrder::find($id);
        $this->notFoundUnless($order);
        $products = ClientProduct::where('client_order_id', $order->id)->with('product')->get();
        $conteo = count($products);
        $products_z = Product::orderBy('name', 'ASC')->lists('name', 'id');
        $processes = Process::where('id','!=',1)->orderBy('name', 'ASC')->lists('name', 'id');
        $products_t = Product::all();
        $p_t_count = count($products_t);

        return view('add.preview.cristal_edit', compact('order','conteo','products', 'processes', 'products_t', 'p_t_count', 'products_z'));
    }

    public function list_order_products_store_az($id, NewProductOrder $request)
    {
        $order = ClientOrder::find($id);
        $this->notFoundUnless($order);

        //Cristales 
        $cristal = $request->input('cristal');
        $cantidad = $request->input('cantidad');
        $ancho = $request->input('ancho');
        $largo = $request->input('largo');
        $process = $request->input('process');
        $cantidad_p = $request->input('cantidad_p');
        $p_p = $request->input('p_p');
        $precio = $request->input('precio');

        $number = count($cristal);  
        if($number > 0)  
        {  
            for($i=0; $i<$number; $i++)  
            {
                if(!empty($cantidad[$i]) && !empty($cristal[$i]) && !empty($ancho[$i]) && !empty($largo[$i]) && !empty($precio[$i]))//Solo si se envia nombre y contenido
                {  
                    $client_order = new ClientProduct;
                    $client_order->client_order_id = $order->id;
                    $client_order->cantidad = $cantidad[$i];
                    $client_order->product_id = $cristal[$i];
                    $client_order->ancho = $ancho[$i];
                    $client_order->largo = $largo[$i];
                    $client_order->p = $precio[$i];
                    $client_order->save();

                    //Procesos
                    $p_count = count($process[0]);  //Error
                    if($p_count > 0)  
                    {  
                        for($j=0; $j<$p_count; $j++)  
                        {
                            if(!empty($process[$i][$j]) && !empty($cantidad_p[$i][$j]) && !empty($p_p[$i][$j]))//Solo si se envia nombre y contenido
                            {  
                                $order_product = new ClientProcess;
                                $order_product->cantidad = 1;
                                $order_product->confirmed = 0;
                                $order_product->p = 1;
                                $order_product->client_product_id = $client_order->id;
                                $order_product->process_id = 1;
                                $order_product->save();

                                for($b=0; $b < $cantidad_p[$i][$j]; $b++) 
                                { 
                                    $order_product = new ClientProcess;
                                    $order_product->cantidad = 1;
                                    $order_product->confirmed = 0;
                                    $order_product->p = $p_p[$i][$j];
                                    $order_product->client_product_id = $client_order->id;
                                    $order_product->process_id = $process[$i][$j];
                                    $order_product->save();
                                }

                            }   
                        } 
                    } 
                    //Procesos
                }  
            } 
        }
        //Cristales

        return \Redirect::back()->with(['message'=>'Se Agrego con Éxito']);
    }

    public function delete_order_product_az($id)
    {
        $order = ClientProduct::find($id);
        $order->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

    public function orca($id)
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);
        $products = OrderProduct::where('order_id', $order->id)->with('product')->get();
        $conteo = count($products);
        $products_z = Product::orderBy('name', 'ASC')->lists('name', 'id');
        $processes = Process::where('id','!=',1)->orderBy('name', 'ASC')->lists('name', 'id');
        $products_t = Product::all();
        $p_t_count = count($products_t);

        return view('add.preview.orca.cristal_edit', compact('order','conteo','products', 'processes', 'products_t', 'p_t_count', 'products_z'));
    }

    public function list_order_products_store_orca($id, NewProductOrder $request)
    {
        $order = Order::find($id);
        $this->notFoundUnless($order);

        //Cristales 
        $cristal = $request->input('cristal');
        $cantidad = $request->input('cantidad');
        $ancho = $request->input('ancho');
        $largo = $request->input('largo');
        $process = $request->input('process');
        $cantidad_p = $request->input('cantidad_p');
        $p_p = $request->input('p_p');
        $precio = $request->input('precio');

        $number = count($cristal);  
        if($number > 0)  
        {  
            for($i=0; $i<$number; $i++)  
            {
                if(!empty($cantidad[$i]) && !empty($cristal[$i]) && !empty($ancho[$i]) && !empty($largo[$i]) && !empty($precio[$i]))//Solo si se envia nombre y contenido
                {  
                    $order_product = new OrderProduct;
                    $order_product->order_id = $order->id;
                    $order_product->cantidad = $cantidad[$i];
                    $order_product->product_id = $cristal[$i];
                    $order_product->ancho = $ancho[$i];
                    $order_product->largo = $largo[$i];
                    $order_product->p = $precio[$i];
                    $order_product->save();

                    //Procesos
                    $p_count = count($process[$i]);  //Error
                    if($p_count > 0)  
                    {  
                        for($j=0; $j<$p_count; $j++)  
                        {
                            if(!empty($process[$i][$j]) && !empty($cantidad_p[$i][$j]))//Solo si se envia nombre y contenido
                            {  
                                $order_process = new OrderProcess;
                                $order_process->cantidad = 1;
                                $order_process->p = 1;
                                $order_process->confirmed = 0;
                                $order_process->order_product_id = $order_product->id;
                                $order_process->process_id = 1;
                                $order_process->save();

                                for($b=0; $b < $cantidad_p[$i][$j]; $b++) 
                                { 
                                    $order_process = new OrderProcess;
                                    $order_process->cantidad = 1;
                                    $order_process->p = $p_p[$i][$j];
                                    $order_process->confirmed = 0;
                                    $order_process->order_product_id = $order_product->id;
                                    $order_process->process_id = $process[$i][$j];
                                    $order_process->save();
                                }
                            }   
                        } 
                    } 
                    //Procesos
                }  
            } 
        }
        //Cristales

        return \Redirect::back()->with(['message'=>'Se Agrego con Éxito']);
    }

    public function delete_order_product_orca($id)
    {
        $order = OrderProduct::find($id);
        $order->delete();

        $message = 'Eliminado';
        if($request->ajax())
        {
            return reponse()->json([
                'message'=>$message
            ]);
        }

        \Session::flash('message', $message);
    }

}
