<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\StoreNewClient;
use App\Client;
use App\ClientProcess;

class ClientController extends Controller
{
	public function index()
    {
        return view('add.client');
    }

    public function store(StoreNewClient $request)
    {
    	$client = new Client;
        $client->fill($request->all());
    	$client->save();

        return \Redirect::back()->with('message', 'Creado Con Éxito');
    }

    public function list_client()
    {
        $clients = Client::orderBy('name', 'ASC')->paginate(25);
        
        $sort = \Request::input('sort');
        if($sort)
        {
            if($sort == 'name')
            {
                $clients = Client::orderBy('name','DESC')->paginate(25);
            }elseif($sort == 'name_a')
            {
                $clients = Client::orderBy('name','ASC')->paginate(25);
            }elseif($sort == 'fecha')
            {
                $clients = Client::orderBy('created_at','DESC')->paginate(25);
            }elseif($sort == 'fecha_a')
            {
                $clients = Client::orderBy('created_at','ASC')->paginate(25);
            }
        }

        return view('list.client', compact('clients'));
    }

    public function edit_client($id)
    {
    	$client = Client::findOrFail($id);

        return view('edit.client', compact('client'));
    }

    public function edit_clientStore($id, StoreNewClient $request)
    {
    	$client = Client::findOrFail($id);
    	$client->fill($request->all());
        $client->save();

        return \Redirect::back()->with(['message'=>'Editado con Exito']);
    }

    public function destroy($id)
    {
        $delete = Client::find($id);
        $this->notFoundUnless($delete);

        //Borrando 3ra Relación
        foreach($delete->orders as $order) 
        {
            foreach($order->del_products as $product) 
            {
                ClientProcess::where('client_product_id', $product->id)->delete();
            }
        }
        
        $delete->delete();//Delete db
        return \Redirect::route('list_client')->with('message', 'Eliminado con exito');
    }
	
}
