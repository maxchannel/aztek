<?php namespace App\Http\Middleware;

use Closure;

class IsTech
{
    //Valida si es mi venta
    public function handle($request, Closure $next)
    {
        if($request->user()->is('white'))
        {
            return redirect('/');
        }

        return $next($request);
    }
}
