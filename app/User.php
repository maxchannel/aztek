<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use SoftDeletes, Authenticatable, CanResetPassword;

    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'username', 'type', 'mode'];
    protected $hidden = ['password', 'remember_token'];
    protected $dates = ['deleted_at'];

    public function mode()
    {
        return $this->mode;
    }

    public function notifications()
    {
        return $this->hasMany('App\UserNotification');
    }

    public function getNotifications()
    {
        return $this->notifications()->where('v',0)->count();
    }

    public function is($type)
    {
        return $this->type === $type;
    }

    public function isAdmin()
    {
        return $this->type === 'admin';
    }

    public function isTech()
    {
        return $this->type === 'tech';
    }

    public function isStaff()
    {
        return $this->type === 'staff';
    }
}
