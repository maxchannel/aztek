<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientTech extends Model 
{
	use SoftDeletes;

    protected $table = 'client_teches';
    protected $fillable = ['user_id', 'client_order_id'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\ClientOrder');
    }

    public function techs()
    {
        return $this->hasMany('App\User'); 
    }

}
