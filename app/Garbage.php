<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Garbage extends Model
{
    use SoftDeletes;

    protected $table = 'garbages';
    protected $fillable = ['a','l', 'product_id'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
