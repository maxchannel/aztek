<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderFigure extends Model
{
	use SoftDeletes;

    protected $table = 'order_figures';
    protected $fillable = ['figure_id', 'order_id'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function figures()
    {
        return $this->hasMany('App\Figure'); 
    }

}
