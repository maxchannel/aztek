<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Figure extends Model
{
	use SoftDeletes;

    protected $fillable = ['name', 'route'];
    protected $dates = ['deleted_at'];

}