<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Process extends Model 
{
    use SoftDeletes;

    protected $fillable = ['name', 'maq', 'unity', 'price1', 'price2', 'price3'];
    protected $dates = ['deleted_at'];

}