<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderTech extends Model 
{
	use SoftDeletes;

    protected $table = 'order_teches';
    protected $fillable = ['user_id', 'order_id'];
    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function techs()
    {
        return $this->hasMany('App\User'); 
    }

}
